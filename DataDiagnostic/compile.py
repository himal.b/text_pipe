from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("embedding",  ["embedding.py"]),
    Extension("plot_utility",  ["plot_utility.py"]),
    Extension("preprocessing",  ["preprocessing.py"])
]

setup(
    name = 'My Program Name',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)