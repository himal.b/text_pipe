import re
import nltk
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from tqdm import tqdm

nltk.download('stopwords')
nltk.download('wordnet')

tqdm.pandas(desc="progress bar")

# Pre-processing Functions
misspell_dict = {
    "aren't": "are not", "can't": "cannot", "couldn't": "could not", "couldnt": "could not", "didn't": "did not",
    "doesn't": "does not", "doesnt": "does not", "don't": "do not", "hadn't": "had not", "hasn't": "has not",
    "haven't": "have not", "havent": "have not", "he'd": "he would", "he'll": "he will", "he's": "he is",
    "i'd": "I would", "i'd": "I had", "i'll": "I will", "i'm": "I am", "isn't": "is not", "it's": "it is",
    "it'll": "it will", "i've": "I have", "let's": "let us", "mightn't": "might not", "mustn't": "must not",
    "shan't": "shall not", "she'd": "she would", "she'll": "she will", "she's": "she is", "shouldn't": "should not",
    "shouldnt": "should not", "that's": "that is", "thats": "that is", "there's": "there is", "theres": "there is",
    "they'd": "they would", "they'll": "they will", "they're": "they are", "theyre": "they are", "they've": "they have",
    "we'd": "we would", "we're": "we are", "weren't": "were not", "we've": "we have", "what'll": "what will",
    "what're": "what are", "what's": "what is", "what've": "what have", "where's": "where is", "who'd": "who would",
    "who'll": "who will", "who're": "who are", "who's": "who is", "who've": "who have", "won't": "will not",
    "wouldn't": "would not", "you'd": "you would", "you'll": "you will", "you're": "you are", "you've": "you have",
    "'re": " are", "wasn't": "was not", "we'll": " will", "tryin'": "trying"
}


def remove_class_less_then_k_datapoints(dataframe, target_column_name, k):
    """
    This function will remove those rows where any class is having less than k samples.

    :param dataframe: Pandas DataFrame
    :param target_column_name: Target column name
    :param k: Number of samples use to remove datapoints.
        If value counts of any class is less than k samples remove those datapoints.

    :return:
        dataframe:
            DataFrame containing only those rows with the classes having more than k samples
    """
    dataframe_values_count = dataframe[target_column_name].value_counts()
    class_less_then_k_datapoints = list(dataframe_values_count[dataframe_values_count.values < k].keys())

    if len(class_less_then_k_datapoints) > 0:
        for class_names in class_less_then_k_datapoints:
            dataframe = dataframe[dataframe[target_column_name] != class_names]
    return dataframe


def remove_outlier(dataframe, column_name, threshold):
    """
    Given the pandas dataframe it will return the non-outlier pandas dataframe based on target column.

    This function counts the lengths of words in each sentence. And based on the threshold value
    it removes all the data points having length of words less than the length of words at given
    threshold.

    :param dataframe: pandas dataframe consisting of text and target.
    :param column_name: Target column name.
        column_name must be numeric.
    :param threshold: float; between (0.1 to 0.99)
        to calculate the length of words at given percentile value(threshold).


    :return:
        non_outlier_dataframe
            Dataframe after removing the outliers.
    """
    n_length_words = dataframe[column_name].apply(lambda val: len(val.split()))

    return dataframe[n_length_words >= np.percentile(n_length_words, threshold)]


def remove_null_target_datapoints(dataframe, target_column_name):
    """ Given the dataframe and target column name, return the dataframe having no null target values"""
    return dataframe[pd.notnull(dataframe[target_column_name])]


def remove_duplicate(dataframe, column_name):
    """ Given the pandas dataframe it will return the non-duplicated pandas dataframe. """
    return dataframe.drop_duplicates(subset=[column_name])


def remove_digits(text):
    """Removing numbers from text"""
    return re.sub(r'[0-9]+', " ", text)


def remove_non_alpha_numeric_char(text):
    """ Remove non alpha characters"""
    return re.sub(r"[^A-Za-z0-9]", " ", text)


def lowercase(text):
    """ Finding the number of all capital word and lower it"""
    return text.lower()


def _get_misspell(misspelling_dict):
    misspell_re = re.compile('(%s)' % '|'.join(misspelling_dict.keys()))
    return misspelling_dict, misspell_re


def replace_typical_misspell(text):
    """ De-Concatenation of words and correction of misspelled words"""
    misspellings, misspellings_re = _get_misspell(misspell_dict)

    def _replace(match):
        return misspellings[match.group(0)]

    return misspellings_re.sub(_replace, text)


def strip_html(text):
    """ Remove all the html tags from text"""
    # clean_text = re.sub(r'http[s]?://\S+', " ", text)
    clean_text = BeautifulSoup(text, 'lxml').get_text()
    clean_text = re.sub(r'\n', " ", clean_text)
    return clean_text


def remove_stop_words_and_punc(text):
    """ Remove all the stopwords and discarding the words having less than 3 characters"""

    # Initialization of Stopwords and removing the words from the stop words list: 'no', 'nor', 'not'
    Stops = set(stopwords.words("english"))
    Stops.remove('no')
    Stops.remove('nor')
    Stops.remove('not')

    cleaned_text = [word for word in text.split() if word not in Stops and len(word) > 2]
    cleaned_text = " ".join(cleaned_text)
    return cleaned_text


def stem(text):
    """ Return text with stemmed words """

    stemmer = PorterStemmer()
    cleaned_text = " ".join([stemmer.stem(word) for word in text.split(" ")])
    return cleaned_text


def lemmatize(text):
    """ Return text with lemmatized words """

    word_net = nltk.WordNetLemmatizer()
    cleaned_text = " ".join([word_net.lemmatize(word) for word in text.split(" ")])
    return cleaned_text


def clean_text_data(series, data_diagnostic_parser):
    """
    Perform preprocessing on series and return cleaned series of text
    1. Remove html tags (if flagged as True)
    2. Lowercase the words (if flagged as True)
    3. Replace misspellings (if flagged as True)
    4. Remove numbers (if flagged as True)
    5. Remove non alpha characters / special characters (if flagged as True)
    6. Remove Stopwords(if flagged as True)
    7. lemmatize or stem the words in text (if flagged as True)

    :param series: pandas series of text.
    :param data_diagnostic_parser: config parser of DataDiagnostic stage.
    :return:
        cleaned_data:
            Pandas series of preprocessed text.
    """

    cleaned_data = series
    print("Preprocessing Start...")

    # Remove HTML tags
    if data_diagnostic_parser.getboolean('preprocessing', 'remove_html_tags'):
        print("Removing HTML Tags")
        cleaned_data = cleaned_data.progress_apply(strip_html)

    # Lowercase the text
    if data_diagnostic_parser.getboolean('preprocessing', 'lowercase'):
        print("Tokenizing and Lowercase Words")
        cleaned_data = cleaned_data.progress_apply(lowercase)

    # Replace misspellings
    if data_diagnostic_parser.getboolean('preprocessing', 'replace_misspell_words'):
        print("De-contraction")
        cleaned_data = cleaned_data.progress_apply(replace_typical_misspell)

    # Remove numbers from text
    if data_diagnostic_parser.getboolean('preprocessing', 'remove_numbers'):
        print("Removing Numbers")
        cleaned_data = cleaned_data.progress_apply(remove_digits)

    # Remove non alpha-numeric characters
    if data_diagnostic_parser.getboolean('preprocessing', 'remove_non_alpha_numeric_char'):
        print("Removing Alpha Numeric Characters")
        cleaned_data = cleaned_data.progress_apply(remove_non_alpha_numeric_char)

    # Remove Stopwords
    if data_diagnostic_parser.getboolean('preprocessing', 'remove_stop_words'):
        print("Removing Stopwords")
        cleaned_data = cleaned_data.progress_apply(remove_stop_words_and_punc)

    # Stemming
    if data_diagnostic_parser.getboolean('preprocessing', 'stemming'):
        print("Stemming")
        cleaned_data = cleaned_data.progress_apply(stem)

    # lemmatisation
    if data_diagnostic_parser.getboolean('preprocessing', 'lemmatize'):
        print("Lemmatisation")
        cleaned_data = cleaned_data.progress_apply(lemmatize)

    print("Preprocessing Done!")

    return cleaned_data
