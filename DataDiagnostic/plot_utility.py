import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.feature_extraction.text import CountVectorizer
from wordcloud import WordCloud

# set the background color to black
plt.style.use('dark_background')


# Utility functions for plotting
def word_frequency_unigram_plot(pandas_series, n_words=15, title=None):
    """
    Given the pandas series of text, calculates the frequency of each word and
    return most frequent words.

    :param pandas_series: Pandas series of text
    :param n_words: float; default=30
        Number of top frequent words to visualize.
    :param title: Title of the plot (train', 'test' or 'validation')

    :return:
        fig: plot of most frequent unigram words
    """

    list_of_all_words = []
    for sent in pandas_series:
        list_of_all_words.extend(sent.split())

    top_words = pd.Series(list_of_all_words).value_counts()[:n_words]
    top_words_prob_dist = top_words.values / sum(top_words.values)

    #  plot of frequency of popular words
    fig = plt.figure(figsize=(12, 6))
    # sns.set(font_scale=1.5)
    sns.barplot(x=top_words.index, y=top_words_prob_dist)
    plt.xlabel("words")
    plt.ylabel("frequency")
    plt.title(f"Frequency of most popular unigram - {title}\n", fontsize=15)
    plt.xticks(rotation=45)
    return fig


def word_frequency_bigram_plot(pandas_series, n_words=10, title=None):
    """
    Given the pandas series of text, calculates the frequency of bigram word and
    return most frequent words.

    :param pandas_series: Pandas series of text
    :param n_words: float; default=30
        Number of top frequent words to visualize.
    :param title: Title of the plot (train', 'test' or 'validation')

    :return:
        fig: plot of most frequent bigram words
    """
    vec = CountVectorizer(ngram_range=(2, 2)).fit(pandas_series)
    bow = vec.transform(pandas_series)
    sum_words = bow.sum(axis=0)
    word_freq = [(word, sum_words[0, idx]) for word, idx in vec.vocabulary_.items()]
    word_freq = sorted(word_freq, key=lambda x: x[1], reverse=True)
    word_freq_df = pd.DataFrame(word_freq, columns=['bigram', 'frequency'])[:n_words]

    #  plot of frequency of popular words
    fig = plt.figure(figsize=(12, 9))
    # sns.set(font_scale=1.5)
    sns.barplot(x=word_freq_df['bigram'], y=word_freq_df['frequency'])
    plt.xlabel("words")
    plt.ylabel("frequency")
    plt.title(f"Frequency of most popular bigram words - {title}\n", fontsize=15)
    plt.xticks(rotation=45)
    return fig


def word_cloud_plot(pandas_series, title=None):
    """
    Given the pandas series of text return the wordcloud of text.

    :param pandas_series:Pandas series of text
    :param title: Title of the plot (train', 'test' or 'validation')

    :return:
        fig: plot of wordcloud
    """
    all_words = ' '.join([text for text in pandas_series])
    w_cloud = WordCloud(width=800, height=500, random_state=2, max_font_size=100).generate(all_words)
    fig = plt.figure(figsize=(12, 7))  # tunable parameter
    plt.imshow(w_cloud, interpolation="bilinear")
    plt.axis('off')
    plt.title(f"wordCloud - {title}\n", fontsize=15)
    return fig


def class_distribution_plot(class_labels):
    """ Given the pandas series of class labels return the count plot """
    fig = plt.figure(figsize=(12, 7))
    # sns.set(font_scale=1)
    sns.countplot(x=class_labels)
    plt.xlabel("Class Labels")
    plt.ylabel("frequency")
    plt.title(f"Class Label Distribution\n", fontsize=15)
    plt.xticks(rotation=45)
    return fig


def word_counts_plot(pandas_series, title=None):
    """ Given the pandas series of text data, return the word count distribution plot. """
    word_counts = pandas_series.apply(lambda x: len(x.split()))

    fig = plt.figure(figsize=(16, 9))
    # sns.set(font_scale=1)
    sns.distplot(x=word_counts, ax=plt.axes())
    plt.xlabel("Number of words")
    plt.ylabel("Number of data points")
    plt.title(f"Word counts distribution - {title}\n", fontsize=12)
    return fig
