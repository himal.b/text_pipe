from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.preprocessing import StandardScaler


def count_vectorizer_embedding(x_train, x_test, parser):
    """
     Given training text and test text pandas series, fit the CountVectorizer on training
     data and return transformed embedding vectors of training and test in the from of
     sparse matrix along with CountVectorizer instance.

    :param x_train: Pandas series of training data text features
    :param x_test: Pandas series of test data text features
    :param parser: Config parser of DataDiagnostic

    :return:
        bow_vectorizer:
            CountVectorizer instance.
        bow_x_train:
            Vectorized sparse embedding vectors of training text data.
        bow_x_test:
            Vectorized sparse embedding vectors of training text data.
    """
    if parser.get('embedding', 'max_feature') == 'all':
        n_gram = parser.getint('embedding', 'n_gram_range_start'), parser.getint('embedding', 'n_gram_range_end')
        bow_vectorizer = CountVectorizer(max_df=0.90, min_df=2, ngram_range=n_gram, stop_words='english')
    else:
        n_gram = parser.getint('embedding', 'n_gram_range_start'), parser.getint('embedding', 'n_gram_range_end')
        max_feat = parser.getint('embedding', 'max_feature')

        bow_vectorizer = CountVectorizer(max_df=0.90, min_df=2, ngram_range=n_gram, stop_words='english',
                                         max_features=max_feat)

    bow_x_train = bow_vectorizer.fit_transform(x_train)
    bow_x_test = bow_vectorizer.transform(x_test)

    return bow_vectorizer, bow_x_train, bow_x_test


def tfidf_vectorizer_embedding(x_train, x_test, parser):
    """
     Given training text and test text pandas series, fit the TfidfVectorizer on training
     data and return normalized transformed embedding vectors of training and test in the from of
     sparse matrix along with TfidfVectorizer instance.

    :param x_train: Pandas series of training data text features
    :param x_test: Pandas series of test data text features
    :param parser: Config parser of DataDiagnostic

    :return:
        tfidf_vectorizer:
            TFIDFVectorizer instance.
        standard_scalar:
            StandardScalar instance.
        tfidf_x_train:
            Vectorized sparse embedding vectors of training text data.
        tfidf_x_test:
            Vectorized sparse embedding vectors of training text data.
    """
    if parser.get('embedding', 'max_feature') == 'all':
        n_gram = parser.getint('embedding', 'n_gram_range_start'), parser.getint('embedding', 'n_gram_range_end')
        tfidf_vectorizer = TfidfVectorizer(max_df=0.90, min_df=2, ngram_range=n_gram, stop_words='english')
    else:
        n_gram = parser.getint('embedding', 'n_gram_range_start'), parser.getint('embedding', 'n_gram_range_end')
        max_feat = parser.getint('embedding', 'max_feature')

        tfidf_vectorizer = TfidfVectorizer(max_df=0.90, min_df=2, ngram_range=n_gram, stop_words='english',
                                           max_features=max_feat)

    tfidf_x_train = tfidf_vectorizer.fit_transform(x_train)
    tfidf_x_test = tfidf_vectorizer.transform(x_test)

    # Standard Scaling
    standard_scalar = StandardScaler(with_mean=False)
    tfidf_x_train = standard_scalar.fit_transform(tfidf_x_train)
    tfidf_x_test = standard_scalar.transform(tfidf_x_test)

    return tfidf_vectorizer, standard_scalar, tfidf_x_train, tfidf_x_test
