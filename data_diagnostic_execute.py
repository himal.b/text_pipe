import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import os
    import warnings
    from configparser import ConfigParser, ExtendedInterpolation

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import LabelEncoder

    from DataDiagnostic import preprocessing, plot_utility
    from DataDiagnostic.embedding import count_vectorizer_embedding, tfidf_vectorizer_embedding
    from Utility import utility

    # Ignore DeprecationWarning
    warnings.filterwarnings("ignore")

    # Use dark background
    plt.style.use('dark_background')

    # Remove Plotting Warnings
    plt.rcParams.update({'figure.max_open_warning': 0})

    class PassError(Exception):
        """Base class for exceptions in this module."""
        pass


    class DataDiagnosticsError(Exception):

        # Initializer
        def __init__(self, msg):
            self.msg = msg

        def __str__(self):
            return repr(self.msg)


    def label_encoder_transform(training_set_y, val_set_y, test_set_y):
        """This function will encode target values (y) using 'sklearn' LabelEncoder.
        Takes the input training_set_y, val_set_y, test_set_y and return the instance of
        fitted LabelEncoder and set of encoded values of training_set_y, val_set_y, test_set_y.

        :param training_set_y: target values of training set
        :param val_set_y: target values of validation set
        :param test_set_y: target values of test set.
        :return:
            label_encoder:
                instance of fitted LabelEncoder
            encoded_training_set_y:
                label encoded values of training set
            encoded_val_set_y:
                label encoded values of validation set
            encoded_test_set_y:
                label encoded values of test set.

        Return eg; label_encoder, (encoded_training_set_y, encoded_val_set_y, encoded_test_set_y
        """
        label_encoder = LabelEncoder()
        label_encoder.fit(training_set_y)

        encoded_training_set_y = label_encoder.transform(training_set_y)
        encoded_val_set_y = label_encoder.transform(val_set_y)
        encoded_test_set_y = label_encoder.transform(test_set_y)

        return label_encoder, (encoded_training_set_y, encoded_val_set_y, encoded_test_set_y)


    class DataDiagnostic:

        def __init__(self, parser):
            self.parser = parser

        def _data_splitting(self, x, y):
            """ Credits: https://datascience.stackexchange.com/a/55322
            This function splits the dataset into given training ratio, validation ratio and test ratio.
            And return the training set, validation set and test set.

            :param x: Feature column
            :param y: Target column

            :return:
                x_train, x_val, x_test, y_train, y_val, y_test
                    training set, validation set and test set.
            """

            # Defines ratios, w.r.t. whole dataset.
            ratio_train = self.parser.getfloat('train_val_test_split', 'train_set_size')
            ratio_val = self.parser.getfloat('train_val_test_split', 'validation_set_size')
            ratio_test = self.parser.getfloat('train_val_test_split', 'test_set_size')
            random_state = self.parser.getint('train_val_test_split', 'random_state')

            # Produces test split.
            x_remaining, x_test, y_remaining, y_test = train_test_split(
                x, y, test_size=ratio_test, stratify=y, random_state=random_state)

            # Adjusts val ratio, w.r.t. remaining dataset.
            ratio_remaining = 1 - ratio_test
            ratio_val_adjusted = ratio_val / ratio_remaining

            # Produces train and val splits.
            x_train, x_val, y_train, y_val = train_test_split(
                x_remaining, y_remaining, test_size=ratio_val_adjusted, stratify=y_remaining, random_state=random_state)

            return x_train, x_val, x_test, y_train, y_val, y_test

        def load_and_preprocess(self):
            """
            This function load the dataset and preprocess it.
        
            This function does the following:
            1. Load Data
            2. Remove null data points, duplicate data points, outlier data points.
            3. Split the Data into training set, validation set and test set.
            4. Preprocess training set, validation set and test set.
            5. Create new DataFrame containing only cleaned text and encoded target values of
            training set, validation set and test set.
            6. Save the CleanedDataFrame csv file

            :return: None
            """
            meta_data_save_path = self.parser.get('save_path', 'meta_data_save_path')

            # Getting the path of dataset
            dataset_path = self.parser.get('dataset', 'dataset_path')
            complete_dataset_path = os.path.join(dataset_path, os.listdir(dataset_path)[0])

            input_column_name = None
            target_column_name = None

            # Get the name of text columns and target columns from self.parser.
            if self.parser.get('dataset', 'target_column') != 'None' and self.parser.get('dataset',
                                                                                         'target_column') != 'None':
                input_column_name = self.parser.get('dataset', 'input_column')
                target_column_name = self.parser.get('dataset', 'target_column')

            # Or get the index of text columns and target columns from self.parser if name is not available.
            elif self.parser.get('dataset', 'input_column_index') != 'None' and self.parser.get('dataset',
                                                                                                'target_column_index') != 'None':
                input_column_name_index = self.parser.get('dataset', 'input_column_index')
                target_column_name_index = self.parser.get('dataset', 'target_column_index')

            # Load the dataset removing datapoints having null target column.
            data = pd.read_csv(complete_dataset_path,encoding='latin1')
            n_datapoints = len(data)

            # Remove null target datapoints.
            data = preprocessing.remove_null_target_datapoints(data, target_column_name)
            n_null_target_datapoints = n_datapoints - len(data)
            print("length of data:",len(data))
            print('n_datapoints:', n_datapoints)
            print('n_null_target_datapoints:', n_null_target_datapoints)

            utility.meta_info_save(component_name='input_data',
                                   data={'n_datapoints': n_datapoints,
                                         'n_null_target_datapoints': n_null_target_datapoints},
                                   save_path=meta_data_save_path, pickle_name='meta_info_data_diagnostic.pkl')
            

            # Reset the index and generalise the column names
            text_dataframe = data[[input_column_name, target_column_name]].reset_index(drop=True)
            text_dataframe.columns = ['Text', 'Target']
            # save 10 samples as metadata sample_text_dataframe
            text_dataframe.head(10).to_csv(os.path.join(meta_data_save_path, 'sample_text_dataframe.csv'))

            # Remove duplicates
            if self.parser.getboolean('preprocessing', 'remove_duplicates'):
                print('remove_duplicates: ', self.parser.getboolean('preprocessing', 'remove_duplicates'))

                try:
                    n_datapoints_before_duplicates_removed = len(text_dataframe)
                    text_dataframe = preprocessing.remove_duplicate(dataframe=text_dataframe, column_name='Text')
                    n_duplicate = n_datapoints_before_duplicates_removed - len(text_dataframe)
                    n_duplicate_percent = n_duplicate / n_datapoints_before_duplicates_removed
                    # print("N_Duplicates:",n_duplicate)
                    # print('n_duplicate_percent', np.round(n_duplicate_percent, 3))

                    
                    utility.meta_info_save(component_name='duplicate',
                                           data={'n_duplicate': n_duplicate,
                                                 'n_duplicate_percent': np.round(n_duplicate_percent, 3)},
                                           save_path=meta_data_save_path, pickle_name='meta_info_data_diagnostic.pkl')

                except Exception as e:
                    print("Remove Duplicates Error: ")
                    raise e

            # Remove remove outliers
            if self.parser.getboolean('preprocessing', 'remove_outliers'):
                print('remove_outliers: ', self.parser.getboolean('preprocessing', 'remove_duplicates'))
                threshold = self.parser.getfloat('preprocessing', 'remove_outliers_threshold')

                try:
                    n_datapoints_before_outlier_removed = len(text_dataframe)
                    text_dataframe = preprocessing.remove_outlier(dataframe=text_dataframe, column_name='Text',
                                                                  threshold=threshold)
                    n_outlier = n_datapoints_before_outlier_removed - len(text_dataframe)
                    n_outlier_percent = n_outlier / n_datapoints_before_outlier_removed
                    # print('n_outlier', n_outlier)
                    # print('n_outlier_percent', np.round(n_outlier_percent, 3))

                    utility.meta_info_save(component_name='outlier',
                                           data={'n_outlier': n_outlier,
                                                 'n_outlier_percent': np.round(n_outlier_percent, 3)},
                                           save_path=meta_data_save_path, pickle_name='meta_info_data_diagnostic.pkl')

                except Exception as e:
                    print("Remove Outliers Error: ")
                    raise e

            # Remove those classes and data points where number of data points belongs to specific class is less than 3.
            if self.parser.getboolean('preprocessing', 'remove_class_less_then_k_datapoints'):
                print('remove_class_less_then_k_datapoints: ',
                      self.parser.getboolean('preprocessing', 'remove_class_less_then_k_datapoints'))

                # print("Before remove_class_less_then_k_datapoints shape ", text_dataframe.shape)
                k_samples = self.parser.getint('preprocessing', 'remove_class_less_then_k_datapoints_sample')
                text_dataframe = preprocessing.remove_class_less_then_k_datapoints(dataframe=text_dataframe,
                                                                                   target_column_name='Target',
                                                                                   k=k_samples)
                # print("After remove_class_less_then_k_datapoints shape ", text_dataframe.shape)

            # split data in training set, validation set and test set
            x_train, x_val, x_test, y_train, y_val, y_test = self._data_splitting(text_dataframe['Text'],
                                                                                  text_dataframe['Target'])

            # Saving training set, validation set, test set of text data
            dataframe_training_set = pd.DataFrame({'Text': x_train.values, 'Target': y_train.values})
            dataframe_val_set = pd.DataFrame({'Text': x_val.values, 'Target': y_val.values})
            dataframe_test_set = pd.DataFrame({'Text': x_test.values, 'Target': y_test.values})

            text_dataframe_save_path = self.parser.get('save_path', 'text_dataframe_save_path')
            utility.save_training_val_test_dataframe(dataframe_training_set, dataframe_val_set, dataframe_test_set,
                                                     text_dataframe_save_path, 'dataframe')

            # Encoding target labels
            label_encoder, (encoded_training_set_y, encoded_val_set_y, encoded_test_set_y) = label_encoder_transform(
                y_train.values, y_val.values, y_test.values)
            # Save the fitted LabelEncoder instance as pickle file
            utility.save_pickle_file(label_encoder, meta_data_save_path, 'label_encoder.pkl')
    ####################################################################
            print("\n Cleaning complete Data")
            cleaned_text_complete_data = preprocessing.clean_text_data(text_dataframe['Text'], self.parser)

    #################################################

            # preprocessing training-set validation-set and test-set of text data and Saving as a dataframe
            print("\n Cleaning Training Data")
            cleaned_text_training_set = preprocessing.clean_text_data(dataframe_training_set['Text'], self.parser)
            print("\n Cleaning Validation Data")
            cleaned_text_val_set = preprocessing.clean_text_data(dataframe_val_set['Text'], self.parser)
            print("\n Cleaning Testing Data")
            cleaned_text_test_set = preprocessing.clean_text_data(dataframe_test_set['Text'], self.parser)
    #########################################
            label_encoder = LabelEncoder()
            label_encoder.fit(text_dataframe['Target'])
            encoded_complete_data_y = label_encoder.transform(text_dataframe['Target'])
            cleaned_text_dataframe_data = pd.DataFrame(
                {'Text': cleaned_text_complete_data, 'Target': encoded_complete_data_y})

    ############################################
            # Saving training set, validation set, test set of cleaned text data and encoded target labels as dataframe.
            cleaned_text_dataframe_training_set = pd.DataFrame(
                {'Text': cleaned_text_training_set, 'Target': encoded_training_set_y})

            cleaned_text_dataframe_val_set = pd.DataFrame({'Text': cleaned_text_val_set, 'Target': encoded_val_set_y})

            cleaned_text_dataframe_test_set = pd.DataFrame({'Text': cleaned_text_test_set, 'Target': encoded_test_set_y})

            # Saving
            cleaned_dataframe_save_path = self.parser.get('save_path', 'cleaned_dataframe_save_path')
            utility.save_df(cleaned_text_dataframe_data,cleaned_dataframe_save_path,'cleaned_complete_dataframe')
            utility.save_training_val_test_dataframe(cleaned_text_dataframe_training_set, cleaned_text_dataframe_val_set,
                                                     cleaned_text_dataframe_test_set, cleaned_dataframe_save_path,
                                                     'cleaned_dataframe')

            # Plotting and meta info before and after preprocessing
            print("Saving meta plots and metadata..")
            # Class distribution plot
            if self.parser.getboolean('plots', 'class_distribution_plot'):
                fig = plot_utility.class_distribution_plot(text_dataframe['Target'])
                plot_save_path = self.parser.get('save_path', 'meta_data_save_path')
                utility.save_plot(fig, plot_save_path, 'class_distribution_plot.png')

            # Word count plot
            if self.parser.getboolean('plots', 'word_count_plot'):
                # Before preprocessing
                fig = plot_utility.word_counts_plot(dataframe_training_set['Text'], title='Before preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path,
                                  plot_name='word_count_before_preprocessing.png')

                # After preprocessing
                fig = plot_utility.word_counts_plot(cleaned_text_dataframe_training_set['Text'],
                                                    title='After preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path, plot_name='word_count_after_preprocessing.png')

            # Word Cloud plot
            if self.parser.getboolean('plots', 'word_cloud_plot'):
                # Before preprocessing
                fig = plot_utility.word_cloud_plot(dataframe_training_set['Text'], title='Before preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path,
                                  plot_name='word_cloud_before_preprocessing.png')

                # After preprocessing
                fig = plot_utility.word_cloud_plot(cleaned_text_dataframe_training_set['Text'], title='After preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path, plot_name='word_cloud_after_preprocessing.png')

            # Word frequency plot unigram and bigram
            if self.parser.getboolean('plots', 'word_frequency_plot'):
                # Before preprocessing
                fig = plot_utility.word_frequency_unigram_plot(dataframe_training_set['Text'], title='Before preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path,
                                  plot_name='word_frequency_unigram_before_preprocessing.png')

                # After preprocessing
                fig = plot_utility.word_frequency_unigram_plot(cleaned_text_dataframe_training_set['Text'],
                                                               title='After preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path,
                                  plot_name='word_frequency_unigram_after_preprocessing.png')

                # Before preprocessing
                fig = plot_utility.word_frequency_bigram_plot(dataframe_training_set['Text'], title='Before preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path,
                                  plot_name='word_frequency_bigram_before_preprocessing.png')

                # After preprocessing
                fig = plot_utility.word_frequency_bigram_plot(cleaned_text_dataframe_training_set['Text'],
                                                              title='After preprocessing')
                utility.save_plot(figure=fig, save_path=meta_data_save_path,
                                  plot_name='word_frequency_bigram_after_preprocessing.png')

            # Saving metadata
            train_val_test_split = {'n_datapoints_train': len(x_train), 'n_datapoints_val': len(x_val),
                                    'n_datapoints_test': len(x_test)}

            utility.meta_info_save(component_name='train_val_test_split', data=train_val_test_split,
                                   save_path=meta_data_save_path, pickle_name='meta_info_data_diagnostic.pkl')
            return None

        def embeddings_execute(self):
            """
            This function convert the text data into embedding vectors and save the embeddings of training set
            and validation set.
        
            This function does the following:
            1. Load the cleaned training set, validation set and test set.
            2. Remove any datapoints having null text columns if there are any after preprocessing.
            3. CountVectorizer embedding if flag is true.
             Save the Embeddings of training and validation set and instance of CountVectorizer
            3. TfidfVectoriser embedding if flag is true.
            Save the Embeddings of training and validation set and instance of TfidfVectoriser

            :return: None
            """
            # Load Cleaned text Data
            cleaned_dataframe_save_path = self.parser.get('save_path', 'cleaned_dataframe_save_path')
            
            # complete set
            cleaned_dataframe_complete_set = pd.read_csv(
                os.path.join(cleaned_dataframe_save_path, 'cleaned_complete_dataframe.csv'))
            # Removing the datapoints where text column is null after preprocessing if there is any.
            cleaned_dataframe_complete_set = cleaned_dataframe_complete_set[
                cleaned_dataframe_complete_set['Text'].notnull()]


            # Training set
            cleaned_dataframe_training_set = pd.read_csv(
                os.path.join(cleaned_dataframe_save_path, 'cleaned_dataframe_training_set.csv'))
            # Removing the datapoints where text column is null after preprocessing if there is any.
            cleaned_dataframe_training_set = cleaned_dataframe_training_set[
                cleaned_dataframe_training_set['Text'].notnull()]

            # Validation set
            cleaned_dataframe_validation_set = pd.read_csv(
                os.path.join(cleaned_dataframe_save_path, 'cleaned_dataframe_validation_set.csv'))
            # Removing the datapoints where text column is null after preprocessing if there is any.
            cleaned_dataframe_validation_set = cleaned_dataframe_validation_set[
                cleaned_dataframe_validation_set['Text'].notnull()]

            # Separating text and target columns
            complete_x=cleaned_dataframe_complete_set['Text']
            complete_y=cleaned_dataframe_complete_set['Target']

            train_x = cleaned_dataframe_training_set['Text']
            train_y = cleaned_dataframe_training_set['Target']
            val_x = cleaned_dataframe_validation_set['Text']
            val_y = cleaned_dataframe_validation_set['Target']

            # CountVectorizer Embeddings
            if self.parser.getboolean('embedding', 'embedding.countvectoriser.execution'):
                print("CountVectorizer Embeddings: ", self.parser.get('embedding', 'embedding.countvectoriser.execution'))

                count_vectoriser, count_vect_train_x, count_vect_val_x = count_vectorizer_embedding(train_x, val_x,
                                                                                                    self.parser)
                _, count_vect_complete_x, _ = count_vectorizer_embedding(complete_x, val_x,self.parser)

                # Saving Embeddings
                count_vect_embedding_save_path = self.parser.get('save_path', 'count_vect_embedding_save_path')

                # Saving CountVectorizer instance
                utility.save_pickle_file(file=count_vectoriser, save_path=count_vect_embedding_save_path,
                                         pickle_name='count_vectoriser.pkl')
                # Saving CountVectorizer vocabularies
                utility.save_pickle_file(file=count_vectoriser.get_feature_names(),
                                         save_path=count_vect_embedding_save_path,
                                         pickle_name='vocab.pkl')

                # Saving count_vect_train_x and train_y instance
                
                np.savetxt(count_vect_embedding_save_path+"/"+'count_vect_x.csv',count_vect_complete_x.toarray(), delimiter=',', fmt='%f')
                np.savetxt(count_vect_embedding_save_path+"/"+'vect_y.csv',complete_y, delimiter=',', fmt='%f')
                
                # utility.save_df(pd.DataFrame(count_vect_complete_x.toarray()),count_vect_embedding_save_path,'count_vect_complete_x')
                # utility.save_df(complete_y,count_vect_embedding_save_path,'count_vect_y')


                utility.save_pickle_file(file=count_vect_train_x, save_path=count_vect_embedding_save_path,
                                         pickle_name='count_vect_train_x.pkl')
                utility.save_pickle_file(file=train_y.values, save_path=count_vect_embedding_save_path,
                                         pickle_name='train_y.pkl')

                # Saving count_vect_val_x and val_y instance
                utility.save_pickle_file(file=count_vect_val_x, save_path=count_vect_embedding_save_path,
                                         pickle_name='count_vect_val_x.pkl')
                utility.save_pickle_file(file=val_y.values, save_path=count_vect_embedding_save_path,
                                         pickle_name='val_y.pkl')

            # TfidfVectoriser Embeddings
            if self.parser.getboolean('embedding', 'embedding.tfidfvectoriser.execution'):
                print("TfidfVectoriser Embeddings: ", self.parser.get('embedding', 'embedding.tfidfvectoriser.execution'))

                tfidf_vectoriser, standard_scalar, tfidf_vect_train_x, tfidf_vect_val_x = tfidf_vectorizer_embedding(
                    train_x,val_x,self.parser)
                _,_, tfidf_vect_complete_x, _ = tfidf_vectorizer_embedding(
                    complete_x,val_x,self.parser)

                # Saving Embeddings
                tfidf_vect_embedding_save_path = self.parser.get('save_path', 'tfidf_vect_embedding_save_path')

                # Saving TfidfVectoriser instance and StandardScalar instance
                utility.save_pickle_file(file=tfidf_vectoriser, save_path=tfidf_vect_embedding_save_path,
                                         pickle_name='tfidf_vectoriser.pkl')
                utility.save_pickle_file(file=standard_scalar, save_path=tfidf_vect_embedding_save_path,
                                         pickle_name='standard_scalar.pkl')
                # Saving TfidfVectoriser vocabularies
                utility.save_pickle_file(file=tfidf_vectoriser.get_feature_names(),
                                         save_path=tfidf_vect_embedding_save_path,
                                         pickle_name='vocab.pkl')

                # Saving tfidf_vect_train_x and train_y instance
                np.savetxt(tfidf_vect_embedding_save_path+"/"+'tfidf_vect_x.csv',tfidf_vect_complete_x.toarray(), delimiter=',', fmt='%f')
                
                np.savetxt(tfidf_vect_embedding_save_path+"/"+'vect_y.csv',complete_y, delimiter=',', fmt='%f')

                utility.save_pickle_file(file=tfidf_vect_train_x, save_path=tfidf_vect_embedding_save_path,
                                         pickle_name='tfidf_vect_train_x.pkl')
                utility.save_pickle_file(file=train_y.values, save_path=tfidf_vect_embedding_save_path,
                                         pickle_name='train_y.pkl')

                # Saving tfidf_vect_train_x and val_y instance
                utility.save_pickle_file(file=tfidf_vect_val_x, save_path=tfidf_vect_embedding_save_path,
                                         pickle_name='tfidf_vect_val_x.pkl')
                utility.save_pickle_file(file=val_y.values, save_path=tfidf_vect_embedding_save_path,
                                         pickle_name='val_y.pkl')

            return None

    # from configparser import ConfigParser, ExtendedInterpolation
    # data_diagnostic_parser = ConfigParser(interpolation=ExtendedInterpolation())
    # data_diagnostic_parser.read('./Configs/config_data_diagnostic.properties')
    # data_diagnostic = DataDiagnostic(data_diagnostic_parser)
    # # load_and_preprocess function execution
    # data_diagnostic.load_and_preprocess()
else:
    print("Invalid License")