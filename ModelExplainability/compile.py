from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("fairml_exp",  ["fairml_exp.py"]),
    Extension("global_explainability",  ["global_explainability.py"]),
    Extension("local_explainability",  ["local_explainability.py"])
]

setup(
    name = 'My Program Name',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)