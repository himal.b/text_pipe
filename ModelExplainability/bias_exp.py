import pickle
import pandas as pd
import operator
import os
import matplotlib.pyplot as plt
from fairml import audit_model
from fairml import plot_dependencies
import eli5

def fairml_text(model,train_features,feature_names,n_features):
    """
    The Fairness Indicators enables computation and visualization
    of commonly-identified fairness metrics for classification models
    and performs evaluation over multiple thresholds
    :param model:
    :param train_features:
    :param feature_names:
    :param n_features:
    :return:
      fig: The plot of fairml Text
    """
    
    train_df = pd.DataFrame(train_features.toarray(),columns =feature_names)

    importances, _ = audit_model(model.predict,train_df)
    # print feature importance
    # print(importances)
    imp_med = importances.median()
    imp_dict = {}
    for k,v in sorted(imp_med.items(), key=operator.itemgetter(1))[:n_features]:
        imp_dict[k] = v
    for k,v in sorted(imp_med.items(), key=operator.itemgetter(1),reverse=True)[:n_features]:
        imp_dict[k] = v
    
    fig = plot_dependencies(imp_dict,reverse_values=False,title="FairML feature dependence")
    fig.set_size_inches(10, 6)
    return fig

def eli5_exp(model,vectorizer,X_test,class_names,idx):
  """
    ELI5 is a Python library which allows to visualize
    and debug various Machine Learning models using unified API
    which returns eli5 weights and predictions as dataframe.
    :param model:
    :param vectorizer:
    :param X_test: 
    :param class_names:
    :param idx
    :return:eli_weights,eli_pred
    """
  eli_weights = eli5.formatters.as_dataframe.explain_weights_df(model, vec=vectorizer, top=10,
                  target_names=class_names)  
  eli_pred = eli5.explain_prediction_df(model, X_test[idx], vec=vectorizer,
                     target_names=class_names)
  return eli_weights,eli_pred