import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import shap
from sklearn.tree import DecisionTreeClassifier

# Use dark background
# plt.style.use('dark_background')


def decision_tree_surrogate(model, X_test, feature_names, n_features=20):
    """

    :param model:
    :param X_test:
    :param feature_names:
    :param n_features:

    :return:
        fig: plot of Decision tree feature importance
    """
    new_target = model.predict(X_test)
    decision_tree_model = DecisionTreeClassifier()
    decision_tree_model.fit(X_test, new_target)

    importance = decision_tree_model.feature_importances_

    decision_tree_model_imp_feat = pd.DataFrame(data=importance, index=feature_names, columns=['Values'])

    decision_tree_sorted_feat_imp = decision_tree_model_imp_feat.sort_values(by=['Values'], ascending=False)
    top_imp_features = decision_tree_sorted_feat_imp[0: n_features]

    # plotting top important features
    fig = plt.figure(figsize=(15, 7))
    sns.barplot(top_imp_features.index, top_imp_features.values.ravel())
    sns.set(font_scale=1)
    plt.xlabel("Features")
    plt.ylabel("Importance")
    plt.title(f"Decision Tree Surrogate Feature importance\n", fontsize=15)
    plt.xticks(rotation=70)

    return fig, top_imp_features


def logistic_regression_feature_importance(model, feature_names, class_names, n_features):
    """

    :param model:
    :param feature_names:
    :param class_names:
    :param n_features:
    :return:
        fig: sklearn feature importance plots
    """

    figures = list()
    for index, class_name in enumerate(class_names):
        logistic_reg_coef = model.coef_[index-1]
        top_coef_index = np.argsort(np.abs(logistic_reg_coef))[::-1][:n_features]

        fig = plt.figure(figsize=(15, 7))
        sns.barplot(x=logistic_reg_coef[top_coef_index], y=np.array(feature_names)[top_coef_index])
        plt.suptitle("Logistic Regression Top " + str(n_features) + " Coefficients", fontsize=12,
                     fontweight="normal")
        max_value = logistic_reg_coef[top_coef_index].max()
        plt.text(max_value, -1, f'class name: {str(class_name)}', c='r', fontsize=12)

        figures.append(fig)
    return figures


def shap_global_linear_explainer(model, x_train, x_test, feature_names, class_names, n_samples=1000):
    """

    :param model:
    :param x_train:
    :param x_test:
    :param feature_names:
    :param class_names:
    :param n_samples:
    :return:
        fig: shap linear explainer plot
    """
    explainer = shap.LinearExplainer(model, x_train, feature_dependence="independent")
    shap_values = explainer.shap_values(shap.sample(x_test, n_samples))

    # Plotting
    fig = plt.figure(figsize=(15, 7))
    shap.summary_plot(shap_values, x_test[:n_samples], feature_names, plot_type="bar", class_names=class_names,
                      show=False, axis_color='#FFFFFF')

    return fig


def random_forest_feature_importance(model, feature_names, n_features):
    """

    :param model:
    :param feature_names:
    :param n_features:
    :return:
        fig: Random Forest Feature Importance plot
    """

    importance = model.feature_importances_
    top_coef_index = np.argsort(np.abs(importance))[::-1][:n_features]

    # plotting
    fig = plt.figure(figsize=(15, 7))

    sns.barplot(x=importance[top_coef_index], y=np.array(feature_names)[top_coef_index])
    plt.suptitle(f"Random Forest Top {str(n_features)} Features", fontsize=12, fontweight="normal")

    return fig


def shap_global_tree_explainer(model, x_test, feature_names, class_names, n_samples_train=100,
                               n_samples_test=100):
    """

    :param n_samples_test:
    :param model:
    :param x_test:
    :param feature_names:
    :param class_names:
    :param n_samples_train:
    :return:
    """
    explainer = shap.TreeExplainer(model)
    x_test=x_test.toarray()
    shap_values = explainer.shap_values(shap.sample(x_test, n_samples_test), check_additivity=False)

    # plotting
    fig = plt.figure()
    shap.summary_plot(shap_values, shap.sample(x_test, n_samples_test), feature_names, plot_type="bar", class_names=class_names,
                      show=False, axis_color='#FFFFFF')

    return fig
