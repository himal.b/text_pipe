import matplotlib.pyplot as plt
import pickle
import lime
from lime import lime_text
from lime.lime_text import LimeTextExplainer
from sklearn.pipeline import make_pipeline
import numpy as np
import textwrap
import shap 
# import eli5
from sklearn.metrics import confusion_matrix
shap.initjs()

# Use dark background
# plt.style.use('seaborn-white')
# plt.style.use('dark_background')

def perf_measure(y_true,y_pred):

  """
    This function measures the performance of
    model prediction with the true labels and 
    returns the False_positive_index,
    False_negative_index
    :param y_true:
    :param y_pred:
    :return
    """
  labels = set(y_true)
  cm=np.zeros((len(labels),len(labels)),'int')
  fp=[]
  fn=[]
  for i in range(0,len(fp)):
    fp[i]=[]
  for i in range(0,len(fn)):
    fn[i]=[]

  for i in range(0,len(y_true)):
    if y_true[i]==y_pred[i]:
      
      cm[y_true[i]][y_pred[i]]+=1
      
    else:
      cm[y_true[i]][y_pred[i]]+=1
      if y_true[i] > y_pred[i]:
        fp.append(i)

      else:
        fn.append(i) 
        
    
  False_positive_index = fp
  False_negative_index = fn
  return False_positive_index,False_negative_index   


def lime_exp(model,vectorizer,X_test,y_test,class_names,idx,num_features):
  """
  Lime helps to illuminate a machine learning model 
  and to make its predictions individually comprehensible
    :param model:
    :param vectorizer:
    :param X_test:
    :param y_test:
    :param feature_names:
    :param idx:
    :param num_features:
    :return:
      exp: The Lime Text Explainability
    """
  c = make_pipeline(vectorizer, model)
  classes = np.unique(class_names)
  explainer = LimeTextExplainer(class_names=classes)
  exp = explainer.explain_instance(X_test[idx], c.predict_proba,
   num_features=num_features)
  return exp

def shap_local(model,train_features,test_features_array,idx,class_names,feature_names):
  """
  The core idea behind Shapley value-based explanations of
  machine learning models is to use fair allocation results
  for a model’s output among its input features
    :param model:
    :param train_features:
    :param test_features_array: 
    :param idx
    :param feature_names
    :return
      fig: shap force plot
    """
  explainershap = shap.Explainer(model,train_features)
  choosen_instance = test_features_array[idx]
  shap_values = explainershap.shap_values(choosen_instance)
  if len(np.unique(class_names))<3:
    fig = shap.force_plot(explainershap.expected_value, shap_values, choosen_instance,
      feature_names=feature_names,show=False,matplotlib=True)
  else:
    fig = shap.force_plot(explainershap.expected_value[0], shap_values[0], choosen_instance,
      feature_names=feature_names,show=False,matplotlib=True)

  return fig

def shap_tree_local(model,test_features_array,idx,class_names,feature_names):
  """
  The core idea behind Shapley value-based explanations of
  machine learning models is to use fair allocation results
  for a model’s output among its input features
    :param model:
    :param test_features_array: 
    :param idx
    :param feature_names
    :return
      fig: shap force plot
    """
  explainershap = shap.TreeExplainer(model)
  choosen_instance = test_features_array[idx]
  shap_values = explainershap.shap_values(choosen_instance)
  if len(np.unique(class_names))<3:
    fig = shap.force_plot(explainershap.expected_value, shap_values, choosen_instance,
      feature_names=feature_names,show=False,matplotlib=True)
  else:
    fig = shap.force_plot(explainershap.expected_value[0], shap_values[0], choosen_instance,
      feature_names=feature_names,show=False,matplotlib=True)

  return fig
