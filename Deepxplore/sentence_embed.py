import numpy as np
import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
import re
from bs4 import BeautifulSoup
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from configparser import ConfigParser, ExtendedInterpolation
nltk.download('punkt')
# data_path = "./Dataset/cleaned_complete_dataframe.csv"
# # data_path = './Results/DataDiagnostic/CleanedTextData/cleaned_complete_dataframe.csv'
# input_column = 'Text'
# target_column = 'Target'
# parser = ConfigParser(interpolation=ExtendedInterpolation())
# parser.read('./Configs/config_deepxplore.properties')

def sen_embed(data_path, input_column, target_column):
  # data_path = parser.get('path','data_path')
  # input_column = parser.get('path','input_column')
  # target_column = parser.get('path','target_column')
    # fname='/content/cleaned_complete_dataframe.csv'
  df = pd.read_csv(data_path)
  df = df.dropna()
  # print(df.isnull().sum(),"NULL")

  tokenized_sent = []
  for s in df[input_column]:
    # print(s,"S")
    tokenized_sent.append(word_tokenize(s))
  tagged_data = [TaggedDocument(d, [i]) for i, d in enumerate(tokenized_sent)]
  model = Doc2Vec(tagged_data, vector_size = 300, window = 2, min_count = 1, epochs = 100)
  i=0
  features=np.zeros((np.shape(df)[0],300))
  for s in df[input_column]:
      test_doc = word_tokenize(s)
      features[i,:] = model.infer_vector(test_doc)
      i=i+1

  score=np.array(df[target_column])
  if len(df[target_column].unique()) >2:
      score = score-1

  # score = score-1

  sc=np.zeros((len(score)))
  for i in df[target_column].unique():
    in1=np.where(score==i)
    sc[in1[0]]=i

  sc=sc.astype(int)

  data=np.concatenate((features,sc.reshape(-1,1)), axis=1)
  print('done')
  return data
# data = sen_embed(data_path, input_column, target_column)
# fname='./Results/DeepXplore/sen_atis.csv'
# # fname='C:/Users/lov/Downloads/sen.csv'
# np.savetxt(fname,data, delimiter=',', fmt='%f')