# import params
# from params import bcolors
from keras.layers import Activation, Input, Dense
from keras.utils import to_categorical
from keras.models import Model
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler


def normalizedata(X_train):
    scaler = MinMaxScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    return X_train, scaler



def Model1(input_tensor=None,output_features=1,load_weights=False,load_weights_1_path=None, num_features=100): #,load_weights_path=None # original dave
    if input_tensor is None:
        input_tensor = Input(shape=(num_features,))
    x = Dense(num_features, input_dim=num_features, activation='relu', name='fc1')(input_tensor)
    x = Dense(num_features, activation='relu', name='fc2')(x)
    x = Dense(output_features, name='before_softmax')(x)
    x = Activation('softmax', name='predictions')(x)
    m = Model(input_tensor, x)
    if load_weights:
        try:
            m.load_weights(load_weights_1_path)
            # m.load_weights('C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline/Deepxplore/Model/model'+str(fn)+'1.h5')
        except:
            print('Model1 weights was not loaded!') # due to conflict in keras h5py version, change it to "h5py==2.10.0"

    # compiling
    m.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return m


def Model2(input_tensor=None,output_features=1, load_weights=False,load_weights_2_path=None, num_features=100):  # original dave with normal initialization
    if input_tensor is None:
        input_tensor = Input(shape=(num_features,))
    x = Dense(num_features, input_dim=num_features, activation='relu', name='fc1')(input_tensor)
    x = Dense(num_features, activation='relu', name='fc2')(x)
    x = Dense(num_features, activation='relu', name='fc3')(x)
    x = Dense(output_features, name='before_softmax')(x)
    x = Activation('softmax', name='predictions')(x)
    m = Model(input_tensor, x)
    if load_weights:
        try:
            m.load_weights(load_weights_2_path)
            # m.load_weights('C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline/Deepxplore/Model/model'+str(fn)+'2.h5')
        except:
            print('Model2 weights was not loaded!')

    # compiling
    m.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return m


def Model3(input_tensor=None,output_features=1, load_weights=False,load_weights_3_path=None, num_features=100):  # simplified dave
    if input_tensor is None:
        input_tensor = Input(shape=(num_features,))
    x = Dense(num_features, input_dim=num_features, activation='relu', name='fc1')(input_tensor)
    x = Dense(num_features, activation='relu', name='fc2')(x)
    x = Dense(num_features, activation='relu', name='fc3')(x)
    x = Dense(num_features, activation='relu', name='fc4')(x)
    x = Dense(output_features, name='before_softmax')(x)
    x = Activation('softmax', name='predictions')(x)
    m = Model(input_tensor, x)
    if load_weights:
        try:
            m.load_weights(load_weights_3_path)
            # m.load_weights('C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline/Deepxplore/Model/model'+str(fn)+'3.h5')
        except:
            print('Model3 weights was not loaded!')
    # compiling
    m.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return m


def modeldev(data,batch_size,nb_epoch,model1_path,model2_path,model3_path):
    # batch_size = 64
    # nb_epoch = 10
    x_train, x_test,  y_train, y_test = train_test_split(data[:,0:-1], data[:,-1], test_size=0.3, random_state=10)           
    X_train = x_train.astype('float32')
    X_test = x_test.astype('float32')
    y_train = y_train.astype('int')
    y_test = y_test.astype('int')
    output_features = len(np.unique(y_train))
    y_train = to_categorical(y_train,output_features)
    y_test = to_categorical(y_test,output_features)
    for model_name in range(1,4):
        if model_name == 1:
            model = Model1(output_features=output_features,num_features=np.shape(X_train)[1])
            save_model_name = model1_path
            # save_model_name = "C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline/Deepxplore/Model/model"+str(fn)+"1.h5"
        elif model_name == 2:
            model = Model2(output_features=output_features,num_features=np.shape(X_train)[1])
            save_model_name = model2_path
            # save_model_name ='C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline/Deepxplore/Model/model'+str(fn)+'2.h5'
        elif model_name == 3:
            model = Model3(output_features=output_features,num_features=np.shape(X_train)[1])
            save_model_name = model3_path
            # save_model_name = 'C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline/Deepxplore/Model/model'+str(fn)+'3.h5'
        # print("size:",batch_size,nb_epoch)
        model.fit(X_train, y_train, batch_size=batch_size, epochs=nb_epoch, validation_data=(X_test, y_test), verbose=1)
        model.save_weights(save_model_name)
    print("Model weights saved")

