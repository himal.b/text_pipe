from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("modeldevlopment",  ["modeldevlopment.py"]),
    Extension("params",  ["params.py"]),
    Extension("utils",  ["utils.py"])
]

setup(
    name = 'My Program Name',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)