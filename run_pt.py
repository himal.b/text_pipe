import performance_testing_execute as pte
import sys
# import main 
try:
    if __name__ == "__main__":
        pte.main_script()
    sys.exit(0)
except SystemExit:
    print("Program terminated with sys.exit()")
except:
    print("Performance Testing did not execute successfully")
    sys.exit(0)
finally:
    print("Done")