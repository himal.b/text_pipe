from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize

ext_modules = [
    # Extension("app_text",  ["app_text.py"])
    # Extension("attack_vectors_execute",  ["attack_vectors_execute.py"]),
    # Extension("counterfactuals_execute",  ["counterfactuals_execute.py"]),

    # Extension("data_diagnostic_execute",  ["data_diagnostic_execute.py"]),
    # Extension("deepxplore_implementation",  ["deepxplore_implementation.py"]),
    # Extension("json_report_generation",  ["json_report_generation.py"]),
    # Extension("main",  ["main.py"]),
    # Extension("model_explainability_execute",  ["model_explainability_execute.py"]),
    # Extension("modeling_execute",  ["modeling_execute.py"]),
    # Extension("performance_testing_execute",  ["performance_testing_execute.py"]),
    Extension("license_execute",  ["license_execute.py"])
    # Extension("license_execute_pt",  ["license_execute_pt.py"]),
    # Extension("license_flask_execute",  ["license_flask_execute.py"])
]

setup(
    name='Test Compile',
    cmdclass={'build_ext': build_ext},
    ext_modules=cythonize(ext_modules,
                          compiler_directives={'always_allow_keywords': True}, # 'always_allow_keywords' for avoid flask error
                          )

)