import distutils.dir_util
import logging
import os
import pickle
import datetime
import shutil
import time
import mlflow

sep = "/"


class PassError(Exception):
    """Base class for exceptions in this module."""
    pass

class bcolors:
    OK = '\033[92m'
    WARNING = '\033[93m' #YELLOW
    FAIL = '\033[91m' #RED
    RESET = '\033[0m' #RESET COLOR
    green = '\033[0;32;47m'
    
def remove_previous_result():
    print('Removing previous results if any..')

    try:
        # Copy Previous Results directory
        if os.path.isdir('./Results'):
            date_time = str(datetime.datetime.now()).split(' ')

            time = date_time[1].split('.')[0].split(':')
            time_path = os.path.join(date_time[0], str(time[0] + '-' + time[1] + '-' + time[2]))

            distutils.dir_util.mkpath(os.path.join('./PreviousResults', time_path) , verbose=True)

        shutil.move('./Results', os.path.join('./PreviousResults', time_path))
        shutil.rmtree('./Results')
        print('"Result" folder is successfully moved')

    except Exception as e:
        print(e.__class__)
        print('The directory name "Results" is not found')

    try:
        # removing Results director
        shutil.rmtree('./Results')
        print('"Result" folder is successfully removed')
    except Exception as e:
        print(e.__class__)
        print('The directory name "Results" is not found')

    try:
        # removing Results directory
        shutil.rmtree('./mlruns')
        print('"mlruns" folder is successfully removed')
    except Exception as e:
        print(e.__class__)
        print('The directory name "mlruns" is not found')
        
    try:
        # removing Results directory
        shutil.rmtree('./logging')
        print('"logging" folder is successfully removed')
    except Exception as e:
        print(e.__class__)
        print('The directory name "logging" is not found')

    try:
        # removing Results directory
        # removing mlruns database
        os.remove('./mlruns.db')
        print('"mlruns.db"  is successfully removed')
    except Exception as e:
        print(e.__class__)
        print('The file name "mlruns.db" is not found')
    return None


def logger_setup(name, log_file_name, level=logging.DEBUG):
    os.makedirs('logging', exist_ok=True)
    formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s:%(filename)s:%(module)s')
    handler = logging.FileHandler(os.path.join('logging', log_file_name))
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger


def save_pickle_file(file, save_path, pickle_name):
    """Utility function for dumping a pickle file."""
    distutils.dir_util.mkpath(save_path, verbose=True)
    with open(os.path.join(save_path, pickle_name), "wb") as f:
        pickle.dump(file, f)

    return None


def save_plot(figure, save_path, plot_name):
    """ Utility function to save plots.
    :param figure: Matplotlib Figure.
    :param save_path: str;
        Path where the plot will be saved.
    :param plot_name: str;
        Name of the plot with postfix literals. {.png or .jpg etc..}

    :return:
        None
    """
    # Create the path if not exists
    distutils.dir_util.mkpath(save_path, verbose=True)
    figure.savefig(os.path.join(save_path, plot_name))
    return None
    
def save_html(figure, save_path, plot_name):
    """ Utility function to save plots.
    :param figure: Matplotlib Figure.
    :param save_path: str;
        Path where the plot will be saved.
    :param plot_name: str;
        Name of the plot with postfix literals. {.png or .jpg etc..}

    :return:
        None
    """
    # Create the path if not exists
    distutils.dir_util.mkpath(save_path, verbose=True)
    figure.save_to_file(os.path.join(save_path, plot_name))
    return None

def meta_info_save(component_name, data, save_path, pickle_name):
    """ Utility function for saving metadata. If pickle is not created, it will create a empty
    pickle file as given pickle_name and save the meta info """
    try:

        with open(os.path.join(save_path, pickle_name), 'rb') as f:
            meta_dict = pickle.load(f)
            meta_dict[component_name] = data

        with open(os.path.join(save_path, pickle_name), "wb") as f:
            pickle.dump(meta_dict, f)

    except Exception as e:
        print("Meta info directory not found. Creating new directory ")

        # Create the path if not exists
        distutils.dir_util.mkpath(save_path, verbose=True)
        meta_dict = dict()

        with open(os.path.join(save_path, pickle_name), "wb") as f:
            meta_dict[component_name] = data
            pickle.dump(meta_dict, f)

        PassError(e)
    return None


def yield_artifacts(run_id, path=None):
    """Yield all artifacts in the specified run
    Credit: https://github.com/mlflow/mlflow/blob/master/examples/sklearn_autolog/utils.py"""
    client = mlflow.tracking.MlflowClient()
    for item in client.list_artifacts(run_id, path):
        if item.is_dir:
            yield from yield_artifacts(run_id, item.path)
        else:
            yield item.path


def fetch_logged_data(run_id):
    """ Fetch params, metrics, tags, and artifacts in the specified run
    Credit: https://github.com/mlflow/mlflow/blob/master/examples/sklearn_autolog/utils.py"""
    client = mlflow.tracking.MlflowClient()
    data = client.get_run(run_id).data

    tags = {k: v for k, v in data.tags.items() if not k.startswith("mlflow.")}
    artifacts = list(yield_artifacts(run_id))
    return {
        "params": data.params,
        "metrics": data.metrics,
        "tags": tags,
        "artifacts": artifacts,
    }


def save_training_val_test_dataframe(training_set, val_set, test_set, save_path, initials):
    """ Utility function to save training set, validation set and test set.
    :param training_set: DataFrame of training set
    :param val_set: DataFrame of validation set
    :param test_set: DataFrame of test set.
    :param save_path: str;
        Path to save DataFrame
    :param initials: str;
        Initial name of DataFrame {'text_dataframe' or 'cleaned_text_dataframe'}

    :return:
        None
    """

    # Create the path if not exists
    distutils.dir_util.mkpath(save_path, verbose=True)

    # Saving csv files
    training_set.to_csv(os.path.join(save_path, f'{initials}_training_set.csv'))
    val_set.to_csv(os.path.join(save_path, f'{initials}_validation_set.csv'))
    test_set.to_csv(os.path.join(save_path, f'{initials}_test_set.csv'))
    print("Dataframe Saved")
    return None

def save_df(data, save_path,initials):
    # Create the path if not exists
    distutils.dir_util.mkpath(save_path, verbose=True)
    # Saving csv files
    data.to_csv(os.path.join(save_path,f'{initials}.csv'))
    return None

def mlflow_logger(experiment_name, metrics=[], params = [], artifacts = [], tags={}):
    try:
        mlflow.create_experiment(name = experiment_name)
    except:
        mlflow.set_experiment(experiment_name)

    with mlflow.start_run(run_name = experiment_name):
        for artifact in artifacts:
            mlflow.log_artifact(artifact)

        for metric in metrics:
            mlflow.log_metric(metric)

        for key, val in tags.items():
            mlflow.set_tag(key, val)

        for param in params:
            mlflow.log_params(param)
