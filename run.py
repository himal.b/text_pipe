import sys
import main 
try:
    if __name__ == "__main__":
        main.main_script()
    sys.exit(0)
except SystemExit:
    print("Program terminated with sys.exit()")
except:
    print("Pipeline did not execute successfully")
    sys.exit(0)
finally:
    print("Done")


