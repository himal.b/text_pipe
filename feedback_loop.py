import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
	import pandas as pd
	import numpy as np
	import os
	import json
	from sklearn.metrics import classification_report, accuracy_score
	from DataDiagnostic import preprocessing
	import pickle
	from data_diagnostic_execute import DataDiagnostic, DataDiagnosticsError
	import pickle
	from configparser import ConfigParser, ExtendedInterpolation
	from Utility import utility as ut

	color= ut.bcolors


	class PassError(Exception):
	    """Base class for exceptions in this module."""
	    pass

	def aug_csv_reader(aug_results_path):
		aug_df = pd.read_csv(aug_results_path)
		#stacking all augmented columns into one column
		all_aug_df = aug_df.iloc[:,3:].T.stack().reset_index()[0]
		#concatinating class labels
		orig_aug_df_class = pd.DataFrame(aug_df.iloc[:,2].tolist()*len(aug_df.iloc[:,3:].columns))
		return all_aug_df, orig_aug_df_class

	def ta_csv_reader(ta_path):
	    ta_list=[]
	    ta_class=[]
	    df_ta = pd.DataFrame()
	    for csv in os.listdir(ta_path):
	        # print(csv)
	        df = pd.read_csv(ta_path+csv)
	        df = df[['original_text','perturbed_text','ground_truth_output']].loc[(df['result_type'] == 'Successful') | (df['result_type'] =='Maximized')]
	        ta_list.append(df['perturbed_text'])
	        ta_class.append(df['ground_truth_output'])
	    df_ta['textattack'] = pd.concat(ta_list)
	    df_ta['original_label'] = pd.concat(ta_class)
	    return df_ta


	def augmentation_data_to_df():

		# augmentation:
		#config parser to load augmentation data
		aug_parser = ConfigParser(interpolation=ExtendedInterpolation())
		aug_parser.read('./Configs/config_augmentation.properties')

		if aug_parser.getboolean('augment_type', 'parameters.sentences'):
			folder_path = aug_parser.get('save_path','aug_results_sent')
			aug_results_path = folder_path+'/'+os.listdir(folder_path)[0]
			all_aug_sent , orig_aug_sent_class = aug_csv_reader(aug_results_path)
		else:
			all_aug_sent = None
			orig_aug_sent_class = None

		if aug_parser.getboolean('augment_type', 'parameters.words'):
			folder_path = aug_parser.get('save_path','aug_results_word')
			aug_results_path = folder_path+'/'+os.listdir(folder_path)[0]
			all_aug_word , orig_aug_word_class = aug_csv_reader(aug_results_path)	
		else:
			all_aug_word = None
			orig_aug_word_class = None

		if aug_parser.getboolean('augment_type', 'parameters.characters'):
			#reading augmented dataframe
			folder_path = aug_parser.get('save_path','aug_results_char')
			aug_results_path = folder_path+'/'+os.listdir(folder_path)[0]
			all_aug_char, orig_aug_char_class = aug_csv_reader(aug_results_path)
		else:
			all_aug_char = None
			orig_aug_char_class = None

		#concatinating each level augmentation
		x_aug_combine = pd.DataFrame(columns = ['augmentation','orig_sent_class'])
		x_aug_combine['augmentation'] = pd.concat([all_aug_sent,all_aug_word,all_aug_char])
		x_aug_combine['orig_sent_class'] =  pd.concat([orig_aug_sent_class,orig_aug_word_class,orig_aug_char_class])
		return x_aug_combine

	def counterfactuals_data_to_df():


		#config parser for counterfactual data
		cf_parser = ConfigParser(interpolation=ExtendedInterpolation())
		cf_parser.read('./Configs/config_counterfactuals.properties')	
		folder_path = cf_parser.get('save_path','cf_results')
		# print(sep.join(folder_path,os.listdir(folder_path)[0]))
		cf_results_path = folder_path + '/' + os.listdir(folder_path)[0]
		#reading counterfactuals dataframe
		cf_data_csv = pd.read_csv(cf_results_path)

		return cf_data_csv

	def text_attacks_data_to_df():

		# textattack:
		df_ta_combine = pd.DataFrame(columns = ['textattack','original_label'])
		#config parser to load textattacks data
		ta_parser = ConfigParser(interpolation=ExtendedInterpolation())
		ta_parser.read('./Configs/config_attack_vectors.properties')		
		
		if ta_parser.getboolean('embedder_type','parameters.cv_tokenizer'):
			ta_cv_path = ta_parser.get('save_path','attack_results_cv_csv_path')
			df_ta_cv = ta_csv_reader(ta_cv_path)
		else:
			df_ta_cv = pd.DataFrame(columns = ['textattack','original_label'])
		
		if ta_parser.getboolean('embedder_type','parameters.tfidf_tokenizer'):
			ta_tfidf_path = ta_parser.get('save_path','attack_results_tfidf_csv_path')
			df_ta_tfidf = ta_csv_reader(ta_tfidf_path)
		else:
			df_ta_tfidf = pd.DataFrame(columns = ['textattack','original_label'])

		df_ta_combine = pd.concat([df_ta_cv,df_ta_tfidf])

		return df_ta_combine


	def retrain(logger):
			
		#config parser for feedback_loop
		feedback_parser = ConfigParser(interpolation=ExtendedInterpolation())
		feedback_parser.read('./Configs/config_feedbackloop.properties')

		# config parser for modeling paths
		model_parser = ConfigParser(interpolation=ExtendedInterpolation())
		model_parser.read('./Configs/config_modeling.properties')

		# save path for feedback lop results
		result_path = feedback_parser.get('save_path', 'folder_path') 
		
		# reading the data from prescribed paths
		# augmentation:
		try:
			if feedback_parser.getboolean('Augmentation','set_execution'):
				x_aug_combine = augmentation_data_to_df()
			else:
				x_aug_combine = pd.DataFrame(columns = ['augmentation','orig_sent_class'])
		
		except Exception as e:
			print("\n *** augmentation data not available *** ")
			logger.critical(f'*** Please check for augmentation data in results folder *** \n{e}')
			PassError(e)

		
		# counterfactuals:
		try:
			if feedback_parser.getboolean('Counterfactuals', 'set_execution'):
				cf_data = counterfactuals_data_to_df()
			else:
				cf_data = pd.DataFrame(columns = ['perturbed','original_label'])
		except Exception as e:
			print("\n *** counterfactuals data not available *** ")
			logger.critical(f'*** Please check for counterfactuals data in results folder *** \n{e}')
			PassError(e)

		# textattack:
		try:
			if feedback_parser.getboolean('Text_Attacks', 'set_execution'):
				df_ta_combine = text_attacks_data_to_df()
			else:
				df_ta_combine = pd.DataFrame(columns = ['textattack','original_label'])
		except Exception as e:
			print("\n *** text attacks data not available *** ")
			logger.critical(f'*** Please check for text attacks data in results folder *** \n{e}')
			PassError(e)

		#starting the loop
		if feedback_parser.getboolean('Augmentation','set_execution') | feedback_parser.getboolean('Counterfactuals', 'set_execution')| feedback_parser.getboolean('Text_Attacks', 'set_execution'):
			
			try:
				# Create feedbackloop folder
				if not os.path.exists(result_path):
					os.makedirs(result_path)
			
				# loading the pre-saved label_encoder from modeling
				path_to_file = model_parser.get('load_path','data_diagnostic_metadata_save_path')
				file = open(path_to_file + '/label_encoder.pkl','rb')
				le = pickle.load(file)
				le_dict = dict(zip(le.classes_, le.transform(le.classes_)))

				vectoriser_to_retrain = feedback_parser.get('retrain','vectoriser_to_load') # 'count' or 'tfidf'

				#reading vectoriser and model pickle file
				if vectoriser_to_retrain == 'count':
					path_to_vector = model_parser.get('load_path','count_vect_embedding_load_path')
					vec = pickle.load(open(path_to_vector + '/count_vectoriser.pkl','rb'))
				else:
					path_to_vector = model_parser.get('load_path','tfidf_vect_embedding_load_path')
					vec = pickle.load(open(path_to_vector + '/tfidf_vectoriser.pkl','rb'))
				
				# loading saved model

				path_to_model = model_parser.get("save_path","model_save_path")
				model = pickle.load(open(path_to_model + '/model.pkl','rb'))
			except Exception as e:
				print("\n *** model files not available *** ")
				logger.critical(f'*** Please check for model files in results folder *** \n{e}')
				
			try:
				# making consolidated dataframe
				x_combine = pd.DataFrame(columns = ['recreated_text','orig_sent_class'])

				# First, concatenating augmentation and counterfactual data
				# Reason: These two csvs have labels as strings
				x_combine = pd.concat([x_aug_combine.rename(columns={'augmentation':'recreated_text','orig_sent_class':'orig_sent_class'}),cf_data.rename(columns={'perturbed':'recreated_text','original_label':'orig_sent_class'})],ignore_index=True)
				# Now encodeing the labels to numericals 
				x_combine['orig_sent_class'] = x_combine['orig_sent_class'].replace(le_dict)
				# Next, concatenating the data from text_attacks csvs
				x_combine = pd.concat([x_combine, df_ta_combine.rename(columns={'textattack':'recreated_text','original_label':'orig_sent_class'})],ignore_index=True)



				if len(x_combine["recreated_text"]): # if data is not "EMPTY":

					#config parser for data diagnostic configs
					data_diagnostic_parser = ConfigParser(interpolation=ExtendedInterpolation())
					data_diagnostic_parser.read('./Configs/config_data_diagnostic.properties')

					# preprocessing for whole generated data
					x_combine["recreated_text"] = preprocessing.clean_text_data(x_combine["recreated_text"], data_diagnostic_parser)

					"""
					Note: Data points used to retrain the model is combination of original training data points and corner case images
					Number of samples given for corner case generation : 642
					Total number of misclassified images generated (Attack Vectors + Metamorphic Testing + DeepXplore): 2364
					Percentage of corner case generation : 368.224 %
					"""
					try:
						#reading cleaned data of train, validation and test
						cleaned_df_path = data_diagnostic_parser.get("save_path","cleaned_dataframe_save_path")
						test_df = pd.read_csv(cleaned_df_path + '/cleaned_dataframe_test_set.csv')
						train_df = pd.read_csv(cleaned_df_path + '/cleaned_dataframe_training_set.csv')
						val_df = pd.read_csv(cleaned_df_path + '/cleaned_dataframe_validation_set.csv')
					except Exception as e:
						print("\n *** Issue with clean data path *** ")
						logger.critical(f'*** Please check for cleaned dataframe in results folder *** \n{e}')
						PassError(e)					

					#concatinating validation and train dataframe and using it as train dataset
					train_df = pd.concat([train_df,val_df])
					train_df = train_df.drop(columns=["Unnamed: 0"])
					test_df = test_df.drop(columns=["Unnamed: 0"])
					train_df = train_df.dropna()

					#predicting and evaluating train dataframe
					train_vec = vec.transform(train_df['Text'])
					pred_train = model.predict(train_vec)
					pred_test = model.predict(vec.transform(test_df['Text']))
					pred_aug = model.predict(vec.transform(x_combine['recreated_text']))
					print('Classification Report for generated data before retraining:\n', classification_report(x_combine['orig_sent_class'].astype(int),pred_aug))				
					before_path = feedback_parser.get('save_path', 'feed_result_before_path')

					if not os.path.exists(before_path):
						os.makedirs(before_path)
					
					# generate .json file of classification_report
					report_dict = classification_report(train_df['Target'],pred_train,output_dict=True)
					del report_dict['accuracy']
					json_result = json.dumps(json.loads(json.dumps(report_dict), parse_float=lambda x: round(float(x), 2)))
					with open(before_path + "/train_before_report.json", "w") as f_result:
						f_result.write(json_result)		

					# generate .json file of classification_report
					report_dict = classification_report(test_df['Target'],pred_test,output_dict=True)
					del report_dict['accuracy']
					json_result = json.dumps(json.loads(json.dumps(report_dict), parse_float=lambda x: round(float(x), 2)))
					with open(before_path + "/test_before_report.json", "w") as f_result:
						f_result.write(json_result)

					report_dict = classification_report(x_combine['orig_sent_class'].astype(int),pred_aug,output_dict=True)
					del report_dict['accuracy']
					json_result = json.dumps(json.loads(json.dumps(report_dict), parse_float=lambda x: round(float(x), 2)))
					with open(before_path + "/aug_before_report.json", "w") as f_result:
						f_result.write(json_result)

					#concatinating train and augmented dataframe which will be further used for re-training the model
					total_train_text = pd.concat([train_df['Text'],x_combine['recreated_text']])
					# total_train_text = total_train_text.tolist()
					total_train_target = pd.concat([train_df['Target'],x_combine['orig_sent_class']])
					# print(type(total_train_target),"TOTAL TARGET")

					# MODEL RETRAINING
					t_vec = vec.fit(total_train_text)
					total_train_vec = t_vec.transform(total_train_text)
					model.fit(total_train_vec,total_train_target.astype(int))

					#predicting train and test dataframe using retrained model
					pred_total = model.predict(t_vec.transform(total_train_text))
					pred_train = model.predict(t_vec.transform(train_df['Text']))
					pred_test = model.predict(t_vec.transform(test_df['Text']))
					pred_aug = model.predict(t_vec.transform(x_combine['recreated_text']))
					# print(type(pred_aug[0]))

					dict1 = {}
					if feedback_parser.getboolean('Augmentation', 'set_execution') & feedback_parser.getboolean('Counterfactuals', 'set_execution') & feedback_parser.getboolean('Text_Attacks', 'set_execution'):
						dict1["number of samples generated (Augmentation + Counterfactuals + TextAttacks)"] = len(x_combine)
						dict1["number of train samples (train + val + augmentation + counterfactuals + TextAttacks)"] = len(total_train_text)
					
					elif feedback_parser.getboolean('Augmentation', 'set_execution') & feedback_parser.getboolean('Counterfactuals', 'set_execution'):
						dict1["number of samples generated (Augmentation + Counterfactuals)"] = len(x_combine)
						dict1["number of train samples (train + val + augmentation + counterfactuals)"] = len(total_train_text)
					
					elif feedback_parser.getboolean('Augmentation', 'set_execution') & feedback_parser.getboolean('Text_Attacks', 'set_execution'):
						dict1["number of samples generated (Augmentation + TextAttacks)"] = len(x_combine)
						dict1["number of train samples (train + val + augmentation + TextAttacks)"] = len(total_train_text)
					
					elif feedback_parser.getboolean('Counterfactuals', 'set_execution') & feedback_parser.getboolean('Text_Attacks', 'set_execution'):
						dict1["number of samples generated (Counterfactuals + TextAttacks)"] = len(x_combine)
						dict1["number of train samples (train + val + counterfactuals + TextAttacks)"] = len(total_train_text)	
					
					elif feedback_parser.getboolean('Augmentation', 'set_execution'):
						dict1["number of samples generated (Augmentation)"] = len(x_combine)
						dict1["number of train samples (train + val + Augmentation)"] = len(total_train_text)	

					elif feedback_parser.getboolean('Counterfactuals', 'set_execution'):
						dict1["number of samples generated (Counterfactuals)"] = len(x_combine)
						dict1["number of train samples (train + val + Counterfactuals)"] = len(total_train_text)	

					elif feedback_parser.getboolean('Text_Attacks', 'set_execution'):
						dict1["number of samples generated (TextAttacks)"] = len(x_combine)
						dict1["number of train samples (train + val + TextAttacks)"] = len(total_train_text)
					
					# dict1["number of samples generated (Augmentation + Counterfactuals)"] = len(x_combine)
					# dict1["number of train samples (train + val + augmentation + counterfactuals + textattacks)"] = len(total_train_text)
					
					dict1["number of test samples"] = len(test_df)
					
					with open(result_path + "/num_samples.json", "w") as outfile:
						json.dump(dict1, outfile)

					# pred_test = model.predict(vec.transform(test_df['Text']))
					# print('Classification Report for original train data after retraining:\n', classification_report(train_df['Target'],pred_train))
					# print('Classification Report for original test data after retraining:\n', classification_report(test_df['Target'],pred_test))
					print('Classification Report for generated data after retraining:\n', classification_report(x_combine['orig_sent_class'].astype(int),pred_aug))
					
					after_path = feedback_parser.get('save_path', 'feed_result_after_path')

					if not os.path.exists(after_path):
						os.makedirs(after_path)

					# generate .json file of classification_report
					report_dict = classification_report(train_df['Target'],pred_train,output_dict=True)
					del report_dict['accuracy']
					json_result = json.dumps(json.loads(json.dumps(report_dict), parse_float=lambda x: round(float(x), 2)))
					with open(after_path + "/train_after_report.json", "w") as f_result:
						f_result.write(json_result)
					# class_report = pd.DataFrame.from_dict(report_dict)
					# class_report.to_json(result_path+'/train_classification_report_after_retraining.json', index = True)

					# generate .json file of classification_report
					report_dict = classification_report(test_df['Target'],pred_test,output_dict=True)
					del report_dict['accuracy']
					json_result = json.dumps(json.loads(json.dumps(report_dict), parse_float=lambda x: round(float(x), 2)))
					with open(after_path + "/test_after_report.json", "w") as f_result:
						f_result.write(json_result)

					# generate .json file of classification_report
					report_dict = classification_report(x_combine['orig_sent_class'].astype(int), pred_aug,output_dict=True)
					# print(report_dict, "DICTIONARY")
					del report_dict['accuracy']
					json_result = json.dumps(json.loads(json.dumps(report_dict), parse_float=lambda x: round(float(x), 2)))
					with open(after_path + "/aug_after_report.json", "w") as f_result:
						f_result.write(json_result)
					print(f"{color.OK}\n *** Feedback Loop executed successfully !!!***{color.RESET}")
					logger.info("Feedback Loop successfully executed.")
			except Exception as e:
				# print("\n *** model files not available *** ")
				# logger.critical(f'*** Please check for model files in results folder *** \n{e}')
				print(f"{color.FAIL}\n *** Feedback Loop failed ***{color.RESET}")
				logger.critical('Please check for any issues in the Feedback Loop!!')
				logger.exception('Feedback Loop did not execute successfully!!')
				PassError(e)


		return None
	logger = ut.logger_setup('Feedbackloop', 'Feedbackloop.log')
	retrain(logger)

else:
	print("Invalid License")