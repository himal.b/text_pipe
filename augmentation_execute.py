import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    from Augmentation.custom_aug import nlp_augmentation
    from configparser import ConfigParser, ExtendedInterpolation
    from datetime import datetime
    import pandas as pd
    import numpy as np
    import os
    import nlpaug.augmenter.char as nac
    import nlpaug.augmenter.word as naw
    import nlpaug.augmenter.sentence as nas
    import nlpaug.flow as nafc
    from nlpaug.util import Action
    from Utility import utility

    color= utility.bcolors

    # Setting the parser 
    parser = ConfigParser(interpolation=ExtendedInterpolation())
    parser.read('./Configs/config_augmentation.properties')



    start_time = datetime.now()

    class PassError(Exception):
        """Base class for exceptions in this module."""
        pass


    def execute_sentence(df_sentence,df_text,df_target):
        ## sentence
        # df_sentence = pd.DataFrame(data=None,columns=["input_text","Contextual_insert_xlnet","Contextual_insert_gpt2","Contextual_insert_distilgpt2"])
        for original_text,target in zip(df_text,df_target):
            # print(original_text,target)

            nlp_class = nlp_augmentation(original_text)
            

            try:
                Contextual_insert_xlnet = nlp_class.ContextualSentence(nas.ContextualWordEmbsForSentenceAug,model_path='xlnet-base-cased')
            except Exception as e:
                Contextual_insert_xlnet = original_text
            try:
                Contextual_insert_gpt2 = nlp_class.ContextualSentence(nas.ContextualWordEmbsForSentenceAug,model_path='gpt2')
            except Exception as e:
                Contextual_insert_gpt2 = original_text
            try:
                Contextual_insert_distilgpt2 = nlp_class.ContextualSentence(nas.ContextualWordEmbsForSentenceAug,model_path='distilgpt2')
            except:
                Contextual_insert_distilgpt2 = original_text

            df_sentence.loc[len(df_sentence.index)]=[original_text,target,Contextual_insert_xlnet,Contextual_insert_gpt2,Contextual_insert_distilgpt2]

        return df_sentence


    def execute_word(df_word,df_text,df_target):
        ### Word
        # df_word = pd.DataFrame(data=None,columns=["input_text","Spelling_substitute","TfidfAug_insert","TfidfAug_substitute",
        #                                      "RandomWordAug_crop","RandomWordAug_substitute",
        #                                      "RandomWordAug_swap","RandomWordAug_delete","SplitAug_split",'SynonymAug_wordnet_ppdb','SynonymAug_wordnet',
        #                                      'AntonymAug_wordnet','Reserved_Aug'])
        reserved_words = [['FW', 'Fwd', 'F/W', 'Forward'],]

        for original_text,target in zip(df_text,df_target):
            # print(original_text,target)
            

            nlp_class = nlp_augmentation(original_text)
            
            try:

                SpellingAug_substitute = nlp_class.nlp_word(naw.SpellingAug,action='')
            except Exception as e:

                SpellingAug_substitute = original_text
            try:
                TfidfAug_insert = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='insert')

            except Exception as e: 
                TfidfAug_insert = original_text

            try:
                TfidfAug_substitute = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='substitute')
            except Exception as e:
                TfidfAug_substitute = original_text

            try:
                TfidfAug_swap = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='swap')
            except Exception as e: 
                TfidfAug_swap = original_text

            try:
                TfidfAug_delete = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='delete')
            except Exception as e: 
                TfidfAug_delete = original_text 

            try:
                RandomWordAug_crop = nlp_class.nlp_word(naw.RandomWordAug,action='crop')
            except Exception as e: 
                RandomWordAug_crop = original_text

            try:
                RandomWordAug_substitute = nlp_class.nlp_word(naw.RandomWordAug,action='substitute')
            except Exception as e: 
                RandomWordAug_substitute = original_text


            try:
                RandomWordAug_swap = nlp_class.nlp_word(naw.RandomWordAug,action='swap')
            except Exception as e: 
                 RandomWordAug_swap = original_text   

            try:
                RandomWordAug_delete = nlp_class.nlp_word(naw.RandomWordAug,action='delete')
            except Exception as e: 
                RandomWordAug_delete = original_text

            try:
                SplitWordAug_split = nlp_class.nlp_word(naw.SplitAug,action='split')
            except Exception as e: 
                SplitWordAug_split = original_text

            try:
                SynonymAug_wordnet_ppdb = nlp_class.synonym_antonym(naw.SynonymAug,aug_src='wordnet',model_path='./Modeling/ppdb-2.0-tldr')
            except Exception as e: 
                SynonymAug_wordnet_ppdb = original_text

            try:
                SynonymAug_wordnet = nlp_class.synonym_antonym(naw.SynonymAug,aug_src='wordnet',model_path='')

            except Exception as e: 
                SynonymAug_wordnet = original_text

            try:
                AntonymAug_wordnet = nlp_class.synonym_antonym(naw.AntonymAug,aug_src='wordnet',model_path='')
            except Exception as e: 
                AntonymAug_wordnet = original_text

            try:
                Reserved_Aug = nlp_class.ResevWord(naw.ReservedAug,reserved_words)
            except Exception as e: 
                Reserved_Aug = original_text


            df_word.loc[len(df_word.index)]=[original_text,target,SpellingAug_substitute,TfidfAug_insert,TfidfAug_substitute,
                                   RandomWordAug_crop,RandomWordAug_substitute,
                                   RandomWordAug_swap,RandomWordAug_delete,SplitWordAug_split,SynonymAug_wordnet_ppdb,SynonymAug_wordnet,
                                   AntonymAug_wordnet,Reserved_Aug]
        
        return df_word

    def execute_character(df_char,df_text,df_target):
        ## character
        # df_char = pd.DataFrame(data=None,columns=["input_text","OcrAug_insert","OcrAug_substitute","OcrAug_swap","OcrAug_delete","KeyboardAug_insert","KeyboardAug_substitute","KeyboardAug_swap","KeyboardAug_delete","RandomCharAug_insert","RandomCharAug_substitute","RandomCharAug_swap","RandomCharAug_delete"])
        for original_text,target in zip(df_text,df_target):
            nlp_class = nlp_augmentation(original_text)

            OcrAug_insert = nlp_class.nlp_char(nac.OcrAug,action='insert')
            OcrAug_substitute = nlp_class.nlp_char(nac.OcrAug,action='substitute')
            OcrAug_swap = nlp_class.nlp_char(nac.OcrAug,action='swap')
            OcrAug_delete = nlp_class.nlp_char(nac.OcrAug,action='delete')
            KeyboardAug_insert = nlp_class.nlp_char(nac.OcrAug,action='insert')
            KeyboardAug_substitute = nlp_class.nlp_char(nac.OcrAug,action='substitute')
            KeyboardAug_swap = nlp_class.nlp_char(nac.OcrAug,action='swap')
            KeyboardAug_delete = nlp_class.nlp_char(nac.OcrAug,action='delete')
            RandomCharAug_insert = nlp_class.nlp_char(nac.RandomCharAug,action='insert')
            RandomCharAug_substitute = nlp_class.nlp_char(nac.RandomCharAug,action='substitute')
            RandomCharAug_swap = nlp_class.nlp_char(nac.RandomCharAug,action='swap')
            RandomCharAug_delete = nlp_class.nlp_char(nac.RandomCharAug,action='delete')

            df_char.loc[len(df_char.index)]=[original_text,target,OcrAug_insert,OcrAug_substitute,OcrAug_swap,OcrAug_delete,KeyboardAug_insert,KeyboardAug_substitute,KeyboardAug_swap,KeyboardAug_delete,RandomCharAug_insert,RandomCharAug_substitute,RandomCharAug_swap,RandomCharAug_delete]
        
        return df_char

    def augment_main():
        # Loading the dataset name form the config file 
        data_path = parser.get('dataset','dataset_path')
        input_col = parser.get('dataset','input_column')
        target_col = parser.get('dataset','target_column')
        dataset_path = os.path.join(data_path, os.listdir(data_path)[0])

        data = pd.read_csv(dataset_path)
        end_samples = parser.getint('dataset','end_samples')
        start_samples = parser.getint('dataset','start_samples')
        df_text = data[input_col][start_samples:end_samples]
        df_target = data[target_col][start_samples:end_samples]

        logger = utility.logger_setup('TextAugmentation', 'TextAugmentation.log')
        if parser.getboolean('augment_type', 'parameters.sentences'):
            try:
                sent_csv_path = parser.get('save_path', 'aug_results_sent')
                if not os.path.exists(sent_csv_path):
                    os.makedirs(sent_csv_path)
                sentence_df = pd.DataFrame(data=None,columns=["input_text","target","Contextual_insert_xlnet","Contextual_insert_gpt2","Contextual_insert_distilgpt2"])
                print(f"{color.OK} *** Sentence augmentation initiated  *** {color.RESET}")
                df_sentence = execute_sentence(sentence_df,df_text,df_target)
                try:
                    df_sentence.to_csv(sent_csv_path+'/'+'augmented_sentence.csv')
                    # df_sentence.to_csv(sent_csv_path+'/'+'augmented_sentence_1.csv')

                    print(f"{color.OK} *** Sentence augmentation ran successfully  *** {color.RESET}")
                    logger.info("Sentence augmentation ran successfully")
                except Exception as e:
                    logger.critical(f'\n *** The file is opened!!! Please close the augment sentence excel file ***\n{e}')
                    PassError(e)
            except Exception as e:
                print(e)
                print(f"{color.FAIL} *** Sentence augmentation Failed to execute *** {color.RESET}")
                logger.critical(f'\n *** sentence save path is not defined in augmentation config file!! ***\n{e}')
                logger.info("Sentence augmentation Failed to execute")
                PassError(e)

        if parser.getboolean('augment_type', 'parameters.words'):
            try:
                word_csv_path = parser.get('save_path', 'aug_results_word')
                if not os.path.exists(word_csv_path):
                    os.makedirs(word_csv_path)
                word_df = pd.DataFrame(data=None,columns=["input_text","target","Spelling_substitute","TfidfAug_insert","TfidfAug_substitute",
                                             "RandomWordAug_crop","RandomWordAug_substitute",
                                             "RandomWordAug_swap","RandomWordAug_delete","SplitAug_split",'SynonymAug_wordnet_ppdb','SynonymAug_wordnet',
                                             'AntonymAug_wordnet','Reserved_Aug'])

                print(f"{color.OK} *** Word augmentation initiated  *** {color.RESET}")
                df_word = execute_word(word_df,df_text,df_target)
                try:

                    df_word.to_csv(word_csv_path+'/'+'augmented_word.csv')
                    

                    print(f"{color.OK} *** Word augmentation ran successfully  *** {color.RESET}")
                    logger.info("Word augmentation ran successfully")
                except Exception as e:
                    logger.critical(f'\n *** The file is opened!!! Please close the augment word excel file ***\n{e}')
                    PassError(e)
            except Exception as e:
                print(e)
                print(f"{color.FAIL} Word augmentation Failed to execute {color.RESET}")
                logger.critical(f'\n *** word save path is not defined in augmentation config file!! ***\n{e}')
                logger.info("Word augmentation Failed to execute")
                PassError(e)

        if parser.getboolean('augment_type', 'parameters.characters'):
            try:
                char_csv_path = parser.get('save_path', 'aug_results_char')
                if not os.path.exists(char_csv_path):
                    os.makedirs(char_csv_path)
                char_df = pd.DataFrame(data=None,columns=["input_text","target","OcrAug_insert","OcrAug_substitute","OcrAug_swap","OcrAug_delete","KeyboardAug_insert","KeyboardAug_substitute","KeyboardAug_swap","KeyboardAug_delete","RandomCharAug_insert","RandomCharAug_substitute","RandomCharAug_swap","RandomCharAug_delete"])

                print(f"{color.OK} *** Character augmentation initiated  *** {color.RESET}")
                df_char = execute_character(char_df,df_text,df_target)

                try:
                    df_char.to_csv(char_csv_path+'/'+'augmented_char.csv')
                    print(f"{color.OK} ***  Character augmentation ran successfully  *** {color.RESET}")
                    logger.info("Character augmentation ran successfully")
                except Exception as e:
                    logger.critical(f'\n *** The file is opened!!! Please close the augment character excel file ***\n{e}')
                    PassError(e)
            except Exception as e:
                print(e)
                print(f"{color.FAIL}\n Character augmentation Failed to execute {color.RESET}")
                logger.critical(f'\n *** character save path is not defined in augmentation config file!! ***\n{e}')
                logger.info("Character augmentation Failed to execute")
                PassError(e)

    # augment_main()

    # end_time = datetime.now()

    # print(f"\033[1;32m execution time {end_time-start_time}  \n")
else:
    print("Invalid License")