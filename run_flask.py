import app_text
import sys 
try:
    if __name__ == "__main__":
        server = app_text.main_script()
        server.run(debug=False,port=8000)
        print("Performance Testing Executed Successfully")
    sys.exit(0)
except SystemExit:
    print("Program terminated with sys.exit()")
except:
    print("Performance Testing did not execute successfully")
    sys.exit(0)
finally:
    print("Done")