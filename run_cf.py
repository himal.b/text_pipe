import sys 
import counterfactuals_execute as cfe

try:
    if __name__ == "__main__":
        cfe.cf_main()
        print("Counterfactuals Executed Successfully")
    sys.exit(0)
except SystemExit:
    print("Program terminated with sys.exit()")
except:
    print("Counterfactuals did not execute successfully")
    sys.exit(0)
finally:
    print("Done")