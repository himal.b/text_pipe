import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import logging
    import os
    import pickle
    import sys
    from configparser import ConfigParser, ExtendedInterpolation
    # import mlflow
    from PerformanceTesting import test as pt
    import time
    from Utility import utility as ut


    # CONFIG Parser for Performance Testing
    parser = ConfigParser(interpolation=ExtendedInterpolation())
    parser.read('./Configs/config_performance_testing.properties')

    # mlflow config
    # experiment_name = parser.get('mlflow', 'experiment_name')
    # run_number = parser.getint('mlflow', 'run_number')


    def execute_performance_testing():
        # creating results folder if not exist
        # os.makedirs("Results", exist_ok=True)

        # setting up MLflow experiment
        # try:
        #     mlflow.create_experiment(experiment_name)
        # except:
        #     pass
        # mlflow.set_tracking_uri("sqlite:///mlruns.db")
        # mlflow.set_experiment(experiment_name)

        # # logging saved dataset as artifact
        # with mlflow.start_run(run_name=experiment_name) as run:
        #     exp_id = run.info.experiment_id
        #     run_id = run.info.run_uuid

            #stress test execution
        if parser.get('stress_test_parameter', 'stress_test_execute').lower()=='true':

            a = parser['stress_test_parameter']['locustfile']
            b = parser['stress_test_parameter']['host']
            c = parser['stress_test_parameter']['num_users']
            d = parser['stress_test_parameter']['spawn_rate']
            e = parser['stress_test_parameter']['run_time']

            json_result, json_failures = pt.stress_test(a,b,c,d,e)
            folder = parser['stress_test_parameter']['folder']
            if not os.path.exists(folder):
                os.makedirs(folder)
            # df.to_json( path_or_buf = parser['stress_test_parameter']['stat_path'], orient="records")
            # df_failures.to_json( path_or_buf = parser['stress_test_parameter']['stat_failure_path'], orient="records")
            with open(parser['stress_test_parameter']['stat_path'], "w") as stress_result:
                stress_result.write(json_result)

            with open(parser['stress_test_parameter']['stat_failure_path'], "w") as stress_failure:
                stress_failure.write(json_failures)
            # df( path_or_buf = parser['stress_test_parameter']['stat_path'])
            # df_failures ( path_or_buf = parser['stress_test_parameter']['stat_failure_path'])
            # time.sleep(3)
        if parser.get('soak_test_parameter', 'soak_test_execute').lower()=='true':

            f = parser['soak_test_parameter']['locustfile']
            g = parser['soak_test_parameter']['host']
            h = parser['soak_test_parameter']['num_users']
            i = parser['soak_test_parameter']['spawn_rate']
            j = parser['soak_test_parameter']['run_time']

            json_result, json_failures = pt.soak_test(f,g,h,i,j)
            # df, df_failures = pt.soak_test(f,g,h,i,j)
            folder = parser['soak_test_parameter']['folder']
            if not os.path.exists(folder):
                os.makedirs(folder)

            with open(parser['soak_test_parameter']['stat_path'], "w") as soak_result:
                soak_result.write(json_result)

            with open(parser['soak_test_parameter']['stat_failure_path'], "w") as soak_failure:
                soak_failure.write(json_failures)
            # df.to_json( path_or_buf = parser['soak_test_parameter']['stat_path'], orient="records")
            # df_failures.to_json( path_or_buf = parser['soak_test_parameter']['stat_failure_path'] , orient="records")

    def main_script():
        # logging directory
        # os.makedirs('./logging', exist_ok=True)

        # logger = logging.getLogger(__name__)
        # logger.setLevel(logging.DEBUG)
        # formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s:%(filename)s:%(module)s')
        # file_handler = logging.FileHandler('./logging/performance_testing.log')
        # file_handler.setFormatter(formatter)

        # logger.addHandler(file_handler)

        try:
            logger = ut.logger_setup('PerformanceTesting', 'Performance_Testing.log')

            execute_performance_testing()
            logger.info("PerformanceTesting successfully executed.")
        except:
            logger.critical('Please fix PerformanceTesting')
            logger.exception('PerformanceTesting error!!')
            sys.exit(1)


    # if __name__ == "__main__":
    #     main_script()


    # pt.stress_test()
    # pt.soak_test()
else:
    print("Invalid License")