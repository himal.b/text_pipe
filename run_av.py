import sys 
import attack_vectors_execute as ave

try:
    if __name__ == "__main__":
        ave.Textattack_execute()
        print("Text Attacks Executed Successfully")
    sys.exit(0)
except SystemExit:
    print("Program terminated with sys.exit()")
except:
    print("Text Attacks did not execute successfully")
    sys.exit(0)
finally:
    print("Done")