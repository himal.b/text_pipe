import os
import pandas as pd
import numpy as np
# os.environ["MODEL_DIR"] = ''
import nlpaug.augmenter.char as nac
import nlpaug.augmenter.word as naw
import nlpaug.augmenter.sentence as nas
import nlpaug.flow as nafc

from nlpaug.util import Action



class nlp_augmentation:

    def __init__(self,text):
        self.text = text

    # Character
    def nlp_char(self,augmenter,action):
        '''Use OcrAug, KeyboardAug RandomCharAug'''
#         for text_all in input_text[:100]:

        aug = augmenter
        aug1 = aug(action)
        augmented_text = aug1.augment(self.text)
        # _sentence(augmented_text)
        return augmented_text

    # Word augmenter
    def nlp_word(self,augmenter,action):
        '''Use SpellingAug, RandomWordAug, SplitAug,'''
#         for text_all in input_text[:100]:

        aug = augmenter
        aug1 = aug(action)
        augmented_text = aug1.augment(self.text)
        return augmented_text

    def Tfidf(self,augmenter,model_path,action):
        '''use TfidfAug action = insert,substitute,delete'''

        aug = augmenter
        aug1 = aug(model_path,action)
        augmented_text = aug1.augment(self.text)
        return augmented_text

    def synonym_antonym(self,augmenter,aug_src,model_path):

        '''Use SynonymAug and AntonymAug'''
        #         for text_all in input_text[:100]:

        #         aug = augmenter
        aug = augmenter
        aug1 = aug(aug_src,model_path)
        augmented_text = aug1.augment(self.text)
        return augmented_text

    def ResevWord(self,augmenter,reserved_tokens):


        aug1 = augmenter(reserved_tokens)
        augmented_text = aug1.augment(self.text)
        return augmented_text

    # Sentence augmenter
    def ContextualSentence(self,augmenter,model_path):
        '''Choose from model path = xlnet-base-cased,gpt2,'distilgpt2 for contextualwords embs.
        '''
        aug1 = augmenter(model_path)
        augmented_text = aug1.augment(self.text)
        return augmented_text


    def show(self):
        print('nlp_char Uses ---> OcrAug, KeyboardAug RandomCharAug')


# ## sentence
# df_sentence = pd.DataFrame(data=None,columns=["original","Contextual_insert_xlnet","Contextual_insert_gpt2","Contextual_insert_distilgpt2"])
# for i in df_review[:2]:

#     nlp_class = nlp_augmentation(i,[['FW', 'Fwd', 'F/W', 'Forward'],])
#     original_text = i
#     Contextual_insert_xlnet = nlp_class.ContextualSentence(nas.ContextualWordEmbsForSentenceAug,model_path='xlnet-base-cased')
#     Contextual_insert_gpt2 = nlp_class.ContextualSentence(nas.ContextualWordEmbsForSentenceAug,model_path='gpt2')
#     Contextual_insert_distilgpt2 = nlp_class.ContextualSentence(nas.ContextualWordEmbsForSentenceAug,model_path='distilgpt2')


#     df_sentence.loc[len(df_sentence.index)]=[original_text,Contextual_insert_xlnet,Contextual_insert_gpt2,Contextual_insert_distilgpt2]
# #     print('oooooooo',df_aa)

# #     original_list.append(original_text)
# #     original_list.append(changed_text)
#     # print('##########',df)

# # print('*****',original_list)
# df_sentence.to_csv('augmented_sentence.csv')

# print("\033[1;36;40m Sentence augmentation ran successfully  \n")




# ### Word
# df_word = pd.DataFrame(data=None,columns=["original","Spelling_substitute","TfidfAug_insert","TfidfAug_substitute",
#                                      "RandomWordAug_crop","RandomWordAug_substitute",
#                                      "RandomWordAug_swap","RandomWordAug_delete","SplitAug_split",'SynonymAug_wordnet_ppdb','SynonymAug_wordnet',
#                                      'AntonymAug_wordnet','Reserved_Aug'])
# for i in df_review[:2]:

#     nlp_class = nlp_augmentation(i,[['FW', 'Fwd', 'F/W', 'Forward'],])
#     original_text = i
#     SpellingAug_substitute = nlp_class.nlp_char(naw.SpellingAug,action='')
#     TfidfAug_insert = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='insert')
#     TfidfAug_substitute = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='substitute')
#     # TfidfAug_swap = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='swap')
#     # TfidfAug_delete = nlp_class.Tfidf(naw.TfIdfAug,model_path='',action='delete')
#     RandomWordAug_crop = nlp_class.nlp_word(naw.RandomWordAug,action='crop')
#     RandomWordAug_substitute = nlp_class.nlp_word(naw.RandomWordAug,action='substitute')
#     RandomWordAug_swap = nlp_class.nlp_word(naw.RandomWordAug,action='swap')
#     RandomWordAug_delete = nlp_class.nlp_word(naw.RandomWordAug,action='delete')
#     SplitWordAug_split = nlp_class.nlp_word(naw.SplitAug,action='split')
#     SynonymAug_wordnet_ppdb = nlp_class.synonym_antonym(naw.SynonymAug,aug_src='wordnet',model_path='./Modeling/ppdb-2.0-tldr')
#     SynonymAug_wordnet = nlp_class.synonym_antonym(naw.SynonymAug,aug_src='wordnet',model_path='')
#     AntonymAug_wordnet = nlp_class.synonym_antonym(naw.AntonymAug,aug_src='wordnet',model_path='')
#     Reserved_Aug = nlp_class.ResevWord(naw.ReservedAug)




#     df_word.loc[len(df_word.index)]=[original_text,SpellingAug_substitute,TfidfAug_insert,TfidfAug_substitute,
#                            RandomWordAug_crop,RandomWordAug_substitute,
#                            RandomWordAug_swap,RandomWordAug_delete,SplitWordAug_split,SynonymAug_wordnet_ppdb,SynonymAug_wordnet,
#                            AntonymAug_wordnet,Reserved_Aug]

# #     print('oooooooo',df_aa)

# #     original_list.append(original_text)
# #     original_list.append(changed_text)
#     # print('##########',df)

# # print('*****',original_list)
# df_word.to_csv('augmented_word.csv')

# print("\033[1;33;40m Word augmentation ran successfully  \n")


# ## character
# df_char = pd.DataFrame(data=None,columns=["original","OcrAug_insert","OcrAug_substitute","OcrAug_swap","OcrAug_delete","KeyboardAug_insert","KeyboardAug_substitute","KeyboardAug_swap","KeyboardAug_delete","RandomCharAug_insert","RandomCharAug_substitute","RandomCharAug_swap","RandomCharAug_delete"])
# for i in df_review[:2]:

#     nlp_class = nlp_augmentation(i,[['FW', 'Fwd', 'F/W', 'Forward'],])
#     original_text = i
#     OcrAug_insert = nlp_class.nlp_char(nac.OcrAug,action='insert')
#     OcrAug_substitute = nlp_class.nlp_char(nac.OcrAug,action='substitute')
#     OcrAug_swap = nlp_class.nlp_char(nac.OcrAug,action='swap')
#     OcrAug_delete = nlp_class.nlp_char(nac.OcrAug,action='delete')
#     KeyboardAug_insert = nlp_class.nlp_char(nac.OcrAug,action='insert')
#     KeyboardAug_substitute = nlp_class.nlp_char(nac.OcrAug,action='substitute')
#     KeyboardAug_swap = nlp_class.nlp_char(nac.OcrAug,action='swap')
#     KeyboardAug_delete = nlp_class.nlp_char(nac.OcrAug,action='delete')
#     RandomCharAug_insert = nlp_class.nlp_char(nac.RandomCharAug,action='insert')
#     RandomCharAug_substitute = nlp_class.nlp_char(nac.RandomCharAug,action='substitute')
#     RandomCharAug_swap = nlp_class.nlp_char(nac.RandomCharAug,action='swap')
#     RandomCharAug_delete = nlp_class.nlp_char(nac.RandomCharAug,action='delete')



#     df_char.loc[len(df_char.index)]=[original_text,OcrAug_insert,OcrAug_substitute,OcrAug_swap,OcrAug_delete,KeyboardAug_insert,KeyboardAug_substitute,KeyboardAug_swap,KeyboardAug_delete,RandomCharAug_insert,RandomCharAug_substitute,RandomCharAug_swap,RandomCharAug_delete]

# #     print('oooooooo',df_aa)

# #     original_list.append(original_text)
# #     original_list.append(changed_text)
#     # print('##########',df)

# # print('*****',original_list)
# df_char.to_csv('augmented_char.csv')

# print("\033[1;31;40m Character augmentation ran successfully  \n")

# end_time = datetime.now()

# print(f"\033[1;32m execution time {end_time-start_time}  \n")