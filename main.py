import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import os
    import pickle
    import matplotlib.pyplot as plt
    from configparser import ConfigParser, ExtendedInterpolation
    from data_diagnostic_execute import DataDiagnostic, DataDiagnosticsError
    from modeling_execute import Modeling, ModelingError
    from model_explainability_execute import ModelExplainability, FileNotFound
    import deepxplore_implementation
    import augmentation_execute as ae
    import json_report_generation as jrg
    from Utility import utility as ut

    plt.style.use('dark_background')

    parser = ConfigParser(interpolation=ExtendedInterpolation())
    parser.read('./Configs/config_main.properties')

    color= ut.bcolors

    class PassError(Exception):
        """Base class for exceptions in this module."""
        pass

    def data_diagnos():
        """
        This is the 'main' function of DataDiagnostic stage. This function is executing
        load_and_preprocess function and embeddings_execute function.

        This function load and preprocess the data and then find the embedding vectors and save
        the embedding vectors along with cleaned data and make the dataset ready for model training.
        """

        # Logger setup function
        logger = ut.logger_setup('DataDiagnostic', 'DataDiagnostic.log')

        # Loading DataDiagnostic stage self.parser
        data_diagnostic_parser = ConfigParser(interpolation=ExtendedInterpolation())
        data_diagnostic_parser.read('./Configs/config_data_diagnostic.properties')  # Path of the config file

        # instance of DataDiagnostic
        if parser.getboolean('loading_data_diagnostic', 'set_execution'):

            try:
                print(f"{color.OK}*** Initiating DataDiagnostic Stage ***{color.RESET}")
                data_diagnostic = DataDiagnostic(data_diagnostic_parser)
                # load_and_preprocess function execution
                data_diagnostic.load_and_preprocess()
                logger.info("load_and_preprocess function successfully executed.")

            except Exception as e:
                print("load_and_preprocess error: ")
                print(f"{color.FAIL}\n *** Datadiagnostic  failed ***{color.RESET}")
                logger.critical(f'\n *** Please fix load_and_preprocess function ***\n{e}')
                PassError(e)
            # embeddings_execute function execution
            try:
                print(f"{color.OK}*** Initiating Word Embeddings ***{color.RESET}")
                data_diagnostic.embeddings_execute()
                ut.mlflow_logger(experiment_name = 'Preprocessing', artifacts = ['./Results/DataDiagnostic'])
                print('embeddings completed.')
                logger.info("embeddings_execute function successfully executed.")

            except Exception as e:
                print("embeddings_execute error: ")
                print(f"{color.FAIL}\n *** Word Embeddings failed ***{color.RESET}")
                logger.critical(f'\n *** Please fix embeddings_execute function ***\n{e}')
                PassError(e)
            
        return None

    def model_main():
        """
        This is the 'main' function of Modeling stage. This function is executing
        modeling_execute function.

        modeling_execute function loads the embedding vectors of CountVectoriser and TfidfVectoriser and
        then find the best model using either random search or grid search. And save the best
        fitted model and meta info of model.
        """

        # Logger setup function
        logger = ut.logger_setup('Modeling', 'Modeling.log')

        modeling_parser = ConfigParser(interpolation=ExtendedInterpolation())
        modeling_parser.read('./Configs/config_modeling.properties')  # Path of the config file
        # modeling_execute function execution
        if parser.getboolean('model_stage', 'set_execution'):
            try:
                print(f"{color.OK}*** Initiating Modeling stage ***{color.RESET}")
                modeling = Modeling(modeling_parser, logger)
                modeling.modeling_execute()
                ut.mlflow_logger(experiment_name = 'Modeling', artifacts = ['./Results/Modeling'])
                print("Modeling stage is successfully executed.")
                logger.info("modeling_execute function successfully executed.")

            except Exception as e:
                print(f"{color.FAIL}\n *** Modeling failed ***{color.RESET}")
                logger.critical(f'\n *** Please fix modeling_execute function ***\n{e}')
                PassError(e)
                
            # os.system('mlflow ui --backend-store-uri sqlite:///mlruns.db') # causing error - debug it
        return None

    def explainability():
        # Logger setup function
        logger = ut.logger_setup('ModelExplainability', 'ModelExplainability.log')

        # Loading DataDiagnostic stage Parser
        model_explainability_parser = ConfigParser(interpolation=ExtendedInterpolation())
        model_explainability_parser.read('./Configs/config_model_explainability.properties')

        # Load model_tune_dict
        if parser.getboolean('model_explainability', 'set_execution'):
            try:
                print(f"{color.OK}*** Initiating Model Explainability ***{color.RESET}")
                best_estimator_dict_path = os.path.join(model_explainability_parser.get('load_path', 'model_save_path'),
                                                        'best_estimator_dict.pkl')
                with open(best_estimator_dict_path, 'rb') as f:
                    best_estimator_dict = pickle.load(f)

            except Exception as e:
                print("\n *** best_estimator_dict was not loaded *** ")
                logger.critical(f' *** best_estimator_dict file was not loaded *** :\n {e}')
                PassError(e)

            # Load the trained model
            try:
                model_path = os.path.join(model_explainability_parser.get('load_path', 'model_save_path'), 'model.pkl')
                with open(model_path, 'rb') as f:
                    model = pickle.load(f)

            except Exception as e:
                print("\n *** Model pickle file not found *** ")
                logger.critical(f' *** Model pickle file not found *** :\n {e}')
                PassError(e)

            # Load the Label Encoder instance
            try:
                label_encoder_path = os.path.join(model_explainability_parser.get('load_path', 'meta_data_save_path'),
                                                  'label_encoder.pkl')
                with open(label_encoder_path, 'rb') as f:
                    label_encoder = pickle.load(f)
                    class_names = label_encoder.classes_

            except Exception as e:
                print("\n ***Label Encoder instance file not found *** ")
                logger.critical(f' *** Label Encoder instance file not found *** :\n {e}')
                PassError(e)

            # Model Explainability
            try:
                model_explainability = ModelExplainability(best_estimator_dict, model, class_names,
                                                           model_explainability_parser, logger)
                model_explainability.explainability_execute()
                ut.mlflow_logger(experiment_name = 'ModelExplainability', artifacts = ['./Results/ModelExplainability'])
                print(f"{color.OK}*** Model Explainability was completed.***{color.RESET}")
            except Exception as e:
                logger.critical(f'{color.FAIL}\n *** Model Explainability was not executed ***{color.RESET}:\n {e}')
                PassError(e)        
            
        return None

    def deepxplore():
        # Logger setup function
        logger = ut.logger_setup('DeepXplore', 'DeepXplore.log')
        try:
            print(f"{color.OK}*** Initiating DeepXplore stage ***{color.RESET}")
            deepxplore_implementation.execute_deepxplore()
            ut.mlflow_logger(experiment_name = 'DeepXplore', artifacts = ['./Results/DeepXplore'])
            logger.info("Deepxplore successfully executed.")

        except Exception as e:
            print(f"{color.FAIL}\n *** DeepXplore stage failed ***{color.RESET}")
            logger.critical('Please check data and Models files in Deepxplore pipeline')
            logger.exception('Deepxplore stage did not executed!!')
            PassError(e)
        return None

    def augmentation():
        # Logger setup function
        logger = ut.logger_setup('Augmentation', 'Augmentation.log')
        try:
            print(f"{color.OK}*** Initiating Augmentation Stage ***{color.RESET}")
            ae.augment_main()
            ut.mlflow_logger(experiment_name = 'Augmentation', artifacts = ['./Results/Augmentation'])

            print(f"{color.OK}***  Augmentation Stage successfully executed. ***{color.RESET}")
            logger.info("Augmentation stage successfully executed.")

        except Exception as e:
            print(f"{color.FAIL}\n *** Augmentation stage failed ***{color.RESET}")
            logger.critical('Please check all the files present in each folder in Augmentation stage')
            logger.exception('Augmentation stage did not execute successfully!!')
            PassError(e)
        return None

    def feedback():
        # Logger setup function
        logger = ut.logger_setup('Feedbackloop', 'Feedbackloop.log')

        # try:
        print(f"{color.OK}*** Initiating Feedback Loop ***{color.RESET}")
        fbl.retrain(logger)
        # ut.mlflow_logger(experiment_name = 'FeedbackLoop', artifacts = ['./Results/Feedbackloop'])
            # logger.info("Feedback Loop successfully executed.")
            # print(f"{color.OK}***Feedback Loop executed successfully***{color.RESET}")

        # except Exception as e:
        #     print(f"{color.FAIL}\n *** Feedback Loop failed ***{color.RESET}")
        #     logger.critical('Please check for any issues in the Feedback Loop!!')
        #     logger.exception('Feedback Loop did not execute successfully!!')
        #     PassError(e)
        return None

    def json_dump():
        # Logger setup function
        logger = ut.logger_setup('Json_dump', 'Json_dump.log')
        try:
            jrg.json_dump_generation()
            logger.info("json_dump_generation successfully executed.")

        except Exception as e:
            print("*** Json_dump error!! ***")
            logger.critical(f'\n *** Please fix json_report_generation ***:\n {e}')
            PassError(e)
        return None


    def main_script():
        if parser.getboolean('move_previous_results', 'set_execution'):
            ut.remove_previous_result()
        if parser.getboolean('loading_data_diagnostic', 'set_execution'):
            data_diagnos()
        if parser.getboolean('model_stage', 'set_execution'):
            model_main()
        if parser.getboolean('model_explainability', 'set_execution'):
            explainability()
        if parser.getboolean('DeepXplore', 'set_execution'):
            # deepxplore_implementation.main_script()
            deepxplore()
        if parser.getboolean('Augmentation', 'set_execution'):
            # deepxplore_implementation.main_script()
            augmentation()
        # if parser.getboolean('Performance_Testing', 'set_execution'):
        #     import performance_testing_execute as pte
        #     pte.main_script()
        if parser.getboolean('Feedbackloop', 'set_execution'):
            feedback()
        if parser.getboolean('Json_dump', 'set_execution'):
            json_dump()
    # main_script()

    # Execution
    # if __name__ == '__main__':
        # data_diagnos()
        # model_main()
        # explainability()

else:
    print("Invalid License")