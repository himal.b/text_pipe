# import license_execute as le
# license_status = le.license_status()
# if license_status == 'Valid@tAi':
from Counterfactuals.text_counterfactuals_custom import text_counterfactuals
import nltk
nltk.download('omw-1.4') # omw needed for polyjuice importing.

# load required modules from polyjuice
from polyjuice import Polyjuice
from polyjuice.generations import ALL_CTRL_CODES

# these libraries provide modules to deal with text data
# import transformers
# import munch
# import sentence_transformers
# import pattern
# import zss
import nltk
import ftfy
import re
from tqdm import tqdm
# to work on processed data
import spacy
import scipy

from configparser import ConfigParser, ExtendedInterpolation
from sklearn.preprocessing import LabelEncoder
from datetime import datetime
import pandas as pd
import numpy as np
import os
import logging
from Utility import utility 
# from IPython import get_ipython

# to load and save models
import pickle

color= utility.bcolors
# blue = "\033[1;34;40m"

# Setting the parser 
parser = ConfigParser(interpolation=ExtendedInterpolation())
parser.read('./Configs/config_counterfactuals.properties')



# to download the required spacy language module
# To load the spacy first we need to download the en_core_web_sm
#  using the python command in the terminal
# get_ipython().system(' python -m spacy download en_core_web_sm')

# load the downloaded spacy langauge module
spacy.load('en_core_web_sm')

start_time = datetime.now()

class PassError(Exception):
    """Base class for exceptions in this module."""
    pass

# make a function out of above
def get_perturbed_texts(original_text, perturb_idx, pj):
    
    # 0) takin the input sentence
    text_sentence = original_text[perturb_idx]
    # 1) ftfy - standard text
    text = ftfy.fix_text(text_sentence)
    # 2) strip <br /><br /> tags in some sentences
    text = text.replace('<br /><br />',' ')
    # 3) remove backslash-apostrophe 
    text = re.sub("\'", "", text)
    # 4) sentences from paragraph using nltk
    text = nltk.tokenize.sent_tokenize(text)
    # 5) remove everything except alphabets 
    text = pd.Series(text).apply(lambda txt: re.sub("[^a-zA-Z]"," ",txt))
    # 6) remove whitespaces 
    text = pd.Series(text).apply(lambda txt: ' '.join(txt.split()))
    # 7) convert text to lowercase 
    text = pd.Series(text).apply(lambda txt: txt.lower())
    # 8) after processing, apply perturbation from polyjuice
    perturbed_texts = pd.Series(text).apply(lambda txt: pj.perturb(txt))
    # perturbed_texts = pd.Series(text).apply(lambda txt: pj.perturb(txt, ctrl_code=ALL_CTRL_CODES, num_perturbations, perplex_thred))
    if len(text) > 1:
        # print('******************YES*****************')
        # 9) remake paragraph with perturbed sentences
        try:
            perturbed_paragraphs = _perturbed_paragraph(perturbed_texts)
        except:
            perturbed_paragraphs = np.array("") 
    else:
        # print("******************NO******************")
        perturbed_paragraphs = pd.Series(perturbed_texts[0])
    # print(perturbed_paragraphs,"PERTURB PARAGRAPH")
    return perturbed_paragraphs

# Function to merge back perturbed text to paragraph
def _perturbed_paragraph(perturbed_text):
    perturbed_paragraphs = []
    # for p in range(len(perturbed_text[0])):
    perturbed_para = []
    for q in range(len(perturbed_text)):
            # print(q,"Q",p,"P")
            # print(perturbed_text[q][p], )
        perturbed_para.append(perturbed_text[q][0])
        # perturbed_para.append(perturbed_text[q][p])
    perturbed_paragraph = " ".join(perturbed_para)
        # print(perturbed_paragraph, "PERTURBED_PARAGRAPHS")
    perturbed_paragraphs.append(perturbed_paragraph)
    return perturbed_paragraphs

def execute_polyjuice(original_text, orig_labels, label_encoder, perturb_idx, t_model, tfidf, pj):
    # generate perturbed texts
    # perturb using polyjuice perturb function
    # pj = Polyjuice(model_path="uw-hai/polyjuice", is_cuda=False)
    perturbations = get_perturbed_texts(original_text, perturb_idx, pj)
    perturbations = np.array(perturbations)
    # print(perturbations,"PERTURBATIONS 10044")
    # perturbations = pj.perturb(original_text[perturb_idx], ctrl_code=ALL_CTRL_CODES, num_perturbations=None, perplex_thred=3)    
    # if perturbations[0] != "":
    #     # cf_data = None
    cf_data = text_counterfactuals(original_text, t_model, tfidf, perturb_idx).cf_df(orig_labels=orig_labels, label_encoder=label_encoder, perturb_texts=perturbations)
    # else:
    #     # cf_data = text_counterfactuals(original_text, t_model, tfidf, perturb_idx).cf_df(orig_labels=orig_labels, label_encoder=label_encoder, perturb_texts=perturbations)
    #     cf_data = None
    return cf_data

# def execute_polyjuice(original_text, orig_labels, label_encoder, perturb_idx, t_model, tfidf, ctrl_code, num_perturbations, perplex_thred):
#     # generate perturbed texts
#     # perturb using polyjuice perturb function
#     pj = Polyjuice(model_path="uw-hai/polyjuice", is_cuda=False)
    
#     perturbations = pj.perturb(original_text[perturb_idx], ctrl_code=ALL_CTRL_CODES, num_perturbations=None, perplex_thred=3)    
#     cf_data = text_counterfactuals(original_text, t_model, tfidf, perturb_idx).cf_df(orig_labels=orig_labels, label_encoder=label_encoder, perturb_texts=perturbations)
#     return cf_data

def cf_main():
    # Loading the dataset name form the config file 
    # Getting the path of dataset
    dataset_path = parser.get('dataset', 'dataset_path')
    complete_dataset_path = os.path.join(dataset_path, os.listdir(dataset_path)[0])

    # dataset_path = parser.get('dataset','dataset_path')
    # complete_dataset_path = data_path + "/" + parser.get('dataset', 'dataset_name') + ".csv"
    data = pd.read_csv(complete_dataset_path,encoding='latin1')
    # complete_dataset_path.encode('utf-8')
    input_col = parser.get('dataset','input_column')
    target_col = parser.get('dataset','target_column')
    # samples = parser.getint('dataset','no_of_samples')
    
    # num_perturbations = parser.get('parameters','num_perturbations')
    # perplex_thred = parser.getint('parameters','perplex_thred')
    # perturb_idx_str = parser.get('parameters','perturb_idx')
    # perturb_idx_values = perturb_idx_str.split(",")
    start_idx = parser.getint('parameters','start_idx')
    end_idx = parser.getint('parameters','end_idx')
    # perurb_idx_range = [1,10]
    # perturb_idx_values = range(perurb_idx_range) 
    # print(type(perturb_idx))
    t_model_path = parser.get('save_path', 'model_path')
    tfidf_path = parser.get('save_path', 'vect_path')

    # Load the saved .pkl model using pickel module
    with open(t_model_path, 'rb') as handle:
        t_model = pickle.load(handle)

    # Also load the saved vectorizer
    tfidf = pickle.load(open(tfidf_path, "rb"))


    # ctrl_code = ALL_CTRL_CODES
    # ALL_CTRL_CODES = set([LEXCICAL, RESEMANTIC, NEGATION, INSERT, 
    # DELETE, QUANTIFIER, RESTRUCTURE, SHUFFLE])
    
    label_encoder = LabelEncoder()
    encoded_labels = label_encoder.fit_transform(data[target_col])
    # encoded_labels = label_encoder.transform(training_set_y)
    # print(data.head(10))
    original_text = data[input_col]
    # print(type(original_text))
    orig_labels = data[target_col]
    # encoded_labels = encoded_labels[:samples]

    sent_csv_path = parser.get('save_path', 'cf_results')
    if not os.path.exists(sent_csv_path):
        os.makedirs(sent_csv_path)
    cf_df = pd.DataFrame(data=None,columns=["original","original_label","original_pred","original_pred_proba","perturbed","perturb_label","perturb_pred_proba"])#,columns=["input_text","input_label","perturb_text","perturb_label"])
    # perturb_instances = text_counterfactuals.wrap_perturbed_instances(perturb_texts)
    # df = pd.DataFrame(perturb_instances)
    # print(perturb_instances)
    pj = Polyjuice(model_path="uw-hai/polyjuice", is_cuda=False)
    # logger = utility.logger_setup('TextCounterfactuals', 'TextCounterfactuals.log')
    # cf_dataframe = {}
    # for perturb_idx in tqdm(perturb_idx_values,  desc = 'Progress Bar'):
    for perturb_idx in tqdm(range(start_idx,end_idx),  desc = 'Progress Bar'):
        # perturb_idx = int(perturb_idx)
        try:
            df_cf = execute_polyjuice(original_text, orig_labels, label_encoder, perturb_idx, t_model, tfidf, pj)
        except:
            df_cf = pd.DataFrame(data=[original_text],columns = ['original'])

        # df_cf = execute_polyjuice(original_text, orig_labels, label_encoder, perturb_idx, t_model, tfidf, pj)
        # print(df_cf.columns,"dfcf")
        # print(cf_df.columns,"cfdf")
        cf_df = pd.concat([cf_df,df_cf])
        # print(df_cf)
    # print(cf_df)
        # cf_dataframe.append(cf_data)
        # print(perturb_idx)
        # print(cf_dataframe,"CF_DF")
    # cf_dataframe.to_csv(sent_csv_path + '/' + 'counterfactuals_' + str(perturb_idx) + '.csv')
    cf_df.to_csv(sent_csv_path + '/' + 'counterfactuals.csv')
    end_time = datetime.now()
    print(f"\033[1;32m execution time {end_time-start_time}  \n{color.RESET}")
    # for perturb_idx in perturb_idx_values:
    #     try:
    #         perturb_idx = int(perturb_idx)
    #         cf_dataframe = execute_polyjuice(original_text, orig_labels, label_encoder, perturb_idx, t_model, tfidf, ctrl_code, num_perturbations, perplex_thred)
    #         try:
    #             cf_dataframe.to_csv(sent_csv_path + '/' + 'counterfactuals_' + str(perturb_idx) + '.csv')
    #             print(f"{color.OK}\n text counterfactuals generated successfully  \n{color.RESET}")
    #             logger.info("text counterfactuals generated successfully")
    #         except Exception as e:
    #             logger.critical(f'\n *** The file is opened!!! Please close the excel file ***\n{e}')
    #             PassError(e)
    #     except Exception as e:
    #         print(f"{color.OK}\n text counterfactuals did not ran successfully {color.RESET}")
    #         logger.critical(f'\n *** save path is not defined in counterfactuals config file!! ***\n{e}')
    #         logger.info("text counterfactuals did not ran successfully")
    #         PassError(e)

cf_main()
# else:
#     print("Invalid License")

