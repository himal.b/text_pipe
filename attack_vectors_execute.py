import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import os
    import re
    import datasets
    import textattack
    import pandas as pd
    from AttackVectors import black_box_attacks as bba
    #from black_box_attacks import character_level,word_level,sentence_level
    from nltk import word_tokenize
    from textattack import Attacker
    from datasets import dataset_dict
    from sklearn import preprocessing
    from nltk.stem import PorterStemmer
    #import white_box_attacks
    from sklearn.metrics import classification_report
    from textattack.datasets import HuggingFaceDataset
    from sklearn.linear_model import LogisticRegression
    from textattack.models.wrappers import SklearnModelWrapper
    from configparser import ConfigParser, ExtendedInterpolation
    from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer, ENGLISH_STOP_WORDS
    import nltk
    nltk.download('omw-1.4')
    #from white_box_attacks import white_box
    #import white_box_attacks

    # Setting the parser 
    parser = ConfigParser(interpolation=ExtendedInterpolation())
    parser.read('./Configs/config_attack_vectors.properties')

    # Loading the parameters from the config file for attack_args
    cv_csv_path = parser.get('save_path', 'attack_results_cv_csv_path')
    tfidf_csv_path = parser.get('save_path', 'attack_results_tfidf_csv_path')

    # Loading the dataset name form the config file 
    dataset_name = parser.get('load_path','dataset_name')

    # Storing the test part of the dataset for attacking purpose
    dataset = HuggingFaceDataset(dataset_name,split="test")

    # Function for loading the data setting schema 
    def load_data(dataset_split='train'):

        # Loading the train part of the dataset from huggingface
        new_dataset = datasets.load_dataset(dataset_name)[dataset_split]
        # Setting the schema of the dataset
        schema = set(new_dataset.column_names)
        #poem_sentiment
        if {"id","verse_text","label"} <= schema:
          input_columns="verse_text"
          output_column="label"

        #amazon_polarity
        if {"label","content"} <= schema:
          input_columns="content"
          output_column="label"

        #tweet_eval (applicable for these subsets : emotion, hate, sentiment, stance_abortion, stance_athism, stance_feminism,stance_hilary)
        if {"text","label"} <= schema:
          input_columns="text"
          output_column="label"

        #glue/sst2
        if {"sentence","label"} <= schema:
          input_columns="sentence"
          output_column="label"

        # Creating a Dataframe to only only use the required columns
        df = pd.DataFrame()
        # Creating columns corresponding to the schema
        df['Review'] = [review[input_columns] for review in new_dataset]
        df['Sentiment'] = [review[output_column] for review in new_dataset]
        # Remove non-alphanumeric characters
        df['Review'] = df['Review'].apply(lambda x: re.sub("[^a-zA-Z]", ' ', str(x)))
        # Tokenize the training and testing data
        df_tokenized = tokenize_review(df)
        return df_tokenized

    def tokenize_review(df):
        # Tokenize Reviews in training
        tokened_reviews = [word_tokenize(rev) for rev in df['Review']]
        # Create word stems
        stemmed_tokens = []
        porter = PorterStemmer()
        for i in range(len(tokened_reviews)):
            stems = [porter.stem(token) for token in tokened_reviews[i]]
            stems = ' '.join(stems)
            stemmed_tokens.append(stems)
        df.insert(1, column='Stemmed', value=stemmed_tokens)
        return df

    # Function for generating the count vectorizer and transformaing the columns accordingly
    def transform_BOW(training, testing, column_name):    
        vect = CountVectorizer(max_features=100, ngram_range=(1,3), stop_words=ENGLISH_STOP_WORDS)
        vectFit = vect.fit(training[column_name])
        BOW_training = vectFit.transform(training[column_name])
        BOW_training_df = pd.DataFrame(BOW_training.toarray(), columns=vect.get_feature_names())
        BOW_testing = vectFit.transform(testing[column_name])
        BOW_testing_Df = pd.DataFrame(BOW_testing.toarray(), columns=vect.get_feature_names())
        return vectFit, BOW_training_df, BOW_testing_Df

    # Function for generating the tfidf vectorizer and transformaing the columns accordingly
    def transform_tfidf(training, testing, column_name):
        Tfidf = TfidfVectorizer(ngram_range=(1,3), max_features=100, stop_words=ENGLISH_STOP_WORDS)
        Tfidf_fit = Tfidf.fit(training[column_name])
        Tfidf_training = Tfidf_fit.transform(training[column_name])
        Tfidf_training_df = pd.DataFrame(Tfidf_training.toarray(), columns=Tfidf.get_feature_names())
        Tfidf_testing = Tfidf_fit.transform(testing[column_name])
        Tfidf_testing_df = pd.DataFrame(Tfidf_testing.toarray(), columns=Tfidf.get_feature_names())
        return Tfidf_fit, Tfidf_training_df, Tfidf_testing_df

    # Function for generating additional features
    def add_augmenting_features(df):
        tokened_reviews = [word_tokenize(rev) for rev in df['Review']]
        # Create feature that measures length of reviews
        len_tokens = []
        for i in range(len(tokened_reviews)):
            len_tokens.append(len(tokened_reviews[i]))
        len_tokens = preprocessing.scale(len_tokens)
        df.insert(0, column='Lengths', value=len_tokens)

        # Create average word length (training)
        Average_Words = [len(x)/(len(x.split())) for x in df['Review'].tolist()]
        Average_Words = preprocessing.scale(Average_Words)
        df['averageWords'] = Average_Words
        return df

    # Need to write code for both true or false conditions
    # Building the model
    def build_model(X_train, Y_train, X_test, Y_test, name_of_test):
        log_reg = LogisticRegression(C=30, max_iter=200).fit(X_train, Y_train)
        y_pred = log_reg.predict(X_test)
        print('Training accuracy of '+name_of_test+': ', log_reg.score(X_train, Y_train))
        print('Testing accuracy of '+name_of_test+': ', log_reg.score(X_test, Y_test))
        print(classification_report(Y_test, y_pred))  # Evaluating prediction ability
        return log_reg

    # Function for wrapping the model and the tokenizer needed for the attack
    def _wrapping_model(model,tokenizer,path):

        model_wrap = SklearnModelWrapper(model,tokenizer)
        print("model wrapping done")
        _black_box_execution(model_wrap,path)
        #_white_box_execution(model_wrap)
            
    '''
    # Function for executing white box attacks
    def _white_box_execution(model_w):
        # Using parser to check if white box attacks need to be executed
        if parser.getboolean('attack_type', 'parameters.white_box'):
            white_box(model_w,dataset)
    '''
    # Function for executing black box attacks
    def _black_box_execution(model_w,path):
        # Using parser to check if black box attacks need to be executed
        if parser.getboolean('attack_type', 'parameters.black_box'):
            # Checking at which level attack needs to be executed 
            if parser.getboolean('level_of_attack', 'parameters.character_level'):
                bba.character_level(model_w,dataset,path)

            if parser.getboolean('level_of_attack', 'parameters.word_level'):
                print("in word level_function of execute")
                bba.word_level(model_w,dataset,path)
                    
            if parser.getboolean('level_of_attack', 'parameters.sentence_level'):
                bba.sentence_level(model_w,dataset,path)

    def Textattack_execute():
        
        df_train = load_data('train')
        df_train = add_augmenting_features(df_train)
        df_test = load_data('test')
        df_test = add_augmenting_features(df_test)
        
        if parser.getboolean('embedder_type', 'parameters.cv_tokenizer'):
            tokenizer, df_train_bow_unstem, df_test_bow_unstem = transform_BOW(df_train, df_test, 'Review')
            model = build_model(df_train_bow_unstem, df_train['Sentiment'], df_test_bow_unstem, df_test['Sentiment'], 'BOW Unstemmed')
            _wrapping_model(model,tokenizer,cv_csv_path)
            print("cv_tokenizer Done")
        if parser.getboolean('embedder_type','parameters.tfidf_tokenizer'):
            tokenizer, df_train_tfidf_unstem, df_test_tfidf_unstem = transform_tfidf(df_train, df_test, 'Review')
            model = build_model(df_train_tfidf_unstem, df_train['Sentiment'], df_test_tfidf_unstem, df_test['Sentiment'], 'TFIDF Unstemmed')
            _wrapping_model(model,tokenizer,tfidf_csv_path)
            print("TfidfVectorizer done")
    '''
    if __name__ == '__main__':
        Textattack_execute()
    '''
else:
    print("Invalid License")