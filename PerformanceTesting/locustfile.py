# from locust import HttpUser , task , constant
# from configparser import ConfigParser

# class User(HttpUser):
# 	wait_time = constant(0)

# 	@task
# 	def input(self):
# 		parser = ConfigParser()
# 		parser.read('./Configs/config_performance_testing.properties')
# 		a = parser["locust_settings"]["endpoint"]
# 		b = parser["locust_settings"]["form_value"]
# 		self.client.post(a , { "text" : b })

from locust import HttpUser, task, constant
from read_data import CsvRead


class User(HttpUser):
    wait_time = constant(1)

    @task(1)
    def hit_request_1(self):
        
        test_data = CsvRead("./PerformanceTesting/Amazon_1_Review.csv").read()

        data = {
            "text": test_data["Text"]    
        }
        # print(data) 
        name = "One_Row"
        self.client.post("/" , name = name , data = data)


    @task(2)
    def hit_request_2(self):
        
        # test_data = CsvRead("./PerformanceTesting/Amazon_5_Reviews.csv").read()
        test_data = CsvRead("./PerformanceTesting/Amazon_5_Reviews.csv").read()
        data = {
            "text": test_data["Text"]    
        }
        # print(data)
        name = "Five_Rows"
        self.client.post("/" , name = name , data = data)


    @task(3)
    def hit_request_3(self):
        
        test_data = CsvRead("./PerformanceTesting/Amazon_10_Reviews.csv").read()

        data = {
            "text": test_data["Text"]    
        }
        # print(data)
        name = "Ten_Rows"
        self.client.post("/" , name = name , data = data)

    @task(4)
    def hit_request_4(self):
        
        test_data = CsvRead("./PerformanceTesting/Amazon_Review_50.csv").read()

        data = {
            "text": test_data["Text"]    
        }
        # print(data)
        name = "Fifty_Rows"
        self.client.post("/" , name = name , data = data)

    @task(5)
    def hit_request_5(self):
        
        test_data = CsvRead("./PerformanceTesting/Amazon_Review_100.csv").read()

        data = {
            "text": test_data["Text"]    
        }
        # print(data)
        name = "Hundred_Rows"
        self.client.post("/" , name = name , data = data)
		 