import invokust
import json

def stress_test(locustfile,host,num_users,spawn_rate,run_time):

	settings_stress = invokust.create_settings(
	locustfile = locustfile,
	host = host,
	num_users = num_users,
	spawn_rate = spawn_rate,
	run_time= run_time  
	)

	loadtest = invokust.LocustLoadTest(settings_stress)
	loadtest.run()
	result = loadtest.stats()
	json_result = json.dumps(json.loads(json.dumps(result['requests']), parse_float=lambda x: round(float(x), 2)))
	# print(result.keys(),"RESULT KEYS")
	# json_result= json.dumps(result['requests'])
	# json_failures = json.dumps(json.loads(json.dumps(result['failures']), parse_float=lambda x: round(float(x), 2)))
	# print(json_result)
	json_failures = json.dumps(result["failures"])
	# print( json_failures )
	return json_result,json_failures

def soak_test(locustfile,host,num_users,spawn_rate,run_time):
	
	settings_soak = invokust.create_settings(
	locustfile = locustfile,
	host = host,
	num_users = num_users,
	spawn_rate = spawn_rate,
	run_time= run_time 
	)
	loadtest = invokust.LocustLoadTest(settings_soak)
	loadtest.run()
	result = loadtest.stats()
	json_result = json.dumps(json.loads(json.dumps(result['requests']), parse_float=lambda x: round(float(x), 2)))
	# json_result= json.dumps(result['requests'])
	# json_failures = json.dumps(json.loads(json.dumps(result['failures']), parse_float=lambda x: round(float(x), 2)))
	json_failures = json.dumps(result["failures"])
	return json_result, json_failures
	# df = pd.read_json(json_result).transpose()
	# df_failures = pd.read_json(json_failures).transpose()
	
	# df.to_json( path_or_buf = "C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline-v2.0/textpipeline/Results/soak_statistics.json" , orient="records")
	# df_failures.to_json( path_or_buf = "C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline-v2.0/textpipeline/Results/soak_statistics_failure.json" , orient="records")







