import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import os
    import pickle
    import pandas as pd
    import matplotlib.pyplot as plt
    import textwrap
    import warnings
    from configparser import ConfigParser, ExtendedInterpolation

    from ModelExplainability import global_explainability,local_explainability,bias_exp
    from Utility import utility

    # Ignore DeprecationWarning
    warnings.filterwarnings("ignore")

    # Use dark background
    plt.style.use('dark_background')
    # plt.style.use('seaborn-white')

    plt.rcParams["axes.grid"] = False
    # Remove Plotting Warnings
    plt.rcParams.update({'figure.max_open_warning': 0})

    class PassError(Exception):
        """Base class for exceptions in this module."""
        pass


    class GlobalExplainerError(Exception):
        """ Raise an error when global_explainability failed to execute."""

        # Initializer
        def __init__(self, msg):
            self.msg = msg

        def __str__(self):
            return repr(self.msg)


    class FileNotFound(Exception):
        """ Raise an error when files not found """

        # Initializer
        def __init__(self, msg):
            self.msg = msg

        def __str__(self):
            return repr(self.msg)


    class ModelExplainability:

        def __init__(self, best_estimator_dict, model, class_names, model_explainability_parser, logger):
            self.best_estimator_dict = best_estimator_dict
            self.model = model
            self.class_names = class_names
            self.model_explainability_parser = model_explainability_parser
            self.logger = logger

        def _global_explainability_linear_model(self, x_train, x_val, vocab):

            global_plots_save_path = self.model_explainability_parser.get('save_path', 'global_explainability_save_path')
            if self.model_explainability_parser.getboolean('global_explainability',
                                                           'execute.explainability.sklearn_implicit'):

                n_features = self.model_explainability_parser.getint('parameters',
                                                                     'logistic_regression.visualise_n_feature')
                try:
                    # get the feature importance plot and save it.
                    figures = global_explainability.logistic_regression_feature_importance(self.model, vocab,
                                                                                           self.class_names, n_features)
                    sklearn_plot_save_path = os.path.join(global_plots_save_path, 'SklearnFeatureImportance')

                    # Saving plots for all the class labels (only for linear model)
                    for index, fig in enumerate(figures):
                        utility.save_plot(fig, sklearn_plot_save_path,
                                          f'logistic_regression_feature_importance_{index}.png')
                        # plt.close()
                    print("sklearn implicit feature importance plot saved.")

                except Exception as e:
                    print("\n *** Global explainability logistic regression feature importance error *** ")
                    self.logger.critical(f' Global explainability logistic regression feature importance error:\n {e}')
                    PassError(e)
            
            if self.model_explainability_parser.getboolean('global_explainability', 'execute.explainability.shap_global'):

                try:
                    n_features = self.model_explainability_parser.getint('parameters',
                                                                         'shap_global_linear.visualise_n_feature')
                    # get the feature importance plot and save it.
                    fig = global_explainability.shap_global_linear_explainer(self.model, x_train, x_val, vocab,
                                                                             self.class_names, n_features)
                    shap_global_plot_save_path = os.path.join(global_plots_save_path, 'ShapGlobal')
                    utility.save_plot(fig, shap_global_plot_save_path, 'shap_global_linear_feature_importance.png')
                    # plt.close()
                    print("Shap global feature importance plot saved.")

                except Exception as e:
                    print("\n *** Global explainability shap linear explainer feature importance error *** ")
                    self.logger.critical(f' Global explainability shap linear explainer feature importance error:\n {e}')
                    PassError(e)

            if self.model_explainability_parser.getboolean('global_explainability',
                                                           'execute.explainability.decision_tree_surrogate'):
                try:
                    n_features = self.model_explainability_parser.getint('parameters',
                                                                         'decision_tree_surrogate.visualise_n_feature')
                    # get the feature importance plot and save it.
                    fig, _ = global_explainability.decision_tree_surrogate(self.model, x_val, vocab, n_features)
                    decision_tree_plot_save_path = os.path.join(global_plots_save_path, 'DecisionTreeSurrogate')
                    utility.save_plot(fig, decision_tree_plot_save_path, 'DecisionTreeSurrogate_feature_importance.png')
                    plt.style.use('dark_background')
                    # plt.close()
                    print("Decision tree surrogate global feature importance plot saved.")

                except Exception as e:
                    print("\n *** Global explainability decision tree surrogate explainer feature importance error *** ")
                    self.logger.critical(
                        f' Global explainability decision tree surrogate explainer feature importance error:\n {e}')
                    PassError(e)

            return None

        def _global_explainability_forest_model(self, x_train, x_val, vocab):

            global_plots_save_path = self.model_explainability_parser.get('save_path', 'global_explainability_save_path')
            if self.model_explainability_parser.getboolean('global_explainability',
                                                           'execute.explainability.sklearn_implicit'):

                n_features = self.model_explainability_parser.getint('parameters',
                                                                     'logistic_regression.visualise_n_feature')
                try:
                    # get the feature importance plot and save it.
                    figure = global_explainability.random_forest_feature_importance(self.model, vocab,n_features)
                    sklearn_plot_save_path = os.path.join(global_plots_save_path, 'RandomforestFeatureImportance')

                    utility.save_plot(figure, sklearn_plot_save_path,'random_forest_feature_importance.png')
                    # plt.close()
                    print("random forest feature importance plot saved.")

                except Exception as e:
                    print("\n *** Global explainability random forest feature importance error *** ")
                    self.logger.critical(f' Global explainability random forest feature importance error:\n {e}')
                    PassError(e)
            
            if self.model_explainability_parser.getboolean('global_explainability', 'execute.explainability.shap_global'):

                try:
                    n_features = self.model_explainability_parser.getint('parameters',
                                                                         'shap_global_linear.visualise_n_feature')
                    # get the feature importance plot and save it.
                    fig = global_explainability.shap_global_tree_explainer(self.model, x_val, vocab,
                                                                             self.class_names, n_features)
                    shap_global_plot_save_path = os.path.join(global_plots_save_path, 'ShapGlobal')
                    utility.save_plot(fig, shap_global_plot_save_path, 'shap_global_tree_feature_importance.png')
                    # plt.close()
                    print("Shap global feature importance plot saved.")

                except Exception as e:
                    print("\n *** Global explainability shap tree explainer feature importance error *** ")
                    self.logger.critical(f' Global explainability shap tree explainer feature importance error:\n {e}')
                    PassError(e)

            if self.model_explainability_parser.getboolean('global_explainability',
                                                           'execute.explainability.decision_tree_surrogate'):
                try:
                    n_features = self.model_explainability_parser.getint('parameters',
                                                                         'decision_tree_surrogate.visualise_n_feature')
                    # get the feature importance plot and save it.
                    fig, _ = global_explainability.decision_tree_surrogate(self.model, x_val, vocab, n_features)
                    decision_tree_plot_save_path = os.path.join(global_plots_save_path, 'DecisionTreeSurrogate')
                    utility.save_plot(fig, decision_tree_plot_save_path, 'DecisionTreeSurrogate_feature_importance.png')
                    plt.style.use('dark_background')
                    # plt.close()
                    print("Decision tree surrogate global feature importance plot saved.")

                except Exception as e:
                    print("\n *** Global explainability decision tree surrogate explainer feature importance error *** ")
                    self.logger.critical(
                        f' Global explainability decision tree surrogate explainer feature importance error:\n {e}')
                    PassError(e)

            return None

        def _local_explainability_linear_model(self, x_train,text_val_x,text_val_y,x_val,y_val, vocab,vectorizer):

            # plt.style.use('dark_background')
            plt.rcParams["axes.grid"] = False
            local_plots_save_path = self.model_explainability_parser.get('save_path', 'local_explainability_save_path')
            y_pred = self.model.predict(x_val)
            # plot the indexes based on false +ves and false -ves
            False_positive_index,False_negative_index = local_explainability.perf_measure(y_val,y_pred)
        
            # lime explainability
            if self.model_explainability_parser.getboolean('local_explainability',
                                                           'execute.explainability.lime_exp'):

                n_features = self.model_explainability_parser.getint('parameters',
                                                                     'lime_exp.num_features')
                # top_labels = self.model_explainability_parser.getint('parameters','lime_exp.top_labels')            
                try:
                    # get the lime explainability plot and save it.
                    lime_plot_save_path = os.path.join(local_plots_save_path, 'LimeExplanation')
                    for i in False_positive_index[:5]:
                        # print("False Positive instance number in test_data",i)
                        exp = local_explainability.lime_exp(self.model,vectorizer,text_val_x,text_val_y,self.class_names,i,n_features)
                        fig= exp.as_pyplot_figure()
                        # wrapper = textwrap.TextWrapper(width=70) 
                        # string = wrapper.fill(text=text_val_x[i])
                        # plt.title("Text: {}" .format(string))
                        plt.title("Lime Explanation")
                        plt.xlabel("Predicted class: {} True class: {} " .format(y_pred[i],text_val_y[i]))
                        # plt.show()
                        lime_plot_fp_save_path = os.path.join(lime_plot_save_path,'FalsePositive')
                        utility.save_plot(fig, lime_plot_fp_save_path, 'lime_fp_'+str(i)+'.png')
                        # plt.close()
                        # utility.save_html(fig, lime_plot_save_path, 'lime_fp_'+str(i)+'.html')

                    for j in False_negative_index[:5]:
                        # print("False Negative instance number in test_data",j)
                        exp = local_explainability.lime_exp(self.model,vectorizer,text_val_x,text_val_y,self.class_names,j,n_features)
                        fig= exp.as_pyplot_figure()
                        # wrapper = textwrap.TextWrapper(width=70) 
                        # string = wrapper.fill(text=text_val_x[j])
                        # plt.title("Text: {}" .format(string))
                        plt.title("Lime Explanation")
                        plt.xlabel("Predicted class: {} True class: {} " .format(y_pred[j],text_val_y[j]))
                        lime_plot_fn_save_path = os.path.join(lime_plot_save_path,'FalseNegative')
                        utility.save_plot(fig, lime_plot_fn_save_path, 'lime_fn_'+str(j)+'.png')
                        # plt.close()
                    print("lime explainability plot saved.")

                except Exception as e:
                    print("\n *** Local explainability LimeExplanation error *** ")
                    self.logger.critical(f' local explainability LimeExplanation error:\n {e}')
                    PassError(e)
            
            # shap local
            if self.model_explainability_parser.getboolean('local_explainability', 'execute.explainability.shap_local'):

                try:
                    shap_local_plot_save_path = os.path.join(local_plots_save_path, 'Shaplocal')
                    test_features_array = x_val.toarray()
                    # get the shap local plot and save it.
                    for i in False_positive_index[:5]:
                        if self.best_estimator_dict['model_name'] == 'linear_model':
                            fig = local_explainability.shap_local(self.model, x_train,test_features_array,i,self.class_names, vocab)
                        elif self.best_estimator_dict['model_name'] == 'forest_model':
                            fig = local_explainability.shap_tree_local(self.model,test_features_array,i,self.class_names, vocab)
                        # wrapper = textwrap.TextWrapper(width=70) 
                        # string = wrapper.fill(text=text_val_x[i])
                        # plt.title("Text: {}" .format(string))
                        # plt.xlabel("Predicted class: {} True class: {} " .format(y_pred[i],text_val_y[i]), fontsize=18)                                            
                        # plt.xlabel("Text: {} \n Predicted class: {} True class: {} " .format(string,y_pred[i],text_val_y[i]))                                            
                        shap_local_fp_plot_save_path = os.path.join(shap_local_plot_save_path,'FalsePositive')
                        # utility.save_html(fig, shap_local_fp_plot_save_path, 'shap_fp_'+str(i)+'.html')
                        utility.save_plot(fig, shap_local_fp_plot_save_path, 'shap_fp_'+str(i)+'.png')
        
                    for j in False_negative_index[:5]:
                        if self.best_estimator_dict['model_name'] == 'linear_model':
                            fig = local_explainability.shap_local(self.model, x_train,test_features_array,j,self.class_names, vocab)
                        elif self.best_estimator_dict['model_name'] == 'forest_model':
                            fig = local_explainability.shap_tree_local(self.model,test_features_array,j,self.class_names, vocab)
                        # fig= exp.as_pyplot_figure()
                        # wrapper = textwrap.TextWrapper(width=70) 
                        # string = wrapper.fill(text=text_val_x[j])
                        # plt.title("Text: {}" .format(string))
                        # plt.xlabel("Predicted class: {} True class: {} " .format(y_pred[j],text_val_y[j]), fontsize=18)                                                    
                        shap_local_fn_plot_save_path = os.path.join(shap_local_plot_save_path,'FalseNegative')
                        utility.save_plot(fig, shap_local_fn_plot_save_path, 'shap_fn_'+str(j)+'.png')
                    print("Shap local plot saved.")

                except Exception as e:
                    print("\n *** Local explainability shap local error *** ")
                    self.logger.critical(f' Local explainability shap local error:\n {e}')
                    PassError(e)

            # eli5
            # if self.model_explainability_parser.getboolean('bias',
            #                                                'execute.explainability.eli5_exp'):
            #     try:
            #         eli5_df_save_path = os.path.join(local_plots_save_path, 'eli5')
            #         # get the eli5 prediction data frame and save it.
            #         df_fp_eli = pd.DataFrame(data=None,columns=["target","feature","weight","value"])
            #         for i in False_positive_index[:5]:
            #             _,df_pred = local_explainability.eli5_exp(self.model,vectorizer,text_val_x,self.class_names,i)                                                    
            #             eli5_df_fp_save_path = os.path.join(eli5_df_save_path,'FalsePositive')
            #             df_fp_eli = pd.concat([df_fp_eli,df_pred])
            #             # print(df_pred)
            #             # utility.save_df(df_pred, eli5_df_fp_save_path, 'df_fp_pred_'+str(i))                 
            #         utility.save_df(df_fp_eli, eli5_df_fp_save_path, 'df_fp_eli5')
        
            #         df_fn_eli = pd.DataFrame(data=None,columns=["target","feature","weight","value"])
            #         for j in False_negative_index[:5]:
            #             _,df_pred = local_explainability.eli5_exp(self.model,vectorizer,text_val_x,self.class_names,j)                                                    
            #             eli5_df_fn_save_path = os.path.join(eli5_df_save_path,'FalseNegative')
            #             df_fn_eli = pd.concat([df_fn_eli,df_pred])
            #             # utility.save_df(df_pred, eli5_df_fn_save_path, 'df_fn_pred_'+str(j))
            #         utility.save_df(df_fn_eli, eli5_df_fn_save_path, 'df_fn_eli5')

                   
            #         print("Eli5 predictions dataframe saved.")

            #     except Exception as e:
            #         print("\n *** Local explainability eli5 error *** ")
            #         self.logger.critical(
            #             f' Local explainability eli5 error:\n {e}')
            #         PassError(e)

            return None

        def _fairml(self,x_train,vocab,x_val,y_val,text_val_x,vectorizer):

            
            # fairml bias explainability
            if self.model_explainability_parser.getboolean('bias','execute.explainability.fairml'):
                plot_save_path = self.model_explainability_parser.get('save_path', 'bias_save_path')
                n_features = self.model_explainability_parser.getint('parameters','fairml.visualise_n_feature')
                try:
                    # fairml_plot_save_path = os.path.join(plot_save_path, 'Fairml_bias')
                    fig = bias_exp.fairml_text(self.model,x_train,vocab,n_features)
                    utility.save_plot(fig, plot_save_path, 'fairml_bias.png')
                    print("fairml plot saved.")

                except Exception as e:
                    print("\n *** Fairml error *** ")
                    self.logger.critical(f' Fairml error:\n {e}')
                    PassError(e)
            if self.model_explainability_parser.getboolean('bias','execute.explainability.eli5_exp'):
                plot_save_path = self.model_explainability_parser.get('save_path', 'bias_save_path')
                y_pred = self.model.predict(x_val)
                False_positive_index,False_negative_index = local_explainability.perf_measure(y_val,y_pred)
                # print(False_positive_index,False_negative_index,"False_positive_index $ False_negative_index")
                
                try:
                    eli5_df_save_path = os.path.join(plot_save_path, 'eli5')
                    # get the eli5 prediction data frame and save it.
                    df_fp_eli = pd.DataFrame(data=None,columns=["target","feature","weight","value"])
                    for i in False_positive_index[:5]:
                    # for i in range(5):
                        # print(i,"False_positive_index")
                        # print(text_val_x[i])
                        _,df_pred = bias_exp.eli5_exp(self.model,vectorizer,text_val_x,self.class_names,i)                                                    
                        eli5_df_fp_save_path = os.path.join(eli5_df_save_path,'FalsePositive')
                        df_fp_eli = pd.concat([df_fp_eli,df_pred])
                        # print(df_pred)
                        # utility.save_df(df_pred, eli5_df_fp_save_path, 'df_fp_pred_'+str(i))                 
                    utility.save_df(df_fp_eli, eli5_df_fp_save_path, 'df_fp_eli5')
        
                    df_fn_eli = pd.DataFrame(data=None,columns=["target","feature","weight","value"])
                    for j in False_negative_index[:5]:
                        # print(j,"False_negative_index")

                        _,df_pred = bias_exp.eli5_exp(self.model,vectorizer,text_val_x,self.class_names,j)                                                    
                        eli5_df_fn_save_path = os.path.join(eli5_df_save_path,'FalseNegative')
                        df_fn_eli = pd.concat([df_fn_eli,df_pred])
                        # utility.save_df(df_pred, eli5_df_fn_save_path, 'df_fn_pred_'+str(j))
                    utility.save_df(df_fn_eli, eli5_df_fn_save_path, 'df_fn_eli5')

                   
                    print("Eli5 predictions dataframe saved.")

                except Exception as e:
                    print("\n *** Local explainability eli5 error *** ")
                    self.logger.critical(
                        f' Local explainability eli5 error:\n {e}')
                    PassError(e)
            return None

        def explainability_execute(self):

            # get the Validation data and vocab of embeddings
            train_x = None
            val_x = None
            val_y = None
            vocab = None
            vectorizer = None
            text_val_x = None
            text_val_y = None
            val_data_path = self.model_explainability_parser.get('load_path','cleaned_dataframe_save_path')
            val_data_complete_path = os.path.join(val_data_path, 'cleaned_dataframe_validation_set.csv')
            val_data = pd.read_csv(val_data_complete_path) 
            # print(val_data.head())
            text_val_x = val_data['Text']
            text_val_y = val_data['Target']

            if self.best_estimator_dict['embedding_name'] == 'count_vectoriser':

                # Loading Validation CountVectoriser Embeddings and Validation Target values and Vocabulary.
                count_vect_path = self.model_explainability_parser.get('load_path', 'count_vect_embedding_load_path')

                # Training CountVectoriser Embeddings
                try:
                    with open(os.path.join(count_vect_path, 'count_vect_train_x.pkl'), 'rb') as f:
                        train_x = pickle.load(f)

                    # Validation CountVectoriser Embeddings
                    with open(os.path.join(count_vect_path, 'count_vect_val_x.pkl'), 'rb') as f:
                        val_x = pickle.load(f)

                    # Validation Target values
                    with open(os.path.join(count_vect_path, 'val_y.pkl'), 'rb') as f:
                        val_y = pickle.load(f)

                    # Vocabulary
                    with open(os.path.join(count_vect_path, 'vocab.pkl'), 'rb') as f:
                        vocab = pickle.load(f)

                    # Vectorizer
                    with open(os.path.join(count_vect_path, 'count_vectoriser.pkl'), 'rb') as f:
                        vectorizer = pickle.load(f)

                except Exception as e:
                    self.logger.critical(f'*** count_vectoriser pickle file was not loaded ***\n{e}')
                    raise PassError(e)

            elif self.best_estimator_dict['embedding_name'] == 'tfidf_vectoriser':

                # Loading Validation TfidfVectoriser Embeddings and Validation Target values and Vocabulary.
                tfidf_vect_path = self.model_explainability_parser.get('load_path', 'tfidf_vect_embedding_load_path')
                try:
                    # Training TfidfVectoriser Embeddings
                    with open(os.path.join(tfidf_vect_path, 'tfidf_vect_train_x.pkl'), 'rb') as f:
                        train_x = pickle.load(f)

                    # Validation TfidfVectoriser Embeddings
                    with open(os.path.join(tfidf_vect_path, 'tfidf_vect_val_x.pkl'), 'rb') as f:
                        val_x = pickle.load(f)

                    # Validation Target values
                    with open(os.path.join(tfidf_vect_path, 'val_y.pkl'), 'rb') as f:
                        val_y = pickle.load(f)

                    # Vocabulary
                    with open(os.path.join(tfidf_vect_path, 'vocab.pkl'), 'rb') as f:
                        vocab = pickle.load(f)
                    # Vectorizer
                    with open(os.path.join(tfidf_vect_path, 'tfidf_vectoriser.pkl'), 'rb') as f:
                        vectorizer = pickle.load(f)

                except Exception as e:
                    self.logger.critical(f'*** tfidf_vectoriser pickle file was not loaded ***\n{e}')
                    raise PassError(e)

            # Global Explainability
            try:
                if not [item for item in (train_x, val_x, val_y, vocab) if item is None]:

                    if self.best_estimator_dict['model_name'] == 'linear_model':
                        try:
                            self._global_explainability_linear_model(train_x, val_x, vocab)

                        except Exception as e:
                            print("\n *** global_explainability function failed to execute *** ")
                            self.logger.critical(f'*** global_explainability function failed to execute. *** \n{e}')
                            raise PassError(e)

                    elif self.best_estimator_dict['model_name'] == 'forest_model':
                        try:
                            self._global_explainability_forest_model(train_x, val_x, vocab)

                        except Exception as e:
                            print("\n *** global_explainability function failed to execute *** ")
                            self.logger.critical(f'*** global_explainability function failed to execute. *** \n{e}')
                            raise PassError(e)
            
            except Exception as e:
                self.logger.critical(f'*** Embedding files not found for model explainability ***\n{e}')
                # raise FileNotFound(" Embedding files not found for model explainability")
                raise PassError(e)


            # Local Explainability
            try:
                if not [item for item in (train_x,text_val_x,text_val_y,val_x, val_y, vocab,vectorizer) if item is None]:

                    if self.best_estimator_dict['model_name'] == 'linear_model':
                        try:
                            self._local_explainability_linear_model(train_x,text_val_x,text_val_y, val_x,val_y, vocab,vectorizer)

                        except Exception as e:
                            print("\n *** local_explainability function failed to execute *** ")
                            self.logger.critical(f'*** local_explainability function failed to execute. *** \n{e}')
                            raise PassError(e)

                    elif self.best_estimator_dict['model_name'] == 'forest_model':
                        try:
                            self._local_explainability_linear_model(train_x,text_val_x,text_val_y, val_x,val_y, vocab,vectorizer)

                        except Exception as e:
                            print("\n *** local_explainability function failed to execute *** ")
                            self.logger.critical(f'*** local_explainability function failed to execute. *** \n{e}')
                            raise PassError(e)
            except Exception as e:
                self.logger.critical(f'*** Embedding files not found for model explainability ***\n{e}')
                # raise FileNotFound(" Embedding files not found for model explainability")
                raise PassError(e)

            # Fairml Explainability
            try:
                if not [item for item in (train_x,vocab) if item is None]:

                    if self.best_estimator_dict['model_name'] == 'linear_model':
                        try:
                            self._fairml(train_x,vocab,val_x,val_y,text_val_x,vectorizer)

                        except Exception as e:
                            print("\n *** Fairml function failed to execute *** ")
                            self.logger.critical(f'*** Fairml function failed to execute. *** \n{e}')
                            raise PassError(e)

                    elif self.best_estimator_dict['model_name'] == 'forest_model':
                        try:
                            self._fairml(train_x,vocab,val_x,val_y,text_val_x,vectorizer)

                        except Exception as e:
                            print("\n *** Fairml function failed to execute *** ")
                            self.logger.critical(f'*** Fairml function failed to execute. *** \n{e}')
                            raise PassError(e)
            except Exception as e:
                self.logger.critical(f'*** Embedding files not found for model explainability ***\n{e}')
                # raise FileNotFound(" Embedding files not found for model explainability")
                raise PassError(e)

else:
    print("Invalid License")