from truepy import License
import os
import signal
import ntplib
import datetime,time

def license_status():
    try:
        # Load the certificate
        with open('./License/certificate.pem', 'rb') as f:
            certificate = f.read()

        # Load the license
        with open('./License/license.key', 'rb') as f:
            # license = License.load(f, b'123#@U')
            license = License.load(f, b'12345')
# 


        # Verify the license; this will raise License.InvalidSignatureException if
        # the signature is incorrect
        license.verify(certificate)
        # print('Make sure you have an internet connection.')
        try:
            client = ntplib.NTPClient()
            response = None
            while True:
                response = client.request('1.pool.ntp.org')
                if response != None:
                    break
            # response = client.request('2.pool.ntp.org')
            Internet_date_and_time = datetime.datetime.fromtimestamp(response.tx_time)
            current_time = Internet_date_and_time  
            print('\n')
            # print('Internet date and time as reported by Server: ',Internet_date_and_time)
            start_period  = license.data.not_before
            expiry_period = license.data.not_after
            print("Start",start_period)
            print("End",expiry_period)
        except OSError:
            print('\n')
            # print('Internet date and time could not be reported by server.')
            print("Code Cannot be run Offline without Internet Connection")


        # print ('Current Time Stamp',current_time) 
        # print('License information')
        # print('\tvalid from:\t%s' % str(license.data.not_before))
        # print('\tvalid to:\t%s' % str(license.data.not_after))
        # start_period  = license.data.not_before
        # expiry_period = license.data.not_after
        # print("New",expiry_period)
            
        if current_time < expiry_period and current_time > start_period:
            print("Valid License !!!")
            return "Valid@tAi"
            # print('License valid till',expiry_period)
        else:
            # License_status.append("Invalid")
            # print("Kindly check your license period. License period may be Expired/ Advanced !!!")
            # raise Exception("Kindly check your license period. License period may be Expired/ Advanced !!!")
            
            # add auto delete code here
            return "Invalid"
    except Exception as e:
        print(e)
        print("Invalid License !!!")
        return "Invalid"
