from textattack.attack_recipes import DeepWordBugGao2018, Pruthi2019
from textattack.attack_recipes import GeneticAlgorithmAlzantot2018, CheckList2020, IGAWang2019, InputReductionFeng2018
from textattack.attack_recipes import PSOZang2020, PWWSRen2019, TextFoolerJin2019, BAEGarg2019, CLARE2020
from configparser import ConfigParser, ExtendedInterpolation
from textattack import Attacker
import textattack

# Setting the parser 
parser = ConfigParser(interpolation=ExtendedInterpolation())
parser.read('./Configs/config_attack_vectors.properties')

#Loading the parameters for attack arguements
n_examples = parser.getint('attack_arguements', 'parameters.num_examples')

# Function for character level black box attacks 
def character_level(model_wrap, dataset,path):
    DeepWordBug(model_wrap,dataset,path)
    Pruthi(model_wrap,dataset,path)

# Function for word level black box attacks

def word_level(model_wrap, dataset,path):
    
    #CheckList(model_wrap,dataset,path)
    try:

        Alzantot(model_wrap,dataset,path)
    except Exception as e:
        print("Alzantot",e)
    try:
        IGA(model_wrap,dataset,path)
    except Exception as e:
        print("IGA",e)
    try:
        InputReduction(model_wrap,dataset,path)
    except Exception as e:
        print("InputReduction",e)
    try:
        PWWS(model_wrap,dataset,path)
    except Exception as e:
        print("PWWS",e)
    try:
        TextFooler(model_wrap,dataset,path)
    except Exception as e:
        print("TextFooler",e)

# Function for sentence level black box attacks
def sentence_level(model_wrap, dataset,path):
    
    BAE(model_wrap,dataset,path)
    
# Function for attacking the model with the required attack for black box
def _attacking_blackbox_model(attack, dataset, attack_args):

    attacker = Attacker(attack, dataset, attack_args)
    attacker.attack_dataset()


def DeepWordBug(model_wrap, dataset,path):

    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"DEEPWORDBUG.csv")
    attack = DeepWordBugGao2018.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)

def Pruthi(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"PRUTHI.csv")            
    attack = Pruthi2019.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)
    
def Alzantot(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"ALZANTOT.csv")
    attack =  GeneticAlgorithmAlzantot2018.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)

def CheckList(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"CHECKLIST.csv")        
    attack =  CheckList2020.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)

def IGA(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"IGA.csv")        
    attack =  IGAWang2019.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)

def InputReduction(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"INPUTREDUCTION.csv")        
    attack =  InputReductionFeng2018.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)

def PSO(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"PSO.csv")        
    attack =  PSOZang2020.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)

def PWWS(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"PWWS.csv")        
    attack =  PWWSRen2019.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)
    

def TextFooler(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"TEXTFOOLER.csv")
    attack =  TextFoolerJin2019.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)


def BAE(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"BAE.csv")     
    attack =  BAEGarg2019.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)

def CLARE(model_wrap, dataset, path):
    attack_args = textattack.AttackArgs(num_examples = n_examples,log_to_csv=path+"CLARE.csv")    
    attack =  CLARE2020.build(model_wrap)
    _attacking_blackbox_model(attack, dataset, attack_args)



