from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    # Extension("app_text",  ["app_text.py"]),
    # Extension("data_diagnostic_execute",  ["data_diagnostic_execute.py"]),
    # Extension("deepxplore_implementation",  ["deepxplore_implementation.py"]),
    # Extension("json_report_generation",  ["json_report_generation.py"]),
    # Extension("main",  ["main.py"]),
    # Extension("model_explainability_execute",  ["model_explainability_execute.py"]),
    # Extension("modeling_execute",  ["modeling_execute.py"]),
    # Extension("performance_testing_execute",  ["performance_testing_execute.py"]),
    Extension("black_box_attacks",  ["black_box_attacks.py"])
    #Extension("attack_vectors_execute",  ["attack_vectors_execute.py"])
    #Extension("license_flask_execute",  ["white_box_attacks.py"])
]

setup(
    name = 'My Program Name',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)