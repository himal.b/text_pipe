import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import os
    import pandas as pd
    import numpy as np
    import logging
    import pickle
    # import json
    import sys
    from configparser import ConfigParser, ExtendedInterpolation
    from Deepxplore.params import *
    from Deepxplore.utils import *
    from Deepxplore.modeldevlopment import *
    from sklearn.model_selection import train_test_split
    from Deepxplore import sentence_embed as se
    import tensorflow as tf
    from Utility import utility as ut
    tf.compat.v1.disable_eager_execution()
    color= ut.bcolors
    m1f=[]
    tsi=[]
    sep = "/"
    parser = ConfigParser(interpolation=ExtendedInterpolation())
    parser.read('./Configs/config_deepxplore.properties')

    class PassError(Exception):
        """Base class for exceptions in this module."""
        pass

    # class NumpyEncoder(json.JSONEncoder):
    #     def default(self, obj):
    #         if isinstance(obj, np.ndarray):
    #             return obj.tolist()
    #         return json.JSONEncoder.default(self, obj)


    def neuraoncov(data,ma,mb,load_weights_1_path,load_weights_2_path,load_weights_3_path,save_path): 

        x_train, x_test,  y_train, y_test = train_test_split(data[:,0:-1], data[:,-1], test_size=0.3, random_state=10)
        tsnew=np.concatenate((x_test, y_test.reshape(-1,1)), axis=1)  
        trnew=np.concatenate((x_train, y_train.reshape(-1,1)), axis=1) 
        #modeldev(trnew,fn)
        X_test = x_test.astype('float32')
        num_features = X_test.shape[1]
        output_features = len(np.unique(y_train))
        print("num_features:",num_features)
        feat_names=[]
        for i in range(1,num_features+1):
            feat_names.append('f'+str(i))
        names=[]
        for i in range(0,X_test.shape[0]):
            names.append('TS'+str(i))
        incre_idx, incre_decre_idx = init_feature_constraints(feat_names,num_features)
        input_tensor = Input(shape=(num_features,))
        # load multiple models sharing same input tensor
        K.set_learning_phase(0)
        pred=np.zeros((X_test.shape[0],4))
        n = output_features
        pred1=np.zeros((X_test.shape[0],n*4))
        K.set_learning_phase(0)
        model1 = Model1(input_tensor=input_tensor,output_features=output_features, load_weights=True,load_weights_1_path=load_weights_1_path,num_features=num_features)
        model2 = Model2(input_tensor=input_tensor,output_features=output_features, load_weights=True,load_weights_2_path=load_weights_2_path,num_features=num_features)
        model3 = Model3(input_tensor=input_tensor,output_features=output_features, load_weights=True,load_weights_3_path=load_weights_3_path,num_features=num_features)
        m1f.append(model1)
        m1f.append(model2)
        m1f.append(model3)
        pred[:,0]= np.argmax(model1.predict(X_test),axis=1)
        pred[:,1]= np.argmax(model2.predict(X_test),axis=1)
        pred[:,2]= np.argmax(model3.predict(X_test),axis=1)
        pred[:,3]= y_test
        pred1[:,0:n]= model1.predict(X_test)
        pred1[:,n:n*2]= model2.predict(X_test)
        pred1[:,n*2:n*3]= model3.predict(X_test)
        y_test1 = to_categorical(y_test,output_features)
        pred1[:,n*3:n*4]= y_test1
        # the json file where the output must be stored 
        # out_file = open(sep.join([save_path,"pred_prob.json"]), "w") 
        # json.dump(pred1, out_file, cls=NumpyEncoder)
        # out_file.close()
        np.savetxt(sep.join([save_path,'pred_prob.csv']),pred1, delimiter=',', fmt='%f')
        np.savetxt(sep.join([save_path,'pred.csv']),pred,header="Model1,Model2,Model3,orig_label",comments="", delimiter=',', fmt='%f')
        
        threshold=0.0
        seeds=20
        step=10.0
        weight_diff=2.0
        weight_nc=0.1
        target_model=0
        grad_iterations=50

        model_layer_dict1, model_layer_dict2, model_layer_dict3 = init_coverage_tables(model1, model2, model3)
        for _ in range(seeds):
            # print(len(X_test))
            idx = random.randint(0, X_test.shape[0])
            # print(idx)
            gen_pdf = np.expand_dims(X_test[idx], axis=0)
            orig_pdf = gen_pdf.copy()
            
        # first check if input already induces differences
            label1, label2, label3 = np.argmax(model1.predict(gen_pdf)[0]), np.argmax(model2.predict(gen_pdf)[0]), np.argmax(
            model3.predict(gen_pdf)[0])
            if not label1 == label2 == label3:
                print(bcolors.OKGREEN + 'input already causes different outputs: {}, {}, {}'.format(label1, label2,
                                                                                                label3) + bcolors.ENDC)
                tsi.append(gen_pdf)
                update_coverage(gen_pdf, model1, model_layer_dict1, threshold)
                update_coverage(gen_pdf, model2, model_layer_dict2, threshold)
                update_coverage(gen_pdf, model3, model_layer_dict3, threshold)

                print(bcolors.OKGREEN + 'covered neurons percentage %d neurons %.3f, %d neurons %.3f, %d neurons %.3f'
                  % (len(model_layer_dict1), neuron_covered(model_layer_dict1)[2], len(model_layer_dict2),
                     neuron_covered(model_layer_dict2)[2], len(model_layer_dict3),
                     neuron_covered(model_layer_dict3)[2]) + bcolors.ENDC)
                averaged_nc = (neuron_covered(model_layer_dict1)[0] + neuron_covered(model_layer_dict2)[0] +
                           neuron_covered(model_layer_dict3)[0]) / float(
                neuron_covered(model_layer_dict1)[1] + neuron_covered(model_layer_dict2)[1] +
                neuron_covered(model_layer_dict3)[1])
                print(bcolors.OKGREEN + 'averaged covered neurons %.3f' % averaged_nc + bcolors.ENDC)

                # save the result to disk
                
                fname = 'label_diff.json'
                
                with open(sep.join([save_path,fname]), 'a') as f:
                    f.write(
                    'Already causes differences: name: {}, label1:{}, label2: {}, label3: {}\n'.format(names[idx], label1,label2, label3))
                # out_file = open(sep.join([save_path,fname]), "w") 
                # json.dump(f, out_file) 
                # out_file.close()                                                                                 
                continue

        # if all turning angles roughly the same
            orig_label = label1
            layer_name1, index1 = neuron_to_cover(model_layer_dict1)
            layer_name2, index2 = neuron_to_cover(model_layer_dict2)
            layer_name3, index3 = neuron_to_cover(model_layer_dict3)

        # construct joint loss function
            if target_model == 0:
                loss1 = -weight_diff * K.mean(model1.get_layer('before_softmax').output[..., orig_label])
                loss2 = K.mean(model2.get_layer('before_softmax').output[..., orig_label])
                loss3 = K.mean(model3.get_layer('before_softmax').output[..., orig_label])
            elif target_model == 1:
                loss1 = K.mean(model1.get_layer('before_softmax').output[..., orig_label])
                loss2 = -weight_diff * K.mean(model2.get_layer('before_softmax').output[..., orig_label])
                loss3 = K.mean(model3.get_layer('before_softmax').output[..., orig_label])
            elif target_model == 2:
                loss1 = K.mean(model1.get_layer('before_softmax').output[..., orig_label])
                loss2 = K.mean(model2.get_layer('before_softmax').output[..., orig_label])
                loss3 = -weight_diff * K.mean(model3.get_layer('before_softmax').output[..., orig_label])
            loss1_neuron = K.mean(model1.get_layer(layer_name1).output[..., index1])
            loss2_neuron = K.mean(model2.get_layer(layer_name2).output[..., index2])
            loss3_neuron = K.mean(model3.get_layer(layer_name3).output[..., index3])
            layer_output = (loss1 + loss2 + loss3) + weight_nc * (loss1_neuron + loss2_neuron + loss3_neuron)

        # for adversarial image generation
            final_loss = K.mean(layer_output)
        # we compute the gradient of the input picture wrt this loss
            grads = (K.gradients(final_loss, input_tensor)[0])

        # this function returns the loss and grads given the input picture
            iterate = K.function([input_tensor], [loss1, loss2, loss3, loss1_neuron, loss2_neuron, loss3_neuron, grads])
        # we run gradient ascent for 20 steps
            for iters in range(grad_iterations):
                loss_value1, loss_value2, loss_value3, loss_neuron1, loss_neuron2, loss_neuron3, grads_value = iterate(
                [gen_pdf])
                grads_value = constraint(grads_value, incre_idx, incre_decre_idx)  # constraint the gradients value

                gen_pdf += grads_value * step
                gen_pdf=deprocess_image(gen_pdf,ma,mb)
                label1, label2, label3 = np.argmax(model1.predict(gen_pdf)[0]), np.argmax(
                model2.predict(gen_pdf)[0]), np.argmax(model3.predict(gen_pdf)[0])
                print(label1)
                print(label2)
                
                if not label1 == label2 == label3:
                    update_coverage(gen_pdf, model1, model_layer_dict1, threshold)
                    update_coverage(gen_pdf, model2, model_layer_dict2, threshold)
                    update_coverage(gen_pdf, model3, model_layer_dict3, threshold)
                    tsi.append(gen_pdf)
                    print(bcolors.OKGREEN + 'covered neurons percentage %d neurons %.3f, %d neurons %.3f, %d neurons %.3f'
                      % (len(model_layer_dict1), neuron_covered(model_layer_dict1)[2], len(model_layer_dict2),
                         neuron_covered(model_layer_dict2)[2], len(model_layer_dict3),
                         neuron_covered(model_layer_dict3)[2]) + bcolors.ENDC)
                averaged_nc = (neuron_covered(model_layer_dict1)[0] + neuron_covered(model_layer_dict2)[0] +
                               neuron_covered(model_layer_dict3)[0]) / float(
                    neuron_covered(model_layer_dict1)[1] + neuron_covered(model_layer_dict2)[1] +
                    neuron_covered(model_layer_dict3)[1])
                # print(bcolors.OKGREEN + 'averaged covered neurons %.3f' % averaged_nc + bcolors.ENDC)
                fname2='proj.json'
                # os.makedirs(sep.join([deepxplore_results_path,'proj']), exist_ok=True)
                # save the result to disk
                with open(sep.join([save_path,fname2]), 'a') as f:
                    f.write('name: {}, label1:{}, label2: {}, label3: {}\n'.format(names[idx], label1, label2, label3))
                    f.write('changed features: {}\n\n'.format(features_changed(gen_pdf, orig_pdf, feat_names)))
                break
        return len(model_layer_dict1),neuron_covered(model_layer_dict1)[2], len(model_layer_dict2),neuron_covered(model_layer_dict2)[2], len(model_layer_dict3),neuron_covered(model_layer_dict3)[2]

    def cosine(u, v):
        return np.dot(u, v) / (np.linalg.norm(u) * np.linalg.norm(v))

    def best5word(data_path, input_column, target_column, tsi):
        # i=((i-1)%30)+1
        # fname='C:/Users/MULA SHIVA KUMAR/Downloads/strct_text_pipeline/textpipeline/Deepxplore/tfidf_vect_x.csv'
        # data=data[~np.isnan(data).any(axis=1)]
        data = np.genfromtxt(data_path,delimiter=',')
        data=data[:,0:-1]
        simv=np.zeros((np.shape(data)[0],np.shape(tsi)[0]))
        for i in range(0,np.shape(tsi)[0]):
            for j in range(0,np.shape(data)[0]):
                simv[j,i]=cosine(tsi[i],data[j,:])
        s1=np.argsort(simv,axis=0)
        s2=np.sort(simv,axis=0)
        return s1[-20:,:],s2[-20:,:]
        
    # saving metadata
    def meta_info_save(component_name, data):
        try:

            with open(sep.join(['./Results', 'meta_info', 'meta_info_deepxplore.pkl']), 'rb') as f:
                meta_dict = pickle.load(f)
                meta_dict[component_name] = data

            with open(sep.join(['./Results', 'meta_info', 'meta_info_deepxplore.pkl']), "wb") as f:
                pickle.dump(meta_dict, f)

        except:
            os.makedirs(sep.join(['./Results', 'meta_info']), exist_ok=True)
            meta_dict = dict()

            with open(sep.join(['./Results', 'meta_info', 'meta_info_deepxplore.pkl']), "wb") as f:
                meta_dict[component_name] = data
                pickle.dump(meta_dict, f)

    def deepxplore_results(data_path,input_column,target_column,path,model_1_path,model_2_path,model_3_path,batch_size,nb_epoch):
        
        # X=pd.read_csv(x,header=None)
        # y=pd.read_csv(y,header=None)
        # data = np.concatenate((X, y), axis=1)
        # fname = './Results/DeepXplore/sen_atis.csv'
        # data = pd.read_csv(fname)
        # print(data.head(),"DATA")
        os.makedirs(path, exist_ok=True)
        print(f"{color.OK}*** Sentence Embeded Started ***{color.RESET}")
        data = se.sen_embed(data_path, input_column, target_column)
        fname='sen_embedding.csv'
        sen_embed_path = sep.join([path,fname])
        np.savetxt(sen_embed_path,data, delimiter=',', fmt='%f')
        # change fname
        data=np.genfromtxt(sen_embed_path,delimiter=',')
        mb=np.max(data[:,0:-1])
        ma=np.min(data[:,0:-1])
        data[:,0:-1],scaler=normalizedata(data[:,0:-1])
        # data[:,0:-1]=normalizedata(data[:,0:-1])
        data[:,-1]=data[:,-1]-1
        num_features=data.shape[1]-1

        x_train, x_test,  y_train, y_test = train_test_split(data[:,0:-1], data[:,-1], test_size=0.3, random_state=10)           
        tsnew=np.concatenate((x_test,y_test.reshape(-1,1)), axis=1)  
        trnew=np.concatenate((x_train,y_train.reshape(-1,1)), axis=1)

        os.makedirs(sep.join([path,'Model']), exist_ok=True)
        modeldev(trnew,batch_size,nb_epoch,model1_path=model_1_path,model2_path=model_2_path,model3_path=model_3_path)
        
        # os.makedirs(sep.join([deepxplore_results_path,'pred']), exist_ok=True)
        # os.makedirs(sep.join([deepxplore_results_path,'pred1']), exist_ok=True)
        
        try:
            M1_neurons,M1_neuron_covered,M2_neurons,M2_neuron_covered,M3_neurons,M3_neuron_covered= neuraoncov(data,ma,mb,load_weights_1_path=model_1_path,load_weights_2_path=model_2_path,load_weights_3_path=model_3_path,save_path=path)
            
            # print(len(tsi))
            # print(np.shape(tsi)[0])
            # tsim= np.array(tsi)
            tsim=np.zeros((np.shape(tsi)[0],np.shape(tsi)[2]))
            for k1 in range (0,np.shape(tsi)[0]):
                tsim[k1,:]=tsi[k1]
            # os.makedirs(sep.join([deepxplore_results_path,'tsdata']), exist_ok=True)
            fname='tsdata.csv'
            # out_file = open(sep.join([path,fname]), "w") 
            # json.dump(tsim, out_file, cls=NumpyEncoder) 
            # out_file.close()
            np.savetxt(sep.join([path,fname]),tsim, delimiter=',', fmt='%f')
            tsif=np.zeros((len(tsi),len(tsi[0][0])))
            for i in range(0,len(tsi)):
                tsif[i,:]=tsi[i][0]

            fname='tsi.csv'
            np.savetxt(sep.join([path,fname]),tsif, delimiter=',', fmt='%f')

            tsn=scaler.inverse_transform(tsif)

            fname='ntsi.csv'
            np.savetxt(sep.join([path,fname]),tsn, delimiter=',', fmt='%f')

            np.seterr(divide='ignore', invalid='ignore')
            # fname_path = parser.get('path','fname_')
            # data_path = "./Results/DeepXplore/sen_atis.csv"
            # data_path = './Results/DataDiagnostic/CleanedTextData/cleaned_complete_dataframe.csv'
            # input_column = 'Text'
            # target_column = 'Target'
            s1,s2=best5word(sen_embed_path, input_column, target_column, tsn);
            s1n=np.concatenate((s1,s2), axis=1)
            fname='bestword.csv'
            # out_file = open(sep.join([path,fname]), "w") 
            # json.dump(s1, out_file, cls=NumpyEncoder) 
            # out_file.close()
            # os.makedirs(sep.join([deepxplore_results_path,'bestword']), exist_ok=True)
            np.savetxt(sep.join([path,fname]),s1n, delimiter=',', fmt='%f')
            fname='neuron_cov.csv'
            # os.makedirs(sep.join([deepxplore_results_path,'neuron_cov']), exist_ok=True)
            d=[ ('Text','Value'),('M1_neurons', M1_neurons),('M1_neuron_covered',M1_neuron_covered),('M2_neurons', M2_neurons),('M2_neuron_covered',M2_neuron_covered),('M3_neurons', M3_neurons),('M3_neuron_covered',M3_neuron_covered)] #,('Avg_neuron_covered',Avg_neuron_covered)
            nc1=np.array(d)
            # headers = "M1_neurons,M1_neuron_covered,M2_neurons,M2_neuron_covered,M3_neurons,M3_neuron_covered,Avg_neuron_covered"
            # out_file = open(sep.join([path,fname]), "w") 
            # json.dump(nc1, out_file, cls=NumpyEncoder) 
            # out_file.close()
            np.savetxt(sep.join([path,fname]),nc1,fmt='%s',delimiter=',')
            data_path = parser.get('path','data_path')
            # fname='./Dataset/atis_intents.csv'
            # fname='C:/Users/lov/Documents/Amazon_review/AmazonReviews.csv'
            df = pd.read_csv(data_path,encoding='latin-1')
            corner_df = pd.DataFrame(data=None,columns=["text"])
            
            for k in s1:
                # print(k,"K")
                for idx in k:
                    corner_df.loc[len(corner_df.index)]= df['Text'][idx]
            # bestb=[]    
            # for i in range(0,np.shape(s1)[1]):
            #     b=[]
            #     for j in range(0,5):
            #         a=s1[j,i]
            #         b.append(df[input_column][a])
            #     bestb.append(b)

            # df1 = pd.DataFrame(bestb)
            fname = "corner_cases.csv"
            corner_df.to_csv(sep.join([path,fname]))
            # if i%10==0:
            #     fname='nc'+str(i)+'.csv'
            #     np.savetxt(sep.join(["Results", "DeepXplore_results",fname]),nc,delimiter=',', fmt='%f')
        except:
            print('Neurons was not covered!!')
        return None

    def execute_deepxplore():
        # parser
        parser = ConfigParser(interpolation=ExtendedInterpolation())
        parser.read('./Configs/config_deepxplore.properties')
        # parser1 = ConfigParser(interpolation=ExtendedInterpolation())
        # parser1.read('./Configs/config_data_diagnostic.properties')

        # mlflow config
        experiment_name = parser.get('mlflow', 'experiment_name')
        run_name = parser.get('mlflow', 'run_name')

        deepxplore_results_path = parser.get('save_path', 'deepxplore_results_path')
        batch_size = parser.getint('parameter','batch_size')
        nb_epoch = parser.getint('parameter','nb_epoch')

        # if parser1.getboolean('embedding','embedding.countvectoriser.execution') :
        #     # os.makedirs(deepxplore_results_path, exist_ok=True)
        #     cv_x = parser.get('path', 'cv_x_data')
        #     cv_y = parser.get('path', 'cv_y_data')
        #     cv_path = parser.get('save_path', 'cv_path')
        data_path = parser.get('path','data_path')
        input_column = parser.get('path','input_column')
        target_column = parser.get('path','target_column')
        model_1_path = parser.get('path', 'model_1_path')
        model_2_path = parser.get('path', 'model_2_path')
        model_3_path = parser.get('path', 'model_3_path')

        deepxplore_results(data_path,input_column,target_column,deepxplore_results_path,model_1_path,model_2_path,model_3_path,batch_size,nb_epoch)
        # else:
        #     print("Countvectoriser embeddings Not executed ")
        
        # if  parser1.getboolean('embedding','embedding.tfidfvectoriser.execution') :
        #     # os.makedirs(deepxplore_results_path, exist_ok=True)
        #     tfidf_x = parser.get('path', 'tfidf_x_data')
        #     tfidf_y = parser.get('path', 'tfidf_y_data')
        #     tfidf_path= parser.get('save_path', 'tfidf_path') 
        #     model_1_path = parser.get('path', 'model_1_tf_path')
        #     model_2_path = parser.get('path', 'model_2_tf_path')
        #     model_3_path = parser.get('path', 'model_3_tf_path')
      
        #     deepxplore_results(tfidf_x,tfidf_y,tfidf_path,deepxplore_results_path,model_1_path,model_2_path,model_3_path,batch_size,nb_epoch)
        # else:
        #     print("Tfidfvectoriser embeddings Not executed ")
        return None

    # execute_deepxplore()

    # def main_script():
    #     # Logger setup function
    #     # logger = utility.logger_setup('DeepXplore', 'DeepXplore.log')

    #     # logging directory
    #     os.makedirs('logging', exist_ok=True)
    #     logger = logging.getLogger(__name__)
    #     logger.setLevel(logging.DEBUG)
    #     formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s:%(filename)s:%(module)s')
    #     file_handler = logging.FileHandler('logging/Deepxplore.log')
    #     file_handler.setFormatter(formatter)

    #     logger.addHandler(file_handler)

    #     try:

    #         execute_deepxplore()
    #         logger.info("Deepxplore successfully executed.")
    #     except:
    #         logger.critical('Please check data and Models files in Deepxplore pipeline')
    #         logger.exception('Deepxplore Testing pipeline error!!')
    #         # sys.exit(1)
    #         PassError(Exception)

    # if __name__ == '__main__':
    #     main_script()

else:
    print("Invalid License")