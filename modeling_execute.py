import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import os
    import pickle
    from configparser import ConfigParser, ExtendedInterpolation
    import numpy as np
    from Modeling import model_metric_utility
    from Modeling import model_search
    from Utility import utility

    class PassError(Exception):
        """Base class for exceptions in this module."""
        pass

    class ModelingError(Exception):
        """ Raise an error when modeling_execute function failed to execute """

        # Initializer
        def __init__(self, msg):
            self.msg = msg

        def __str__(self):
            return repr(self.msg)


    class FileNotFound(Exception):
        """ Raise an error when embedding vector was not found """

        # Initializer
        def __init__(self, msg):
            self.msg = msg

        def __str__(self):
            return repr(self.msg)


    class Modeling:
        """
        This Class takes the modeling-modeling_parser and logger as input.
        This class first load the embedding vectors using config properties files and
        performs the model tuning task which means search for best model estimator and
        best parameters and also on which embedding vectors we have got best performance.

        After getting best estimator it saves the the model instances and Meta data dictionary to the path mentioned in
        modeling_parser.

        :param modeling_parser: Modeling stage ConfigParser
        :param logger: logger instance of Modeling
        """

        def __init__(self, modeling_parser, logger):
            self.modeling_parser = modeling_parser
            self.logger = logger

        def modeling_execute(self):
            """
            This function loads the embedding vector and performs the then find the best model
            using either random search or grid search mentioned in config properties file.
            And save the best fitted model and meta info of model.

            :return: None
            """
            # CountVectoriser Embeddings loading
            # if data_diagnostic_parser.getboolean('embedding','embedding.countvectoriser.execution'):
            count_vect_train_x = None
            try:  
                count_vect_path = self.modeling_parser.get('load_path', 'count_vect_embedding_load_path')

                with open(os.path.join(count_vect_path, 'count_vect_train_x.pkl'), 'rb') as f:
                    count_vect_train_x = pickle.load(f)

                with open(os.path.join(count_vect_path, 'count_vect_val_x.pkl'), 'rb') as f:
                    count_vect_val_x = pickle.load(f)

                # Encoded Target values loading
                with open(os.path.join(count_vect_path, 'train_y.pkl'), 'rb') as f:
                    train_y = pickle.load(f)

                with open(os.path.join(count_vect_path, 'val_y.pkl'), 'rb') as f:
                    val_y = pickle.load(f)

            except Exception as e:
                print("countvectoriser embedding path was not loaded")
                self.logger.critical(f'*** countvectoriser embedding path was not loaded *** \n{e}')
                PassError(e)

            # TfidfVectoriser Embeddings loading
            # if data_diagnostic_parser.getboolean('embedding','embedding.tfidfvectoriser.execution'):
            tfidf_vect_train_x = None
            try:
                tfidf_vect_path = self.modeling_parser.get('load_path', 'tfidf_vect_embedding_load_path')

                with open(os.path.join(tfidf_vect_path, 'tfidf_vect_train_x.pkl'), 'rb') as f:
                    tfidf_vect_train_x = pickle.load(f)
                with open(os.path.join(tfidf_vect_path, 'tfidf_vect_val_x.pkl'), 'rb') as f:
                    tfidf_vect_val_x = pickle.load(f)

                # Encoded Target values loading
                with open(os.path.join(tfidf_vect_path, 'train_y.pkl'), 'rb') as f:
                    train_y = pickle.load(f)

                with open(os.path.join(tfidf_vect_path, 'val_y.pkl'), 'rb') as f:
                    val_y = pickle.load(f)
                
            except Exception as e:
                print("Tfidf embedding path was not loaded")
                self.logger.critical(f'*** Tfidf embedding path was not loaded *** \n{e}')
                PassError(e)

            # Load Label Encoder instance to get class names
            label_encoder_instance_path = self.modeling_parser.get('load_path', 'data_diagnostic_metadata_save_path')

            try:
                with open(os.path.join(label_encoder_instance_path, 'label_encoder.pkl'), 'rb') as f:
                    label_encoder = pickle.load(f)
                    class_name = label_encoder.classes_
            except Exception as e:
                print("label_encoder file was not loaded")
                self.logger.critical(f'*** label_encoder file was not loaded *** \n{e}')
                PassError(e)

            # Tune the model
            try:
                model_tune_dict = model_search.model_tune(count_vect_train_x,tfidf_vect_train_x, train_y, self.modeling_parser)

                # Saving model tune dictionary as pickle file
                model_save_path = self.modeling_parser.get('save_path', 'model_save_path')
                utility.save_pickle_file(file=model_tune_dict, save_path=model_save_path, pickle_name='model_tune_dict.pkl')

                # Get the best model estimator and embedding name to refit the data on best estimator.
                maximum_score_index = np.argmax(model_tune_dict['model_score'])
                best_estimator_ = model_tune_dict['model_estimator'][maximum_score_index]
                embedding_name = model_tune_dict['embedding_name'][maximum_score_index]
                model_name = model_tune_dict['model_name'][maximum_score_index]
            except Exception as e:
                print("model_tune_dict error")
                self.logger.critical(f'*** model_tune_dict error *** \n{e}')
                PassError(e)

            # Retraining the model using best estimator and optimal embedding vectors.
            try:
                if embedding_name == 'count_vectoriser':
                    model = best_estimator_
                    model.fit(count_vect_train_x, train_y)
                    utility.save_pickle_file(file=model, save_path=model_save_path, pickle_name='model.pkl')

                elif embedding_name == 'tfidf_vectoriser':
                    model = best_estimator_
                    model.fit(tfidf_vect_train_x, train_y)
                    utility.save_pickle_file(file=model, save_path=model_save_path, pickle_name='model.pkl')
            except Exception as e:
                print("\n *** Please fix model_execute function *** ")
                self.logger.critical(f'*** Please fix model_execute function *** \n{e}')
                PassError(e)

            # Load the best estimated trained model
            try:
                with open(os.path.join(model_save_path, 'model.pkl'), 'rb') as f:
                    loaded_model = pickle.load(f)

            except Exception as e:
                print("\n *** Model pickle file not found *** ")
                self.logger.critical(f'*** Model pickle file not found *** \n{e}')
                raise FileNotFound(e)
            try:
                predicted_values_train = None
                predicted_values_val = None
                if embedding_name == 'count_vectoriser':
                    predicted_values_train = loaded_model.predict(count_vect_train_x)
                    predicted_values_val = loaded_model.predict(count_vect_val_x)

                elif embedding_name == 'tfidf_vectoriser':
                    predicted_values_train = loaded_model.predict(tfidf_vect_train_x)
                    predicted_values_val = loaded_model.predict(tfidf_vect_val_x)

                if not [item for item in (predicted_values_train, predicted_values_val) if item is None]:
                    # print(val_y.shape,predicted_values_val.shape)
                    score_train, score_val = model_metric_utility.compute_train_val_accuracy(train_y,
                                                                                             predicted_values_train,
                                                                                             val_y,
                                                                                             predicted_values_val)
                    classification_report_val = model_metric_utility.compute_classification_report(val_y, predicted_values_val)
                    confusion_matrix_fig = model_metric_utility.plot_confusion_matrix(val_y, predicted_values_val, class_name)
                    utility.save_plot(confusion_matrix_fig, model_save_path, 'confusion_matrix.png')

                    # Meta info of best trained model.
                    best_model_meta_info = {'embedding_name': embedding_name, 'model_name': model_name,
                                            'best_estimator': best_estimator_, 'train_score': score_train,
                                            'val_score': score_val,
                                            'val_classification_report': classification_report_val}
                    utility.save_pickle_file(file=best_model_meta_info, save_path=model_save_path,
                                             pickle_name='best_estimator_dict.pkl')
            except Exception as e:
                print("\n *** Please fix model_execute function *** ")
                self.logger.critical("\n *** Please fix model_execute function *** ")
                self.logger.critical("\n predicted_values_train or predicted_values_val None values")
                raise ModelingError("predicted_values_train or predicted_values_val None values")
                PassError(e)

            return None
else:
    print("Invalid License")

