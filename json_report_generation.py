import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
    import json
    import os
    import pickle
    from configparser import ConfigParser, ExtendedInterpolation
    import numpy as np
    import pandas as pd
    from Utility import utility
    parser = ConfigParser(interpolation=ExtendedInterpolation())
    parser.read('./Configs/config_main.properties')


    json_report = dict()

    sep = '/'

    class PassError(Exception):
        """Base class for exceptions in this module."""
        pass

    # stage 1 = "Data Diagnostic"
    # stage 2 = "Modeling"
    # stage 3 = "Global Explainability"
    # stage 4 = "Local Explainability"
    # stage 5 = "Fairml Bias"
    # stage 6 = "Deepxplore"
    # stage 7 = "Performance Testing"
    # stage 8 = "Attack Vectors"
    # stage 9 = "Augmentation"
    # stage 10 = "Counterfactuals"
    # stage 11 = "Feedback Loop"

    def liner():
        
        one_liner_global_exp = "The explanation of the learning algorithm as a whole, including the training data used, appropriate uses of the algorithms, and warnings regarding weaknesses of the algorithm and inappropriate uses."
        one_liner_sklearn_implicit = "The permutation feature importance is defined to be the decrease in a model score when a single feature value is randomly shuffled 1. This procedure breaks the relationship between the feature and the target, thus the drop in the model score is indicative of how much the model depends on the feature."
        one_liner_deepxplore = "The first white-box framework for systematically testing real-world DL systems.We introduced neuron coverage for measuring the parts of a DL system exercised by test inputs."
        one_liner_shap = "The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction."
        one_liner_performance_testing = "It is the practice of evaluating how a system performs in terms of responsiveness and stability under a particular workload."
        one_liner_soak_test = "It is used to measure performance of a software application under a volume of load for an extended period of time. Soak test is done to determine whether a system can sustain continuous load for a longer duration of time."
        one_liner_stress_test = "It is a type of software testing which determines the performance of a system, software product or software application under real life based load conditions. It also calculates model latency such as minimum,average,maximum response time etc."

        one_liner_text_attacks = "An attack vector is a pathway or method used by a hacker to illegally access a network or computer in an attempt to exploit system vulnerabilities."
        one_liner_bae = "This technique replaces and inserts tokens in the original text by masking a portion of the text and leveraging the BERT-MLM to generate alternatives for the masked tokens."
        one_liner_deepwordbug = "This technique employs novel scoring strategies to identify the critical tokens that, if modified, cause the classifier to make an incorrect prediction."
        one_liner_alzantot = "This attack aims to minimize the number of modified words between the original and adversarial examples, but only perform modifications which retain semantic similarity with the original and syntactic coherence."
        one_liner_inputreduction = "This attack iteratively removes the least important word from the input."
        one_liner_pruthi = "This attack we focus on adversarially-chosen spelling mistakes in the context of text classification, addressing the following attack types: dropping, adding, and swapping internal characters within words."
        one_liner_pwws = "This attack is called Probability Weighted Word Saliency (PWWS) for generating adversarial examples on text classification tasks. PWWS introduces a new word substitution order determined by the word saliency and weighted by the classification probability with synonyms substitution strategy."
        one_liner_textfooler = "This attack is effective as it outperforms previous attacks by success rate and perturbation rate, also utility-preserving—it preserves semantic content, grammaticality, and correct types classified by humans, and efficient—it generates adversarial text with computational complexity linear to the text length."
        one_liner_iga = "This attack is synonym substitution based adversarial attack that aims to satisfy all the lexical, grammatical, semantic constraints which are hard to perceived by humans."

        one_liner_augmentation = "Data Augmentation technique is a process that enables us to artificially increase training data size by generating different versions of real datasets without actually collecting the data."
        one_liner_aug_character = "Change in the data within an individual word. For eg: Let's say you make any typing mistake. 'Hello' could be miss spelled as 'hwllo' "
        one_liner_aug_word = "Change in the complete word in a sentence. For eg: You may forget to type something in a sentence. 'I am good' could be mistakenly written as 'I good' "
        one_liner_aug_sentence = "Change in the entire sentence. For eg: Addition of extra sentences in the end of previous sentence."
        one_liner_counterfactuals = "Text Counterfactuals which acts as test-cases to evaluate the properties such as fairness and robustness of these systems. These texts along with its correct labels may also be used to augment the training set to mitigate such properties"

        return {
            "one_liner_global_exp": one_liner_global_exp,
            "one_liner_sklearn_implicit": one_liner_sklearn_implicit,
            "one_liner_deepxplore": one_liner_deepxplore,
            "one_liner_shap": one_liner_shap,

            "one_liner_performance_testing": one_liner_performance_testing,
            "one_liner_soak_test": one_liner_soak_test,
            "one_liner_stress_test": one_liner_stress_test,

            "one_liner_text_attacks": one_liner_text_attacks,
            "one_liner_bae": one_liner_bae,
            "one_liner_deepwordbug": one_liner_deepwordbug,
            "one_liner_alzantot": one_liner_alzantot,
            "one_liner_inputreduction": one_liner_inputreduction,
            "one_liner_pruthi": one_liner_pruthi,
            "one_liner_pwws": one_liner_pwws,
            "one_liner_textfooler": one_liner_textfooler,
            "one_liner_iga": one_liner_iga,

            "one_liner_augmentation": one_liner_augmentation,
            "one_liner_augmented_char": one_liner_aug_character,
            "one_liner_augmented_word": one_liner_aug_word,
            "one_liner_augmented_sentence": one_liner_aug_sentence,
            "one_liner_counterfactuals": one_liner_counterfactuals
        }

    # Data Diagnostic Stage
    def stage1():
        # config loading parser
        data_diagnostic_parser = ConfigParser(interpolation=ExtendedInterpolation())
        data_diagnostic_parser.read('./Configs/config_data_diagnostic.properties')

        # loading all meta info
        with open(sep.join([data_diagnostic_parser.get('save_path', 'meta_data_save_path'),
                            'meta_info_data_diagnostic.pkl']), 'rb') as f:
            data_diagnostic_meta_info = pickle.load(f)
        class_dist = {}
        if data_diagnostic_parser.getboolean('plots', 'class_distribution_plot'):

            class_dist_plot_path = sep.join([data_diagnostic_parser.get('save_path', 'meta_data_save_path'),
                                         'class_distribution_plot.png']).replace("Results/", '')
            d1= {'img_path': class_dist_plot_path}
            class_dist.update(d1)

        # Train Validation Test Ratio
        train_ratio = data_diagnostic_parser.getfloat('train_val_test_split', 'train_set_size')
        val_ratio = data_diagnostic_parser.getfloat('train_val_test_split', 'validation_set_size')
        test_ratio = data_diagnostic_parser.getfloat('train_val_test_split', 'test_set_size')

        # Load head of the data
        csv_head = pd.read_csv(
            sep.join([data_diagnostic_parser.get('save_path', 'meta_data_save_path'), 'sample_text_dataframe.csv'])).drop(
            'Unnamed: 0', axis=1)

        # Plots
        meta_data_save_path = data_diagnostic_parser.get('save_path', 'meta_data_save_path').replace("Results/", '')
        plt_dict ={}
        
        if data_diagnostic_parser.getboolean('plots', 'word_cloud_plot'):

            word_cloud_plots = [sep.join([meta_data_save_path, 'word_cloud_after_preprocessing.png']),
                                sep.join([meta_data_save_path, 'word_cloud_before_preprocessing.png'])]
            d1={'Word Cloud': {'img_path': word_cloud_plots}}
            plt_dict.update(d1)
        if data_diagnostic_parser.getboolean('plots', 'word_count_plot'):

            word_counts_plots = [sep.join([meta_data_save_path, 'word_count_after_preprocessing.png']),
                                sep.join([meta_data_save_path, 'word_count_before_preprocessing.png'])]
            d2={'Word Count Distribution': {'img_path': word_counts_plots}}
            plt_dict.update(d2)
        if data_diagnostic_parser.getboolean('plots', 'word_frequency_plot'):

            unigram_plots = [sep.join([meta_data_save_path, 'word_frequency_unigram_after_preprocessing.png']),
                             sep.join([meta_data_save_path, 'word_frequency_unigram_before_preprocessing.png'])]
            d3={'Word Frequency Unigram': {'img_path': unigram_plots}}
            plt_dict.update(d3)
            bigram_plots = [sep.join([meta_data_save_path, 'word_frequency_bigram_after_preprocessing.png']),
                            sep.join([meta_data_save_path, 'word_frequency_bigram_before_preprocessing.png'])]
            d4={'Word Frequency Bigram': {'img_path': bigram_plots}}
            plt_dict.update(d4)
        json_report["Data Diagnostic"] = {
        'Class Distribution':class_dist,
            'Train Validation Test': {
                'Train Validation Test Size': data_diagnostic_meta_info['train_val_test_split'],
            },

            'Outlier Duplicate Meta Info': {
                'Duplicate': data_diagnostic_meta_info['duplicate'],
                'Outlier': data_diagnostic_meta_info['outlier']
            },

            'Head of Text Data': csv_head.to_dict(orient='records'),

            "Meta Plots": plt_dict

        }

        return None

    # Modeling Stage
    def stage2():
        # config loading parser
        modeling_parser = ConfigParser(interpolation=ExtendedInterpolation())
        modeling_parser.read('./Configs/config_modeling.properties')
        # print((modeling_parser.sections()))

        model_save_path = modeling_parser.get('save_path', 'model_save_path')

        # loading all meta info
        with open(sep.join([model_save_path, 'best_estimator_dict.pkl']), 'rb') as f:
            best_estimator_dict = pickle.load(f)

        # print('best_estimator_dict', best_estimator_dict)
        # meta data
        performance_metric = modeling_parser.get('modeling', 'parameters.performance_metric')
        train_score = np.round(best_estimator_dict['train_score'], 3)
        val_score = np.round(best_estimator_dict['val_score'], 3)

        # Plots
        Confusion_matrix_plot_path = sep.join([model_save_path, 'confusion_matrix.png']).replace("Results/", '')

        json_report["Modeling"] = {
            'Performance Metrics': performance_metric,
            'Train Score': train_score,
            'Test Score': val_score,
            'Confusion Matrix': {'img_path': [Confusion_matrix_plot_path]}

        }
        return None

    # Explainability Stage
    # Global Explainability
    def stage3():
        # config loading parser
        model_explainability_parser = ConfigParser(interpolation=ExtendedInterpolation())
        model_explainability_parser.read('./Configs/config_model_explainability.properties')
        # print((model_explainability_parser.sections()))

        # Global XAI plot paths
        global_explainability_save = model_explainability_parser.get('save_path', 'global_explainability_save_path')
        global_explainability_save_path= global_explainability_save.replace("Results/", '')
        # Sklearn Implicit Plots
        global_exp_dict={}

        if model_explainability_parser.getboolean('global_explainability', 'execute.explainability.sklearn_implicit'):

            try:
                sklearn_implicit_plots = list()
                for image_path in os.listdir(sep.join([global_explainability_save, 'SklearnFeatureImportance'])):
                    sklearn_implicit_plots.append(
                        sep.join([global_explainability_save_path, 'SklearnFeatureImportance', image_path]))
                d={'Sklearn Implicit': {'img_path': sklearn_implicit_plots}}
                global_exp_dict.update(d)
            except:
                global_exp_dict
                # print("Sklearn Feature Importance path not specified")

            try:
                forest_importance_plots = list()
                for image_path in os.listdir(sep.join([global_explainability_save, 'RandomforestFeatureImportance'])):
                    forest_importance_plots.append(sep.join([global_explainability_save_path, 'RandomforestFeatureImportance', image_path]))
                d1={'Random forest Feature Importance': {'img_path': forest_importance_plots}}
                global_exp_dict.update(d1)
            except:
                global_exp_dict
                # print("Random forest Feature Importance path not specified")
        # Shap Plot
        if model_explainability_parser.getboolean('global_explainability', 'execute.explainability.shap_global'):

            try:
                shap_plots = list()
                for image_path in os.listdir(sep.join([global_explainability_save, 'ShapGlobal'])):
                    shap_plots.append(sep.join([global_explainability_save_path, 'ShapGlobal', image_path]))
                d2={'SHAP Global': {'img_path': shap_plots}}
                global_exp_dict.update(d2)
            except:
                global_exp_dict
                # print("SHAP Global path not specified")
        # Decision Tree Surrogate Plot
        if model_explainability_parser.getboolean('global_explainability', 'execute.explainability.decision_tree_surrogate'):

            try:
                decision_tree_surrogate = list()
                for image_path in os.listdir(sep.join([global_explainability_save, 'DecisionTreeSurrogate'])):
                    decision_tree_surrogate.append(sep.join([global_explainability_save_path, 'DecisionTreeSurrogate', image_path]))
                d3={'Decision Tree Surrogate': {'img_path': decision_tree_surrogate}}
                global_exp_dict.update(d3)
            except:
                print("Decision Tree Surrogate path not specified")

        json_report["Global Explainability"] = global_exp_dict
        # {
        #     'Global Explainability': global_exp_dict,
        #     # {
        #     #     'Sklearn Implicit': {'img_path': sklearn_implicit_plots},
        #     #     'SHAP Global': {'img_path': shap_plots},
        #     #     'Decision Tree Surrogate': {'img_path': decision_tree_surrogate}
        #     # },    
        # }
        return None

    #Local explainability
    def stage4():
        # config loading parser
        model_explainability_parser = ConfigParser(interpolation=ExtendedInterpolation())
        model_explainability_parser.read('./Configs/config_model_explainability.properties')
        # print((model_explainability_parser.sections()))
        # Local XAI plot paths
        local_explainability_save = model_explainability_parser.get('save_path', 'local_explainability_save_path')
        local_explainability_save_path = local_explainability_save.replace("Results/", '')

        # Lime Plot
        loc_exp_dict={}
        if model_explainability_parser.getboolean('local_explainability', 'execute.explainability.lime_exp'):
            
            try:
                lime_plots = list()
                for image_path in os.listdir(sep.join([local_explainability_save, 'LimeExplanation','FalsePositive'])):
                    lime_plots.append(sep.join([local_explainability_save_path, 'LimeExplanation','FalsePositive', image_path]))
                # d1 = {'Lime': {'FalsePositive': lime_plots}}
                # loc_exp_dict.update(d1)
                for image_path in os.listdir(sep.join([local_explainability_save, 'LimeExplanation','FalseNegative'])):
                    lime_plots.append(sep.join([local_explainability_save_path, 'LimeExplanation','FalseNegative', image_path]))
                # d1 = {'Lime':{'FalsePositive': lime_plots_fp,'FalseNegative': lime_plots_fn}}
                d1 = {'Lime':{"img_path":lime_plots}}
                loc_exp_dict.update(d1)
            except:
                loc_exp_dict
            # try:
            #     lime_plots_fp = list()
            #     for image_path in os.listdir(sep.join([local_explainability_save, 'LimeExplanation','FalsePositive'])):
            #         lime_plots_fp.append(sep.join([local_explainability_save_path, 'LimeExplanation','FalsePositive', image_path]))
            #     # d1 = {'Lime': {'FalsePositive': lime_plots}}
            #     # loc_exp_dict.update(d1)

            #     lime_plots_fn = list()
            #     for image_path in os.listdir(sep.join([local_explainability_save, 'LimeExplanation','FalseNegative'])):
            #         lime_plots_fn.append(sep.join([local_explainability_save_path, 'LimeExplanation','FalseNegative', image_path]))
            #     # d1 = {'Lime':{'FalsePositive': lime_plots_fp,'FalseNegative': lime_plots_fn}}
            #     d1 = {'Lime':{'FalsePositive': {"img_path":lime_plots_fp},'FalseNegative': {"img_path":lime_plots_fn}}}
            #     loc_exp_dict.update(d1)
            # except:
            #     loc_exp_dict
                # print("Lime path not specified")
        # Shap Plot
        if model_explainability_parser.getboolean('local_explainability', 'execute.explainability.shap_local'):

            try:
                shap_local_plot = list()
                # shap_local_plot_save_path = sep.join([local_explainability_save, 'Shaplocal'])
                # shap_local_fn_plot_save_path = os.path.join(shap_local_plot_save_path,'FalseNegative')
                for image_path in os.listdir(sep.join([local_explainability_save, 'Shaplocal','FalsePositive'])):
                    shap_local_plot.append(sep.join([local_explainability_save_path, 'Shaplocal','FalsePositive', image_path]))
                # d1 = {'SHAP Local': {'FalsePositive': shap_local_plot_fp}}
                # loc_exp_dict.update(d1)

                # shap_local_plot_fn = list()
                for image_path in os.listdir(sep.join([local_explainability_save, 'Shaplocal','FalseNegative'])):
                    shap_local_plot.append(sep.join([local_explainability_save_path, 'Shaplocal','FalseNegative', image_path]))
                d2 = {'SHAP Local':{"img_path":shap_local_plot}}
                # d2 = {'SHAP Local':{'FalsePositive': shap_local_plot_fp,'FalseNegative': shap_local_plot_fn}}
                loc_exp_dict.update(d2)
            except:
                loc_exp_dict
                # print("SHAP Local path not specified")

        # # eli5 dataframe
        # if model_explainability_parser.getboolean('local_explainability', 'execute.explainability.eli5_exp'):

        #     try:
        #         eli5_pred_fp = list()

        #         for data_path in os.listdir(sep.join([local_explainability_save, 'eli5','FalsePositive'])):
        #             eli5_pred_fp.append(sep.join([local_explainability_save_path, 'eli5','FalsePositive', data_path]))
        #         # d1 = {'Eli5 prediction':{'FalsePositive':eli5_pred}}
        #         # loc_exp_dict.update(d1)

        #         eli5_pred_fn = list()
        #         for data_path in os.listdir(sep.join([local_explainability_save, 'eli5','FalseNegative'])):
        #             eli5_pred_fn.append(sep.join([local_explainability_save_path, 'eli5','FalseNegative', data_path]))
        #         d3 = {'eli5_pred':{'FalsePositive': {"data_path":eli5_pred_fp},'FalseNegative': {"data_path":eli5_pred_fn}}}
        #         # d3 = {'eli5_pred':{'FalsePositive': eli5_pred_fp,'FalseNegative': eli5_pred_fn}}

        #         loc_exp_dict.update(d3)
        #     except:
        #         print("Eli5 prediction path not specified")
        

        json_report["Local Explainability"] = loc_exp_dict
        # {
        #     'Local Explainability':loc_exp_dict
        #     # 'Local Explainability': {
        #     #     'Lime': {'img_path': lime_plots},
        #     #     'SHAP Local': {'img_path': shap_local_plots},
        #     #     'Eli5 prediction':{'data_path':eli5_pred}
        #     # },    
        # }
        return None

    # Bias techinque
    def stage5():
        # config loading parser
        model_explainability_parser = ConfigParser(interpolation=ExtendedInterpolation())
        model_explainability_parser.read('./Configs/config_model_explainability.properties')
        # eli5 paths
        eli5_dict={}
        # eli5 dataframe
        if model_explainability_parser.getboolean('bias', 'execute.explainability.eli5_exp'):

            eli5_save = model_explainability_parser.get('save_path', 'bias_save_path')
            eli5_save_path = eli5_save.replace("Results/", '')
            eli_fp_path = sep.join([eli5_save, 'eli5','FalsePositive'])
            eli_fn_path = sep.join([eli5_save, 'eli5','FalseNegative'])
            
            try:
                for data_path in os.listdir(eli_fp_path):
                    eli_fp_head = pd.read_csv(sep.join([eli_fp_path,data_path])).drop('Unnamed: 0', axis=1)
                d1 = {'Feature_weights':eli_fp_head.to_dict(orient='records')}
                eli5_dict.update(d1)
                # for data_path in os.listdir(eli_fn_path):
                #     eli_fn_head = pd.read_csv(sep.join([eli_fn_path,data_path])).drop('Unnamed: 0', axis=1)
                # d2 = {'FalseNegative':eli_fn_head.to_dict(orient='records')}
                # eli5_dict.update(d2)
            except:
                eli5_dict
            # print(eli5_dict)
            # try:
            #     eli5_pred_fp = list()

            #     for data_path in os.listdir(sep.join([local_explainability_save, 'eli5','FalsePositive'])):
            #         eli5_pred_fp.append(sep.join([local_explainability_save_path, 'eli5','FalsePositive', data_path]))
            #     # d1 = {'Eli5 prediction':{'FalsePositive':eli5_pred}}
            #     # loc_exp_dict.update(d1)

            #     eli5_pred_fn = list()
            #     for data_path in os.listdir(sep.join([local_explainability_save, 'eli5','FalseNegative'])):
            #         eli5_pred_fn.append(sep.join([local_explainability_save_path, 'eli5','FalseNegative', data_path]))
            #     d3 = {'eli5_pred':{'FalsePositive': {"data_path":eli5_pred_fp},'FalseNegative': {"data_path":eli5_pred_fn}}}
            #     # d3 = {'eli5_pred':{'FalsePositive': eli5_pred_fp,'FalseNegative': eli5_pred_fn}}
            #     loc_exp_dict.update(d3)
            # except:
            #     print("Eli5 prediction path not specified")
        # Fairml plot paths
        fairml_dict={}
        if model_explainability_parser.getboolean('bias', 'execute.explainability.fairml'):

            fairml_plot_save = model_explainability_parser.get('save_path', 'bias_save_path')
            fairml_plot_save_path = fairml_plot_save.replace("Results/", '')

            # fairml featue dependence Plot
            try:
                fairml_plot = list()
                for image_path in os.listdir(fairml_plot_save):
                    if image_path.endswith(".png"):
                        fairml_plot.append(sep.join([fairml_plot_save_path,image_path]))
                fairml_dict = {'img_path': fairml_plot}
            except:
                fairml_dict
                # print("Fairml bias path not specified")
        json_report["Bias Techniques"] ={
            'FairML': fairml_dict,
            'ELI_5':eli5_dict
        }
        return None
        
    # deep explore stage
    def stage6():
        # config loading parser
        deep_xplore_parser = ConfigParser(interpolation=ExtendedInterpolation())
        deep_xplore_parser.read('./Configs/config_deepxplore.properties')
        # data_diagnostic_parser = ConfigParser(interpolation=ExtendedInterpolation())
        # data_diagnostic_parser.read('./Configs/config_data_diagnostic.properties')

        # deep_xplore paths
        corner_dic = {}
        save_path = deep_xplore_parser.get('save_path', 'deepxplore_results_path').replace("Results/", '')

        try:
            d1 = {'bestword': {'data_path': sep.join([save_path, 'bestword.csv'])}}
            corner_dic.update(d1)
        except:
            corner_dic
        try:
            d2 = {'cornercases': {'data_path': sep.join([save_path, 'corner_cases.csv'])}}
            corner_dic.update(d2)
        except:
            corner_dic
        try:
            path = deep_xplore_parser.get('save_path', 'deepxplore_results_path')
            neuron_cov = pd.read_csv(sep.join([path, 'neuron_cov.csv']))
            d3 = {'neurons_coverage': neuron_cov.to_dict(orient='records')}
            corner_dic.update(d3)
        except:
            corner_dic
            
        #     # label_diff = pd.read_json(sep.join([cv_path, 'label_diff.json']))
        #     # d4 = {'label_diff': {'data_path': label_diff.to_dict(orient='records')}}
        #     # cv.update(d4)
            
        #     # proj = pd.read_json(sep.join([cv_path, 'proj.json']))
        #     # d5 = {'proj': {'data_path': proj.to_dict(orient='records')}}
        #     # cv.update(d5)
        #     try:
        #         # bestword = pd.read_json(sep.join([cv_path, 'bestword.json']))
        #         d6 = {'bestword': {'data_path': sep.join([cv_path, 'bestword.csv'])}}
        #         cv.update(d6)
        #     except:
        #         cv
        #     try:
        #         path = deep_xplore_parser.get('path', 'deepxplore_results_path')
        #         neuron_cov_path = sep.join([path, 'CountVectoriser'])
        #         neuron_cov = pd.read_csv(sep.join([neuron_cov_path, 'neuron_cov.csv']))
        #         d7 = {'neuron_cov': neuron_cov.to_dict(orient='records')}
        #         cv.update(d7)
        #     except:
        #         cv
        # # tfidf results
        # tf = {}
        # if data_diagnostic_parser.getboolean('embedding', 'embedding.tfidfvectoriser.execution'):

        #     # TfidfVectoriser = list()
        #     # for data_path in os.listdir(sep.join([deep_xplore_save_path, 'TfidfVectoriser'])):
        #     #     TfidfVectoriser.append(sep.join([deep_xplore_save_path, 'TfidfVectoriser', data_path]))
        #     # tf = {'TfidfVectoriser': {'data_path': TfidfVectoriser}}
        #     tf_path = sep.join([deep_xplore_save_path, 'TfidfVectoriser'])
            
        #     try:
        #         # pred = pd.read_csv(sep.join([tf_path, 'pred.csv']))
        #         d1 = {'pred': {'data_path': sep.join([tf_path, 'pred.csv'])}}
        #         tf.update(d1)
        #     except:
        #         tf
        #     try:
        #         # pred_prob= pd.read_json(sep.join([tf_path, 'pred_prob.json']))
        #         d2 = {'pred_prob': {'data_path': sep.join([tf_path, 'pred_prob.csv'])}}
        #         tf.update(d2)
        #     except:
        #         tf
        #     try:   
        #         # tsdata = pd.read_json(sep.join([tf_path, 'tsdata.json']))
        #         d3 = {'tsdata': {'data_path': sep.join([tf_path, 'tsdata.csv'])}}
        #         tf.update(d3)
        #     except:
        #         tf
        #         # label_diff = pd.read_json(sep.join([tf_path, 'label_diff.json']))
        #         # d4 = {'label_diff': {'data_path': label_diff.to_dict(orient='records')}}
        #         # tf.update(d4)
                
        #         # proj = pd.read_json(sep.join([tf_path, 'proj.json']))
        #         # d5 = {'proj': {'data_path': proj.to_dict(orient='records')}}
        #         # tf.update(d5)
        #     try:   
        #         # bestword = pd.read_json(sep.join([tf_path, 'bestword.json']))
        #         d6 = {'bestword': {'data_path': sep.join([tf_path, 'bestword.csv'])}}
        #         tf.update(d6)
        #     except:
        #         tf
        #     try:
        #         path = deep_xplore_parser.get('path', 'deepxplore_results_path')
        #         neuron_cov_path = sep.join([path, 'TfidfVectoriser'])
        #         neuron_cov = pd.read_csv(sep.join([neuron_cov_path, 'neuron_cov.csv']))
        #         d7 = {'neuron_cov': neuron_cov.to_dict(orient='records')}
        #         tf.update(d7)
        #     except:
        #         tf

        json_report["White Box Testing"] ={
        "one_liner" : liner()['one_liner_deepxplore'],
        "White Box Corner case": corner_dic

        # {
        #         'CountVectoriser': {'data_path': CountVectoriser}
        #         },
        # 'TfidfVectoriser': tf
        # {
        #         'TfidfVectoriser': {'data_path': TfidfVectoriser}
        #         }
        }
        return None

    # Performance Testing report
    def stage7():
        # config Performance Testing parser
        parser_performance = ConfigParser(interpolation=ExtendedInterpolation())
        parser_performance.read('./Configs/config_performance_testing.properties')

        # save_path = parser_performance.get('model_privacy_results', 'save_path')

        # print(parser_performance.sections())
        try:
            path= parser_performance.get('stress_test_parameter', 'folder')
            save_path= path.replace("Results/", '')
            file_images= []
            for file in os.listdir(path):
                if file.endswith('.png'):
                    file_images.append(sep.join([save_path, file]))
            with open(parser_performance.get("stress_test_parameter", "stat_path"),'rb') as path1:
              stress_data = json.load(path1)
            with open(parser_performance.get("stress_test_parameter", "stat_failure_path"),'rb') as path2:
              stress_failure_data = json.load(path2)
            with open(parser_performance.get("soak_test_parameter", "stat_path"),'rb') as path3:
              soak_data = json.load(path3)
            with open(parser_performance.get("soak_test_parameter", "stat_failure_path"),'rb') as path4:
              soak_failure_data = json.load(path4)

            json_report["Performance Testing"] = {

                "one_liner": liner()['one_liner_performance_testing'],
                
                "Plots":{"Users":{"img_path":[file_images[0]]},"Response_Time":{"img_path":[file_images[1]]},"Total_Requests":{"img_path":[file_images[2]]}},
                
                # "Plots":{"img_path":{"Users":file_images[0],"Response_Time":file_images[1],"Total_Requests":file_images[2]}},

                # "one_liner_soak_test": liner()['one_liner_soak_test'],

                "Soak Test": {"one_liner":liner()['one_liner_soak_test'],"soak_test":soak_data},

                "Soak Test Failures": soak_failure_data,

                "Stress Test": {"one_liner":liner()['one_liner_stress_test'],"stress_test":stress_data},

                "Stress Test Failures": stress_failure_data,        
            }
        except:
            None
        return None

    # Text Attacks
    def stage8():
        attackvectors_parser = ConfigParser(interpolation=ExtendedInterpolation())
        attackvectors_parser.read('./configs/config_attack_vectors.properties')
        path_cv = attackvectors_parser.get('save_path', 'attack_results_cv_csv_path')
        path_tfidf = attackvectors_parser.get('save_path', 'attack_results_tfidf_csv_path')
        path_list = [path_cv,path_tfidf]
        # attack_list_cv = []
        # attack_list_tf = []
        cv_attack_list = {}
        tf_attack_list = {}
        for file in os.listdir(path_cv):
            attack_name = file.split(".",1)[0]
            df = pd.read_csv(path_cv+file)
            df_1 = df[['original_text','perturbed_text']].loc[(df['result_type'] == 'Successful') | (df['result_type'] =='Maximized')]
            # d = {"one_liner": liner()['one_liner_vanishing'],'img_path':files}
            x = df_1.head().to_dict(orient='records')
            # print(x)
            d = {attack_name:{"one_liner": liner()['one_liner_'+str(attack_name)],"data":x}}
            # print(d)
            # attack_list_cv.append({attack_name:df_1.head().to_dict(orient='records')})
            cv_attack_list.update(d)
            # cv_attack_list.update(attack_list_cv)
        # print(cv_attack_list)
        for file in os.listdir(path_tfidf):
            # attack_type.append(file.split(".",1)[0])
            attack_name = file.split(".",1)[0]
            # print(attack_type)
            df = pd.read_csv(path_cv+file)
            df_1 = df[['original_text','perturbed_text']].loc[(df['result_type'] == 'Successful') | (df['result_type'] =='Maximized')]
            x = df_1.head().to_dict(orient='records')
            d = {attack_name:{"one_liner": liner()['one_liner_'+str(attack_name)],"data":x}}
            tf_attack_list.update(d)
            # attack_list_tf.append({attack_name:df_1.head().to_dict(orient='records')})

        json_report["TextAttacks"] = {
        "one_liner": liner()['one_liner_text_attacks'],
        # "one_liner": liner()['one_liner_bae'],
        "CountVectoriser": cv_attack_list,
        "TfidfVectoriser": tf_attack_list
        }
        return None

    #Augmentation
    def stage9():
        nlpaug_parser = ConfigParser(interpolation=ExtendedInterpolation())
        nlpaug_parser.read('./configs/config_augmentation.properties')
        path_results_sent = nlpaug_parser.get('save_path', 'aug_results_sent')
        path_results_word = nlpaug_parser.get('save_path', 'aug_results_word')
        path_results_char = nlpaug_parser.get('save_path','aug_results_char')
        flag_sent = nlpaug_parser.getboolean('augment_type', 'parameters.sentences')
        flag_word = nlpaug_parser.getboolean('augment_type', 'parameters.words')
        flag_char = nlpaug_parser.getboolean('augment_type','parameters.characters')
        list_path = [path_results_sent,path_results_word,path_results_char]
        char_aug_list = {}
        sen_aug_list = {}
        word_aug_list = {}

        if flag_char:
            for file in os.listdir(path_results_char):
                char_aug_name = file.split(".",1)[0]
                df = pd.read_csv(path_results_char+'/'+file).drop('Unnamed: 0', axis=1)
                x = df.head().to_dict(orient='records')
                # d = {char_aug_name:{"one_liner": liner()['one_liner_'+str(char_aug_name)],"data":x}}
                d = {"one_liner": liner()['one_liner_'+str(char_aug_name)],"data":x}
                char_aug_list.update(d)

        if flag_sent:
            for file in os.listdir(path_results_sent):
                sent_aug_name = file.split(".",1)[0]
                df = pd.read_csv(path_results_sent+'/'+file).drop('Unnamed: 0', axis=1)
                x = df.head().to_dict(orient='records')
                d = {"one_liner": liner()['one_liner_'+str(sent_aug_name)],"data":x}
                sen_aug_list.update(d)

        if flag_word:
            for file in os.listdir(path_results_word):
                word_aug_name = file.split(".",1)[0]
                df = pd.read_csv(path_results_word+'/'+file).drop('Unnamed: 0', axis=1)
                x = df.head().to_dict(orient='records')
                d = {"one_liner": liner()['one_liner_'+str(word_aug_name)],"data":x}
                word_aug_list.update(d)

        json_report["Augmentation"] = {
        "one_liner": liner()['one_liner_augmentation'],
        "Character_Based": char_aug_list,
        "Sentence_Based": sen_aug_list,
        "Word_Based": word_aug_list
        }
        return None

    # Counterfactuals
    def stage10():

        counterfact_parser = ConfigParser(interpolation=ExtendedInterpolation())
        counterfact_parser.read('./configs/config_counterfactuals.properties')
        counterfact_path = counterfact_parser.get('save_path', 'cf_results')
        # print(counterfact_path)
        counterfact_list = {}
        for file in os.listdir(counterfact_path):
            counterfact_name = file.split(".",1)[0]
            df = pd.read_csv(counterfact_path+'/'+file).drop('Unnamed: 0', axis=1)
            x = df.head().to_dict(orient='records')
            # d = {char_aug_name:{"one_liner": liner()['one_liner_'+str(char_aug_name)],"data":x}}
            d = {"data":x}
            counterfact_list.update(d)
        json_report["Counterfactuals"] = {
                                        "one_liner": liner()['one_liner_counterfactuals'],
                                        "counterfactuals":counterfact_list
                                      }
        return None

    # Feedback Loop
    def stage11():
        feedback_parser = ConfigParser(interpolation=ExtendedInterpolation())
        feedback_parser.read('./configs/config_feedbackloop.properties')
        feedback_before_path = feedback_parser.get('save_path', 'feed_result_before_path')
        feedback_after_path = feedback_parser.get('save_path', 'feed_result_after_path')
        folder_path = feedback_parser.get('save_path', 'folder_path')
        with open(sep.join([feedback_before_path, "aug_before_report.json"]),'rb') as path:
            aug_feedback_before_data = json.load(path)
            # print(aug_feedback_before_data)
        with open(sep.join([feedback_before_path, "test_before_report.json"]),'rb') as path:
            test_feedback_before_data = json.load(path)
        with open(sep.join([feedback_after_path, "aug_after_report.json"]),'rb') as path:
            aug_feedback_after_data = json.load(path)
        with open(sep.join([feedback_after_path, "test_after_report.json"]),'rb') as path:
            test_feedback_after_data = json.load(path)
        with open(sep.join([folder_path, "num_samples.json"]),'rb') as path:
            feedback_samples = json.load(path)
            # print(feedback_samples)

        # print(feedback_parser.getboolean('Augmentation', 'set_execution'))
        # print(feedback_parser.getboolean('Counterfactuals', 'set_execution'))
        # print(feedback_parser.getboolean('Text_Attacks', 'set_execution'))

        if feedback_parser.getboolean('Augmentation', 'set_execution') & feedback_parser.getboolean('Counterfactuals', 'set_execution') & feedback_parser.getboolean('Text_Attacks', 'set_execution'):
            json_report["Feedback Loop"] = {
            # "Full Forms": {"aug":"Augmentation","cf":"Counterfactuals","ta":"Text Attacks"},
            "Before Model Tuning": {"Augmentation+Counterfactuals+Text Attacks":aug_feedback_before_data,"Test":test_feedback_before_data},
            "After Model Tuning" :{"Augmentation+Counterfactuals+Text Attacks":aug_feedback_after_data,"Test":test_feedback_after_data},
            "Model Tuning Report": feedback_samples,
            }

        elif feedback_parser.getboolean('Augmentation', 'set_execution') & feedback_parser.getboolean('Counterfactuals', 'set_execution'):
            json_report["Feedback Loop"] = {
            # "Full Forms": {"aug":"Augmentation","cf":"Counterfactuals"},
            "Before Model Tuning": {"Augmentation+Counterfactuals":aug_feedback_before_data,"Test":test_feedback_before_data},
            "After Model Tuning" :{"Augmentation+Counterfactuals":aug_feedback_after_data,"Test":test_feedback_after_data},
            "Model Tuning Report": feedback_samples,
            }

        elif feedback_parser.getboolean('Augmentation', 'set_execution') & feedback_parser.getboolean('Text_Attacks', 'set_execution'):
            json_report["Feedback Loop"] = {
            # "Full Forms": {"aug":"Augmentation","ta":"Text Attacks"},
            "Before Model Tuning": {"Augmentation+Text Attacks":aug_feedback_before_data,"Test":test_feedback_before_data},
            "After Model Tuning" :{"Augmentation+Text Attacks":aug_feedback_after_data,"Test":test_feedback_after_data},
            "Model Tuning Report": feedback_samples,
            }       

        elif feedback_parser.getboolean('Counterfactuals', 'set_execution') & feedback_parser.getboolean('Text_Attacks', 'set_execution'):
            json_report["Feedback Loop"] = {
            # "Full Forms": {"cf":"Counterfactuals","ta":"Text Attacks"},
            "Before Model Tuning": { "Counterfactuals+Text Attacks":aug_feedback_before_data,"Test":test_feedback_before_data},
            "After Model Tuning" :{"Counterfactuals+Text Attacks":aug_feedback_after_data,"Test":test_feedback_after_data},
            "Model Tuning Report": feedback_samples,
            }        

        elif feedback_parser.getboolean('Augmentation', 'set_execution'):
            json_report["Feedback Loop"] = {
            # "Full Forms": {"aug":"Augmentation"},
            "Before Model Tuning": {"Augmentation":aug_feedback_before_data,"Test":test_feedback_before_data},
            "After Model Tuning" :{"Augmentation":aug_feedback_after_data,"Test":test_feedback_after_data},
            "Model Tuning Report": feedback_samples,
            } 

        elif feedback_parser.getboolean('Counterfactuals', 'set_execution'):
            json_report["Feedback Loop"] = {
            # "Full Forms": {"cf":"Counterfactuals"},
            "Before Model Tuning": {"Counterfactuals":aug_feedback_before_data,"Test":test_feedback_before_data},
            "After Model Tuning" :{"Counterfactuals":aug_feedback_after_data,"Test":test_feedback_after_data},
            "Model Tuning Report": feedback_samples,
            }

        elif feedback_parser.getboolean('Text_Attacks', 'set_execution'):
            json_report["Feedback Loop"] = {
            # "Full Forms": {"ta":"Text Attacks"},
            "Before Model Tuning": {"Text Attacks":aug_feedback_before_data,"Test":test_feedback_before_data},
            "After Model Tuning" :{"Text Attacks":aug_feedback_after_data,"Test":test_feedback_after_data},
            "Model Tuning Report": feedback_samples,
            }

        return None


    def json_dump_generation():
        # Storing meta data into dict
        logger = utility.logger_setup('Json_dump', 'Json_dump.log')
        # stage1()
        # stage2()
        # stage3()
        # stage4()
        # stage5()
        # stage6()
        # stage7()
        # stage8()
        # stage9()
        # stage11()
        if parser.getboolean('loading_data_diagnostic', 'set_execution'):
            try:
                stage1()
            except Exception as e:
                # print("stage1 error: ")
                logger.critical(f'\n *** Please fix loading_data_diagnostic ***:\n{e}')
                PassError(e)
        if parser.getboolean('model_stage', 'set_execution'):
            try:
                stage2()
            except Exception as e:
                logger.critical(f'\n *** Please fix modelling_stage ***:\n{e}')
                PassError(e)
        if parser.getboolean('model_explainability', 'set_execution'):
            try:
                stage3()
            except Exception as e:
                logger.critical(f'\n *** Please fix Global Explainability stage ***:\n{e}')
                PassError(e)
            try:
                stage4()
            except Exception as e:
                logger.critical(f'\n *** Please fix Local Explainabilitystage ***\n{e}')
                PassError(e)
            try:
                stage5()    
            except Exception as e:
                logger.critical(f'\n *** Please fix Fairml stage ***\n{e}')
                PassError(e)    
        if parser.getboolean('DeepXplore', 'set_execution'):
            try:
                stage6()
            except Exception as e:
                logger.critical(f'\n *** Please fix DeepXplore stage ***\n{e}')
                PassError(e)

        if parser.getboolean('Performance_Testing', 'set_execution'):
            try:
                stage7()
            except Exception as e:
                logger.critical(f'\n *** Please fix Performance Testing ***\n{e}')
                PassError(e)


        if parser.getboolean('Text_Attacks', 'set_execution'):
            try:
                stage8()
            except Exception as e:
                logger.critical(f'\n *** Please fix Text Attacks ***\n{e}')
                PassError(e)

        if parser.getboolean('Augmentation', 'set_execution'):
            try:
                stage9()
            except Exception as e:
                logger.critical(f'\n *** Please fix Augmentation stage ***\n{e}')
                PassError(e)

        if parser.getboolean('Counterfactuals', 'set_execution'):
            try:
                stage10()
            except Exception as e:
                logger.critical(f'\n *** Please fix Counterfactuals stage ***\n{e}')
                PassError(e)

        if parser.getboolean('Feedbackloop', 'set_execution'):
            try:
                stage11()
            except Exception as e:
                logger.critical(f'\n *** Please fix Feedbackloop stage ***\n{e}')
                PassError(e)

        # dumping the JSON file
        # print('json_report', json_report)
        path = os.getcwd() + '/Results'
        # print(path)
        os.chdir(path)
        with open(f'json_metadata.json', 'w') as fp:
            json.dump(json_report, fp)
            print(f'JSON files are dumped')

    # dumping JSON metadata of all stages
    # json_dump_generation()
    # stage11()
else:
    print("Invalid License")