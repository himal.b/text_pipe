import os
import mlflow
import mlflow.sklearn
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV


class PassError(Exception):
    """Base class for exceptions in this module."""
    pass


def _logistic_reg_parameter_search(x, y, search_type='random_search', scoring_metric='accuracy'):
    """
    This function finds out the best parameters of LogisticRegression using grid_search or random_search
    by training the on the input data x and y based on scoring_metric and returns the best_estimator_ and
    best_score_ value.
    :param x: Feature array
    :param y: Target array
    :param search_type: str; default='random_search'
        'grid_search' or 'random_search'.
    :param scoring_metric: str; default='accuracy'
        Scoring metric used to find best estimators.

    :return:
        best_estimator_:  Best estimator obtained from random_search or grid_search
        best_score_: Best validation score obtained from random_search or grid_search
    """

    Max_Iter = 100000
    parameter_grid = {"penalty": ["l2"], "C": [10e-4, 10e-3, 10e-2, 10e-1, 10e-0, 10e-1, 10e-2]}

    logistic_reg = LogisticRegression(class_weight='balanced', max_iter=Max_Iter)
    if search_type == 'grid_search':
        print("grid_search model searching..")
        clf = GridSearchCV(logistic_reg, parameter_grid, scoring=scoring_metric, n_jobs=-2, verbose=1)
        clf.fit(x, y)
        return clf.best_estimator_, clf.best_score_

    elif search_type == 'random_search':
        print("random_search model searching..")
        clf = RandomizedSearchCV(logistic_reg, parameter_grid, scoring=scoring_metric, n_jobs=-2, random_state=1234,
                                 verbose=1)
        clf.fit(x, y)
        return clf.best_estimator_, clf.best_score_

def rf_parameter_search(x, y, search_type='random_search', scoring_metric='accuracy'):
    """
    This function finds out the best parameters of RandomForest using grid_search or random_search
    by training the on the input data x and y based on scoring_metric and returns the best_estimator_ and
    best_score_ value.
    :param x: Feature array
    :param y: Target array
    :param search_type: str; default='random_search'
        'grid_search' or 'random_search'.
    :param scoring_metric: str; default='accuracy'
        Scoring metric used to find best estimators.

    :return:
        best_estimator_:  Best estimator obtained from random_search or grid_search
        best_score_: Best validation score obtained from random_search or grid_search
    """
    n_estimators = [100, 300, 500]
    max_depth = [5, 10, 20]
    min_samples_split = [2, 5, 10]
    min_samples_leaf = [1, 2, 5] 
    parameter_grid = dict(n_estimators = n_estimators, max_depth = max_depth,  
              min_samples_split = min_samples_split, 
             min_samples_leaf = min_samples_leaf)
    forest = RandomForestClassifier(random_state = 1)
    if search_type == 'grid_search':
        print("grid_search model searching..")
        clf = GridSearchCV(forest, parameter_grid, scoring=scoring_metric, n_jobs=-2, verbose=1)
        clf.fit(x, y)
        return clf.best_estimator_, clf.best_score_

    elif search_type == 'random_search':
        print("random_search model searching..")
        clf = RandomizedSearchCV(forest, parameter_grid, scoring=scoring_metric, n_jobs=-2, random_state=1234,
                                 verbose=1)
        clf.fit(x, y)
        return clf.best_estimator_, clf.best_score_

def _model_tune_dict_save(model_tune_dict, embedding_name, model_name, best_estimator_, best_score_):
    """ Utility function for saving metadata in dictionary"""
    model_tune_dict['embedding_name'].append(embedding_name)
    model_tune_dict['model_name'].append(model_name)
    model_tune_dict['model_estimator'].append(best_estimator_)
    model_tune_dict['model_score'].append(best_score_)
    return None


def model_tune(count_vect_x, tfidf_vect_x, y, modeling_parser):
    """
    This function performs the exhaustive model searching task. It takes the CountVectoriser,
    TfidfVectoriser embeddings, target value (y) and parser of Modeling Stage as input and
    train the various models on all the embeddings mentioned in config properties file and
    returns the dictionary containing best_estimator_ and embedding_name along with few more
    metadata.


    1. Perform random_search or grid_search on all the models (flagged in config file) and all the embeddings.
    2. save the best_estimator_ given by random_search or grid_search of all the parameters.
    3. return that dictionary

    :param count_vect_x: sparse matrix;  CountVectoriser embeddings.
    :param tfidf_vect_x: sparse matrix;  TfidfVectoriser embeddings.
    :param y: Target value.
    :param modeling_parser: Modeling Stage parser.
    :return:
        model_tune_dict:
            Dictionary containing metadata about best_estimator_, embedding_name etc.

    """
    model_tune_dict = {'embedding_name': list(), 'model_name': list(), 'model_estimator': list(),
                       'model_score': list()}

    # setting up mlflow experiment
    experiment_name = modeling_parser.get('ml_flow', 'experiment_name')
    try:
        mlflow.create_experiment(experiment_name)
    except Exception as e:
        print(f"Creating New Experiment: {experiment_name}")
        PassError(e)

    mlflow.set_tracking_uri("sqlite:///mlruns.db")
    mlflow.set_experiment(experiment_name)
    mlflow.sklearn.autolog()

    if modeling_parser.getboolean('modeling', 'parameters.random_search'):
        if modeling_parser.getboolean('modeling', 'execute.modeling.logistic_regression'):
            scoring_metric = modeling_parser.get('modeling', 'parameters.performance_metric')

            # For CountVectoriser embeddings
            try:
                with mlflow.start_run(run_name='random_search_count_vect_logistic_regression'):
                    best_estimator_, best_score_ = _logistic_reg_parameter_search(count_vect_x, y, 'random_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'count_vectoriser', 'linear_model', best_estimator_, best_score_)
            except:
                None
            # For TfidfVectoriser embeddings
            try:
                with mlflow.start_run(run_name='random_search_tfidf_logistic_regression'):
                    best_estimator_, best_score_ = _logistic_reg_parameter_search(tfidf_vect_x, y, 'random_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'tfidf_vectoriser', 'linear_model', best_estimator_, best_score_)
            except:
                None
        elif modeling_parser.getboolean('modeling', 'execute.modeling.random_forest'):
            scoring_metric = modeling_parser.get('modeling', 'parameters.performance_metric')

            # For CountVectoriser embeddings
            try:
                with mlflow.start_run(run_name='random_search_count_vect_random_forest'):
                    best_estimator_, best_score_ = rf_parameter_search(count_vect_x, y, 'random_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'count_vectoriser', 'forest_model', best_estimator_, best_score_)
            except:
                None
            # For TfidfVectoriser embeddings
            try:
                with mlflow.start_run(run_name='random_search_tfidf_random_forest'):
                    best_estimator_, best_score_ = rf_parameter_search(tfidf_vect_x, y, 'random_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'tfidf_vectoriser', 'forest_model', best_estimator_, best_score_)
            except:
                None

    elif modeling_parser.getboolean('modeling', 'parameters.grid_search'):
        if modeling_parser.getboolean('modeling', 'execute.modeling.logistic_regression'):
            scoring_metric = modeling_parser.get('modeling', 'parameters.performance_metric')

            # For CountVectoriser embeddings
            try:
                with mlflow.start_run(run_name='grid_search_count_vect_logistic_regression'):
                    best_estimator_, best_score_ = _logistic_reg_parameter_search(count_vect_x, y, 'grid_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'count_vectoriser', 'linear_model', best_estimator_, best_score_)
            except:
                None
            # For TfidfVectoriser embeddings
            try:
                with mlflow.start_run(run_name='grid_search_tfidf_logistic_regression'):
                    best_estimator_, best_score_ = _logistic_reg_parameter_search(tfidf_vect_x, y, 'grid_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'tfidf_vectoriser', 'linear_model', best_estimator_, best_score_)
            except:
                None

        elif modeling_parser.getboolean('modeling', 'execute.modeling.random_forest'):
            scoring_metric = modeling_parser.get('modeling', 'parameters.performance_metric')

            # For CountVectoriser embeddings
            try:
                with mlflow.start_run(run_name='grid_search_count_vect_random_forest'):
                    best_estimator_, best_score_ = rf_parameter_search(count_vect_x, y, 'grid_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'count_vectoriser', 'forest_model', best_estimator_, best_score_)
            except:
                None
            # For TfidfVectoriser embeddings
            try:
                with mlflow.start_run(run_name='grid_search_tfidf_random_forest'):
                    best_estimator_, best_score_ = rf_parameter_search(tfidf_vect_x, y, 'grid_search',
                                                                                  scoring_metric)
                _model_tune_dict_save(model_tune_dict, 'tfidf_vectoriser', 'forest_model', best_estimator_, best_score_)
            except:
                None
    return model_tune_dict