from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
import seaborn
import matplotlib.pyplot as plt

# set the background color to black
plt.style.use('dark_background')


def compute_train_val_accuracy(actual_train_y, predict_train_y, actual_val_y, predict_val_y):
    """ This is a utility function which computes train and val accuracy score and return it"""
    accuracy_score_train = accuracy_score(actual_train_y, predict_train_y)
    accuracy_score_val = accuracy_score(actual_val_y, predict_val_y)
    return accuracy_score_train, accuracy_score_val


def compute_classification_report(actual_train_y, predict_train_y):
    """ Utility function for classification report"""
    return classification_report(actual_train_y, predict_train_y)


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def plot_confusion_matrix(actual_train_y, predict_train_y, labels):
    """
    Utility function to plot a confusion matrix, as returned by sklearn.metrics.confusion_matrix,
    as a heatmap.
    Credits: https://gist.github.com/shaypal5/94c53d765083101efc0240d776a23823
    """
    # confusion matrix figure
    matrix = confusion_matrix(actual_train_y, predict_train_y)
    df_cm = pd.DataFrame(matrix, index=labels, columns=labels)

    fig = plt.figure(figsize=(9, 7))
    try:
        heat_map = sns.heatmap(df_cm, annot=True, fmt="d")
    except ValueError:
        raise ValueError("Confusion matrix values must be integers.")
    heat_map.yaxis.set_ticklabels(heat_map.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=10)
    heat_map.xaxis.set_ticklabels(heat_map.xaxis.get_ticklabels(), rotation=45, ha='right', fontsize=10)
    plt.ylabel('True label', fontdict={'color': 'white'})
    plt.xlabel('Predicted label', fontdict={'color': 'white'})
    return fig

# from sklearn.metrics import accuracy_score, classification_report


# def compute_train_val_accuracy(actual_train_y, predict_train_y, actual_val_y, predict_val_y):
#     """ This is a utility function which computes train and val accuracy score and return it"""
#     accuracy_score_train = accuracy_score(actual_train_y, predict_train_y)
#     accuracy_score_val = accuracy_score(actual_val_y, predict_val_y)
#     return accuracy_score_train, accuracy_score_val


# def compute_classification_report(actual_train_y, predict_train_y):
#     """ Utility function for classification report"""
#     return classification_report(actual_train_y, predict_train_y)


# def confusion_matrix_plot(actual_train_y, predict_train_y):
#     """ Utility function for confusion matrix plot"""
#     return None
