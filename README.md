The project named Text Pipeline is used for end-to-end testing of AI projects involving text Data.

Text pipeline:

A pipeline that helps explaining data distribution before and after preprocessing, our model feature-wise, instance wise and explains the bias in our features using various techniques for global, local explainability, and neuron coverage using deepxplore
Installation and Running the pipeline:
Python 3.7 (anaconda/miniconda is preferable)
Create new virtual environment using:
conda create -n myenvironment python=3.7
Activate virtual environment using:
conda activate myenvironment
cd To your desired path and run command:
pip install -r requirements.txt
Keep your text data in the Dataset folder.
mention the input_column and output_column in config_data_diagnostic.properties which contains in the Configs folder.
To execute the pipeline
run the command: python run.py
the textpipeline will be executed
