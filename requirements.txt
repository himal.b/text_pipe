absl-py==1.0.0
alembic==1.4.1
astor==0.8.1
astunparse==1.6.3
attrs==21.2.0
azure-core==1.7.0
azure-storage-blob==12.3.2
backcall==0.2.0
beautifulsoup4==4.9.3
boto3==1.20.10
botocore==1.23.10
Brotli==1.0.9
cached-property==1.5.2
certifi==2021.10.8
cffi==1.15.0
charset-normalizer==2.0.7
click==8.0.3
cloudpickle==1.6.0
cma==3.0.3
colorama==0.4.4
ConfigArgParse==1.5.3
cryptography==3.4.8
cycler==0.10.0
Cython==0.29.28
databricks-cli==0.16.2
decorator==4.4.2
docker==5.0.3
eli5==0.10.1
entrypoints==0.3
fairml==0.1.1.5rc1
ffmpeg-python==0.2.0
Flask==2.0.2
Flask-BasicAuth==0.2.0
Flask-Cors==3.0.10
flatbuffers==1.12
future==0.18.2
gast==0.2.2
gensim==4.2.0
gevent==21.8.0
geventhttpclient==1.5.3
gitdb==4.0.9
GitPython==3.1.24
google-pasta==0.2.0
gorilla==0.3.0
graphviz==0.18.2
greenlet==1.1.2
grpcio==1.41.1
h5py==2.10.0
idna==3.3
imageio==2.10.5
importlib-metadata==4.8.2
invokust==0.76
ipython==7.16.2
isodate==0.6.0
itsdangerous==2.0.1
jedi==0.17.0
Jinja2==3.0.3
jmespath==0.10.0
joblib==0.16.0
Keras==2.3.1
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.0
kiwisolver==1.3.2
lime==0.2.0.1
llvmlite==0.33.0
locust==2.5.0
lxml==4.6.3
Mako==1.1.5
Markdown==3.3.5
MarkupSafe==2.0.1
matplotlib==3.3.3
mlflow==1.12.1
msgpack==1.0.2
msrest==0.6.17
mypy==0.782
mypy-extensions==0.4.3
networkx==2.6.3
nltk==3.5
nlpaug==1.1.10
ntplib==0.4.0
numba==0.50.1
numpy==1.19.3
oauthlib==3.1.1
opt-einsum==3.3.0
pandas==1.2.5
parso==0.8.2
patsy==0.5.1
pickleshare==0.7.5
Pillow==8.4.0
prometheus-client==0.12.0
prometheus-flask-exporter==0.18.5
prompt-toolkit==3.0.22
protobuf==3.19.1
psutil==5.8.0
pycparser==2.21
pycryptodome==3.14.1
pydub==0.24.1
Pygments==2.10.0
pyparsing==2.4.7
python-dateutil==2.8.1
python-editor==1.0.4
pytz==2021.3
PyWavelets==1.1.1
pywin32==227
PyYAML==6.0
pyzmq==22.3.0
querystring-parser==1.2.4
regex==2021.11.10
requests==2.26.0
requests-oauthlib==1.3.0
resampy==0.2.2
roundrobin==0.0.2
s3transfer==0.5.0
scikit-image==0.18.3
scikit-learn==0.23.2
scipy==1.7.2
seaborn==0.11.0
shap==0.37.0
six==1.16.0
slicer==0.0.3
smart-open==6.0.0
smmap==5.0.0
soupsieve==2.3.1
SQLAlchemy==1.4.27
sqlparse==0.4.2
statsmodels==0.11.1
tabulate==0.8.9
tensorboard==1.15.0
tensorflow==1.15.0
tensorflow-estimator==1.15.1
#tensorflow-gpu==1.15.0
termcolor==1.1.0
threadpoolctl==3.0.0
tifffile==2021.11.2
torch==1.7.1
tqdm==4.48.2
traitlets==5.1.1
truepy==2.0.4
typed-ast==1.4.1
typing_extensions==4.0.0
urllib3==1.26.7
waitress==2.0.0
wcwidth==0.2.5
webencodings==0.5.1
websocket-client==1.2.1
Werkzeug==2.0.2
wincertstore==0.2
wordcloud==1.8.1
wrapt==1.13.3
zipp==3.6.0
zope.event==4.5.0
zope.interface==5.4.0