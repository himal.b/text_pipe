import license_execute as le
license_status = le.license_status()
if license_status == 'Valid@tAi':
	from flask import Flask, render_template, request
	import pickle
	# import numpy as np

	model = pickle.load(open("Results/Modeling/Model/model.pkl", "rb"))
	# cv = pickle.load(open("Results/DataDiagnostic/Embeddings/CountVectoriser/count_vectoriser.pkl", "rb"))
	tfidf = pickle.load(open("Results/DataDiagnostic/Embeddings/TfidfVectoriser/tfidf_vectoriser.pkl", "rb"))
	le = pickle.load(open("Results/DataDiagnostic/MetaData/label_encoder.pkl","rb"))
	def main_script():
		app = Flask(__name__)



		@app.route('/')
		def man():
			return render_template('home.html')


		@app.route('/', methods=['POST'])
		def home():
			if request.method == 'POST':
				data1 = request.form['text']
				data = [data1]
				# print(data,"DATA")
				arr = tfidf.transform(data).toarray()
				pred = model.predict(arr)
				class_label = le.inverse_transform(pred)[0]
				return render_template('after.html', data=class_label)
		return app

	# if __name__ == "__main__":
	# 	app.run(debug = False, port = 8000)

else:
	print("Invalid License")