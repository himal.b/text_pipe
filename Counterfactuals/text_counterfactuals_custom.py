
import os
import re
os.environ["MODEL_DIR"] = ''
from datetime import datetime
from bs4 import BeautifulSoup

# # load required modules from polyjuice
# from polyjuice import Polyjuice
# from polyjuice.generations import ALL_CTRL_CODES

# # these libraries provide modules to deal with text data
# import transformers
# import munch
# import sentence_transformers
# import pattern
# import zss
# import nltk

# # to work on processed data
# import spacy
# import scipy

# to handle processed data (arrays & dataframes) 
import numpy as np
import pandas as pd
from copy import deepcopy

start_time = datetime.now()


class text_counterfactuals:

    def __init__(self, orig, t_model, tfidf, perturb_idx):
        ''' orig: list of original text sentences
            t_model: pretrained text classifier model
            perturb_idx: index of text data to be perturbed
        '''
        self.orig = orig
        self.t_model = t_model
        self.tfidf = tfidf
        self.perturb_idx = perturb_idx


    # make a tuple with original and perturbed sentence as pair
    def wrap_perturbed_instances(self, perturb_texts):
        '''perturb_texts: polyjuice-generated perturbed sentences
        '''
        perturbs = []
        for a in perturb_texts:
            curr_example = deepcopy(self.orig[self.perturb_idx])
            curr_example = (curr_example,a)
            perturbs.append(tuple(curr_example))
        return perturbs


    # generate model outputs given a saved model and data (text)
    def model_outputs(self, texts):
        ''' 
            texts: data on which to apply the model
        '''
        arr = self.tfidf.transform(texts).toarray()
        _pred = self.t_model.predict(arr)
        _pred_proba = self.t_model.predict_proba(arr)
        return _pred,_pred_proba


    # a function to create a dataframe with respect to given text
    def cf_df(self, orig_labels, label_encoder, perturb_texts):
        ''' perturb_instances: tuple of original and perturbed text pair
            orig_texts: input data
            orig_labels: input labels of orig_texts
            perturb_texts: perturbed sentences genrated from polyjuice
        '''
        orig = self.orig
        # print(type(orig), "FORMAT OF ORIGINAL TEXT")
        # print(type(perturb_texts), "FORMAT OF PERTURB TEXT")
        # clean_text = BeautifulSoup(orig, 'lxml').get_text()
        # clean_text = re.sub(r'\n', " ", orig)
        # orig = re.sub(r"[^A-Za-z0-9]", " ", clean_text)
        # print(orig,"\n\n")
        perturb_instances = self.wrap_perturbed_instances(perturb_texts)
        df = pd.DataFrame(data=perturb_instances,columns=['original','perturbed'])
        # print(df.head())
        # df = pd.DataFrame(perturb_instances)
        # df.rename(columns = {0:'original',1:'perturbed'}, inplace = True)
        # countfact_df = df
        # print(orig[:5], "FORMAT OF ORIGINAL TEXT")
        preds, pred_probas = self.model_outputs(orig)
        preds = label_encoder.inverse_transform(preds)
        # print(type(perturb_texts[0]), perturb_texts[0], "FORMAT OF PERTURB TEXT")
        preds_p, pred_probas_p = self.model_outputs(perturb_texts)
        preds_p = label_encoder.inverse_transform(preds_p)
        df.loc[:,('original_pred')] = preds[self.perturb_idx]
        df.loc[:,('original_pred_proba')] = str(round(max(pred_probas[self.perturb_idx]),3))
        df.loc[:,('perturb_label')] = preds_p
        # countfact_df.loc[:,('perturb_label')] = preds_p
        df.insert(5,'perturb_pred_proba', '')
        for i in range(len(pred_probas_p)):
            df.loc[:,('perturb_pred_proba')] = str(round(max(pred_probas_p[i]),3))        
        cols_list = list(df)
        cols_list[1], cols_list[2], cols_list[3] = cols_list[2], cols_list[3], cols_list[1]
        df = df[cols_list]
        df.insert(1,'original_label',orig_labels[self.perturb_idx])
        # countfact_df.insert(1,'original_label',orig_labels[self.perturb_idx])
        # rotation_df.loc[len(rotation_df.index)]=[img_path,class_names[label], score,round(iou,2)]

        return df